<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

//--------------------------------------------------PROFILE SECTION-----------------------------------------------------------------------------
// SALES AGENT ROUTES & REQUESTS
Route::get('/profiles/sales-agent', 'SalesAgentController@index');
Route::get('/sales-agent-table', 'SalesAgentController@datatable');
Route::post('/store-agent', 'SalesAgentController@store');
Route::post('/edit-agent', 'SalesAgentController@edit');
Route::post('/retire-agent', 'SalesAgentController@retire');
Route::post('/remove-detail-agent', 'SalesAgentController@removeDetail');

// LOCAL CUSTOMER ROUTES & REQUESTS
Route::get('/profiles/local-customer', 'LocalCustomerController@index');
Route::post('/store-local-customer', 'LocalCustomerController@store');
Route::post('/edit-local-customer', 'LocalCustomerController@edit');
Route::post('/retire-local-customer', 'LocalCustomerController@retire');
Route::post('/remove-detail-local-customer', 'LocalCustomerController@removeDetail');

// FOREIGN PARTNERS ROUTES & REQUESTS
Route::get('/profiles/foreign-partners', 'ForeignPartnersController@index');
Route::post('/store-foreign-partners', 'ForeignPartnersController@store');
Route::post('/edit-foreign-partners', 'ForeignPartnersController@edit');
Route::post('/retire-foreign-partners', 'ForeignPartnersController@retire');
Route::post('/remove-detail-foreign-partners', 'ForeignPartnersController@removeDetail');

// CUSTOMS BROKERAGE ROUTES & REQUESTS
Route::get('/profiles/customs-brokerage', 'CustomsBrokerageController@index');
Route::post('/store-customs-brokerage', 'CustomsBrokerageController@store');
Route::post('/edit-customs-brokerage', 'CustomsBrokerageController@edit');
Route::post('/retire-customs-brokerage', 'CustomsBrokerageController@retire');
Route::post('/remove-detail-customs-brokerage', 'CustomsBrokerageController@removeDetail');

// TRUCKING COMPANY ROUTES & REQUESTS
Route::get('/profiles/trucking-companies', 'TruckingCompanyController@index');
Route::post('/store-trucking-company', 'TruckingCompanyController@store');
Route::post('/edit-trucking-company', 'TruckingCompanyController@edit');
Route::post('/retire-trucking-company', 'TruckingCompanyController@retire');
Route::post('/remove-detail-trucking-company', 'TruckingCompanyController@removeDetail');

// SHIPPING LINE ROUTES & REQUESTS
Route::get('/profiles/shipping-lines', 'ShippingLineController@index');
Route::post('/store-shipping-line', 'ShippingLineController@store');
Route::post('/edit-shipping-line', 'ShippingLineController@edit');
Route::post('/retire-shipping-line', 'ShippingLineController@retire');
Route::post('/remove-detail-shipping-line', 'ShippingLineController@removeDetail');

// PORT ROUTES & REQUESTS
Route::get('/profiles/ports', 'PortsController@index');
Route::post('/store-port', 'PortsController@store');
Route::post('/edit-port', 'PortsController@edit');
Route::post('/retire-port', 'PortsController@retire');

// ITEM ROUTES & REQUESTS
Route::get('/profiles/items', 'ItemsController@index');
Route::post('/store-item', 'ItemsController@store');
Route::post('/edit-item', 'ItemsController@edit');
Route::post('/retire-item', 'ItemsController@retire');

//--------------------------------------------------TRANSACTIONS SECTION-----------------------------------------------------------------------------

// INCOMING CONTAINERS ROUTES & REQUESTS
Route::get('/transactions/incoming-containers', 'IncomingContainersController@index');
Route::post('/store-incoming-containers', 'IncomingContainersController@store');
Route::post('/additional-details', 'IncomingContainersController@additionalDetail');

// ON PROCESS ROUTES & REQUESTS
Route::get('/transactions/on-process', 'OnProcessController@index');
Route::post('/additional-details-op', 'OnProcessController@additionalDetail');

// ENDORSE TO TRUCKING ROUTES & REQUESTS
Route::get('/transactions/endorsed-to-trucking', 'EndorsedTruckingController@index');
Route::post('/additional-details-et', 'EndorsedTruckingController@additionalDetail');

// DELIVERED CONTAINERS ROUTES & REQUESTS
Route::get('/transactions/delivered-containers', 'DeliveredContainersController@index');

//CONTAINER EXPENSES
Route::post('/container-expenses', 'Controller@containerExpenses');

//ADDITIONAL CONTACT DETAILS FOR ALL CONTACT PERSONS (PROFILES)
Route::post('/additional-contact-detail', 'Controller@additionalContactDetail');

//RETIRE SINGLE RECORD (PROFILES)
Route::post('/single-set-status', 'Controller@singleSetStatus');

//RETIRE, RESTORE, PROCEED, REVERT ALL RECORDS
Route::post('/multiple-retire', 'Controller@multipleRetire');
Route::post('/multiple-set-status', 'Controller@multipleSetStatus');
Route::post('/multiple-proceed', 'Controller@multipleProceed');
Route::post('/multiple-revert', 'Controller@multipleRevert');

//SET STATUS (PROFILES)
Route::post('/set-status', 'Controller@setStatus');

//SET DATERANGE (TRANSACTIONS)
Route::post('/set-daterange', 'Controller@setDaterange');












































//
