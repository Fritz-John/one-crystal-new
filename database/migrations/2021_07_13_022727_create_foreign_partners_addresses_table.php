<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignPartnersAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_partners_addresses', function (Blueprint $table) {
          $table->id();
          $table->string('fp_address_type');
          $table->string('fp_address');
          $table->string('fp_country');
          $table->unsignedBigInteger('fp_id');
          $table->foreign('fp_id')
          ->references('id')
          ->on('foreign_partners');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_partners_addresses');
    }
}
