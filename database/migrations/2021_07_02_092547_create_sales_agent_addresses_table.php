<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesAgentAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_agent_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('sa_address_type');
            $table->string('sa_address');
            $table->string('sa_city');
            $table->unsignedBigInteger('sa_id');
            $table->foreign('sa_id')
            ->references('id')
            ->on('sales_agents');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_agent_addresses');
    }
}
