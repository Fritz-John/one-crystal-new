<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalCustomerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_customer_items', function (Blueprint $table) {
          $table->id();

          $table->unsignedBigInteger('item_id');
          $table->foreign('item_id')
          ->references('id')
          ->on('items');

          $table->unsignedBigInteger('lc_id');
          $table->foreign('lc_id')
          ->references('id')
          ->on('local_customers');

          $table->unsignedBigInteger('ic_id');
          $table->foreign('ic_id')
          ->references('id')
          ->on('containers');

          $table->double('package_price')->nullable();
          $table->date('last_delivery_date')->nullable();
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_customer_items');
    }
}
