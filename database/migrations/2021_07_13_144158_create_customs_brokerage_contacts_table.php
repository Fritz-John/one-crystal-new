<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomsBrokerageContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs_brokerage_contacts', function (Blueprint $table) {
          $table->id();
          $table->string('cb_cp_contact_type');
          $table->string('cb_cp_contact');
          $table->unsignedBigInteger('cb_cp_id');
          $table->foreign('cb_cp_id')
          ->references('id')
          ->on('customs_brokerage_contact_people');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs_brokerage_contacts');
    }
}
