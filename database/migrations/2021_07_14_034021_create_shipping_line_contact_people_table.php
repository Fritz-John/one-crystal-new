<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingLineContactPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shipping_line_contact_people', function (Blueprint $table) {
          $table->id();
          $table->string('sl_cp_name');
          $table->string('sl_cp_position');
          $table->unsignedBigInteger('sl_id');
          $table->foreign('sl_id')
          ->references('id')
          ->on('shipping_lines');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shipping_line_contact_people');
    }
}
