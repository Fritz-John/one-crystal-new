<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContainersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('containers', function (Blueprint $table) {
            $table->id();

            //MODAL FIELDS
            $table->unsignedBigInteger('ic_shipper_id');
            $table->integer('ic_blno');
            $table->unsignedBigInteger('ic_consignee_id');
            $table->unsignedBigInteger('ic_shipping_id');
            $table->unsignedBigInteger('ic_loading_port');
            $table->unsignedBigInteger('ic_discharge_port');
            $table->string('ic_container_size');
            $table->unsignedBigInteger('ic_items');
            $table->integer('ic_container_no');
            $table->integer('ic_packages');
            $table->double('ic_gross_weight');
            $table->unsignedBigInteger('ic_customer');
            $table->string('ic_need_acfta');

            $table->string('ic_external_trucking')->nullable();
            $table->unsignedBigInteger('ic_trucking_company')->nullable();
            $table->double('ic_trucking_charge')->nullable();
            $table->string('ic_tc_payment_status')->nullable();

            $table->string('ic_customer_request');
            $table->unsignedBigInteger('ic_encoded_by');
            $table->unsignedBigInteger('ic_eta_acfta_by')->nullable();
            $table->string('ic_status');

            //DATATABLE FIELDS
            $table->string('ic_payment_status')->nullable();
            $table->date('ic_bl_date')->nullable();
            $table->string('ic_bl_file')->nullable();

            $table->date('ic_pl_date')->nullable();
            $table->string('ic_pl_file')->nullable();

            $table->date('ic_acfta_date')->nullable();
            $table->string('ic_acfta_file')->nullable();

            $table->double('ic_package_price')->nullable();
            $table->string('ic_pp_payment_status')->nullable();

            $table->double('ic_package_cost')->nullable();
            $table->string('ic_pc_payment_status')->nullable();

            $table->date('ic_eta');
            $table->date('ic_yard_date')->nullable();

            $table->date('ic_on_process_date')->nullable();
            $table->string('ic_on_process_remarks')->nullable();

            $table->date('ic_cro_validity')->nullable();
            $table->string('ic_cro_remarks')->nullable();

            $table->date('ic_debt_date')->nullable();
            $table->string('ic_debt_remarks')->nullable();

            $table->string('ic_oft')->nullable();
            $table->string('ic_lft')->nullable();

            $table->date('ic_sl_payment_date')->nullable();
            $table->double('ic_sl_expenses')->nullable();
            $table->double('ic_sl_container_deposit')->nullable();

            $table->string('ic_preassess_duties')->nullable();

            $table->date('ic_lodge_date')->nullable();
            $table->string('ic_lodge_remarks')->nullable();

            $table->date('ic_final_date')->nullable();
            $table->string('ic_final_duties')->nullable();
            $table->string('ic_final_remarks')->nullable();

            $table->date('ic_debited_date')->nullable();
            $table->string('ic_debited_duties')->nullable();
            $table->string('ic_debited_remarks')->nullable();

            $table->string('ic_debit_bank')->nullable();
            $table->string('ic_debit_branch')->nullable();
            $table->double('ic_debit_amount')->nullable();

            $table->date('ic_storage_validity')->nullable();

            $table->date('ic_gate_date')->nullable();
            $table->string('ic_gate_pass_file')->nullable();
            $table->string('ic_gate_remarks')->nullable();

            $table->string('ic_reg_no')->nullable();
            $table->string('ic_entry_no')->nullable();
            $table->string('ic_pro_no')->nullable();

            $table->date('ic_endorsed_date')->nullable();
            $table->date('ic_load_date')->nullable();
            $table->date('ic_empty_date')->nullable();

            $table->date('ic_eir_date')->nullable();
            $table->string('ic_eir_file')->nullable();

            $table->string('ic_in_transit')->nullable();
            $table->string('ic_in_yard')->nullable();
            $table->date('ic_delivery_date')->nullable();

            //FOREIGN KEYS
            $table->foreign('ic_shipper_id')
            ->references('id')
            ->on('foreign_partners_shippers');

            $table->foreign('ic_consignee_id')
            ->references('id')
            ->on('customs_brokerage_consignees');

            $table->foreign('ic_shipping_id')
            ->references('id')
            ->on('shipping_lines');

            $table->foreign('ic_loading_port')
            ->references('id')
            ->on('ports');

            $table->foreign('ic_discharge_port')
            ->references('id')
            ->on('ports');

            $table->foreign('ic_items')
            ->references('id')
            ->on('items');

            $table->foreign('ic_customer')
            ->references('id')
            ->on('local_customers');

            $table->foreign('ic_trucking_company')
            ->references('id')
            ->on('trucking_companies');

            $table->foreign('ic_encoded_by')
            ->references('id')
            ->on('users');

            $table->foreign('ic_eta_acfta_by')
            ->references('id')
            ->on('users');

            $table->string('status')->nullable();
            $table->timestamps();
            $table->datetime('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('containers');
    }
}
