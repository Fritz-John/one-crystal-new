<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocalCustomerContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('local_customer_contacts', function (Blueprint $table) {
          $table->id();
          $table->string('lc_cp_contact_type');
          $table->string('lc_cp_contact');
          $table->unsignedBigInteger('lc_cp_id');
          $table->foreign('lc_cp_id')
          ->references('id')
          ->on('local_customer_contact_people');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('local_customer_contacts');
    }
}
