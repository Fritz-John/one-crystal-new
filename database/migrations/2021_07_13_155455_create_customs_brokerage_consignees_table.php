<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomsBrokerageConsigneesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs_brokerage_consignees', function (Blueprint $table) {
          $table->id();
          $table->string('cc_name');
          $table->string('cc_address');
          $table->string('cc_registration');
          $table->string('cc_tin');
          $table->unsignedBigInteger('cb_id');
          $table->foreign('cb_id')
          ->references('id')
          ->on('customs_brokerages');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs_brokerage_consignees');
    }
}
