<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTruckingCompanyAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trucking_company_addresses', function (Blueprint $table) {
          $table->id();
          $table->string('tc_address_type');
          $table->string('tc_address');
          $table->string('tc_city');
          $table->unsignedBigInteger('tc_id');
          $table->foreign('tc_id')
          ->references('id')
          ->on('trucking_companies');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trucking_company_addresses');
    }
}
