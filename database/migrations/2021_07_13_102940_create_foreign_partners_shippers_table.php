<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateForeignPartnersShippersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foreign_partners_shippers', function (Blueprint $table) {
          $table->id();
          $table->string('fp_sc_name');
          $table->string('fp_sc_address');
          $table->unsignedBigInteger('fp_id');
          $table->foreign('fp_id')
          ->references('id')
          ->on('foreign_partners');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foreign_partners_shippers');
    }
}
