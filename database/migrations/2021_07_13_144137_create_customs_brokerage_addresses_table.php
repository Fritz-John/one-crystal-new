<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomsBrokerageAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customs_brokerage_addresses', function (Blueprint $table) {
          $table->id();
          $table->string('cb_address_type');
          $table->string('cb_address');
          $table->string('cb_city');
          $table->unsignedBigInteger('cb_id');
          $table->foreign('cb_id')
          ->references('id')
          ->on('customs_brokerages');
          $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customs_brokerage_addresses');
    }
}
