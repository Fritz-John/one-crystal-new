<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SalesAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([

        [
          'id' => 1,
          'sa_name' => "Aileen Liu",
          'sa_commission' => 2000,
          'sa_note' => "N/A",
          'status' => "Active",
        ],

        [
          'id' => 2,
          'sa_name' => "Diana Natividad",
          'sa_commission' => 2000,
          'sa_note' => "N/A",
          'status' => "Active",
        ],


      ]);
    }
}
