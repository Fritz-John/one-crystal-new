@extends('layouts.app')

@section('content')
  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-header">
            <h1>Items</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_item_modal"> <i class="fas fa-plus"></i> Add Item</button>

            <button type="button" id="item_request" data-target="select_item[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="item_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>
          <div class="card-body">

            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Item</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_items as $items)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($items->item_name)}}" type="checkbox" class="multi_container" name="select_item[]" value="{{$items->id}}">
                    </td>
                    <td>{{ucwords($items->item_name)}}</td>
                    <td>
                      <button id="{{$items->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_item_modal_{{$items->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$items->id}}" type="button" data-type="item-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($items->item_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>

                  <div class="modal fade" id="edit_item_modal_{{$items->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                      <div class="modal-content">
                        <div class="modal-header occolor_bg text-white">
                          <h5 class="modal-title">Edit Item</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          <div class="row">

                            <div class="col-lg-12">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">Item</div>
                                  </div>
                                  <input type="text" class="form-control edit_required_fields_{{$items->id}}" name="edit_item_name" placeholder="Name" value="{{ucwords($items->item_name)}}">
                                </div>
                              </div>
                            </div>

                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$items->id}}" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                        </div>
                      </div>
                    </div>
                  </div>
                @endforeach
              </tbody>
            </table>

          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="add_item_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Item</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="item_name" placeholder="Name">
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script src="{{asset('js/crud/items-crud.js')}}" charset="utf-8"></script>
@endsection
