@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Trucking Companies</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_tc_modal"> <i class="fas fa-plus"></i> Add Trucking Company</button>

            <button type="button" id="tc_request" data-target="select_tc[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="tc_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>City</th>
                  <th>Contact Person</th>
                  <th>Position</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Terms</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_trucking_companies as $trucking_company)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($trucking_company->tc_name)}}" type="checkbox" class="multi_container" name="select_tc[]" value="{{$trucking_company->id}}">
                    </td>
                    <td>{{ucwords($trucking_company->tc_name)}}</td>
                    <td>
                      @foreach ($trucking_company->address as $tc_address)
                        {{ucwords($tc_address->tc_city)}}
                        @break
                      @endforeach
                    </td>
                    @foreach ($trucking_company->contact as $tc_cp)
                      <td>{{ucwords($tc_cp->tc_cp_name)}}</td>
                      <td>{{ucwords($tc_cp->tc_cp_position)}}</td>
                      <td>
                        @foreach ($tc_cp->detail as $cp_detail)
                          @if ($cp_detail->tc_cp_contact_type == "Mobile")
                            {{$cp_detail->tc_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @foreach ($tc_cp->detail as $cp_detail)
                          @if ($cp_detail->tc_cp_contact_type == "Email")
                            {{$cp_detail->tc_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      @break
                    @endforeach
                    <td>{{$trucking_company->tc_terms}}</td>
                    <td>
                      <button id="{{$trucking_company->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_tc_modal_{{$trucking_company->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$trucking_company->id}}" type="button" data-type="tc-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($trucking_company->tc_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="add_tc_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Trucking Company</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="tc_name" placeholder="Full Name">
                </div>
              </div>
            </div>

            <div data-target="address_container" id="address_1" class="col-lg-12 p-1 default_address">
              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-130">
                      <select data-target="address_field_1" class="form-control required_fields address_1_type">
                        <option value="Office" selected>Office</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Home">Home</option>
                      </select>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_address" placeholder="Address">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">City</div>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_city" placeholder="City">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div data-target="person_container" id="person_1" class="row p-2 default_person">
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Contact Person</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_name" placeholder="Full Name">
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Position</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_position" placeholder="Position">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_contact_person">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_1" class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_1">
                        <option value="/Mobile-" selected>Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-">Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_1" placeholder="Contact Details">
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_2" class="col-lg-6 default_contact">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_2">
                        <option value="/Mobile-">Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-" selected>Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_2" placeholder="Contact Details">
                    <div class="input-group-append">
                      <button name="person_1" data-target="add_modal" type="button" class="btn btn-primary add_contact">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div class="col-lg-12">
              <div class="form-group half">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Terms</div>
                  </div>
                  <input type="number" class="form-control required_fields text-right" name="tc_terms">
                  <div class="input-group-append">
                    <div class="input-group-text"> <small>Days</small> </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Notes</div>
                  </div>
                  <textarea class="form-control required_fields" rows="3" name="tc_note" ></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_tc_details as $tcd)
    <div class="modal fade" id="edit_tc_modal_{{$tcd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg text-white">
            <h5 class="modal-title">Edit Trucking Company</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#main_content_{{$tcd->id}}" role="tab" aria-selected="true">Main Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#add_content_{{$tcd->id}}" role="tab" aria-selected="false">Additional Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#plate_content_{{$tcd->id}}" role="tab" aria-selected="false">Plate Numbers</a>
                      </li>
                    </ul>
                  </div>

                  <div class="card-body">

                    <div class="tab-content" id="custom-tabs-four-tabContent">

                      <div class="tab-pane fade show active" id="main_content_{{$tcd->id}}" role="tabpanel">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Name</div>
                                </div>
                                <input data-target="tc_id" type="hidden" class="form-control edit_required_fields_{{$tcd->id}}" value="{{ucwords($tcd->id)}}">
                                <input data-target="tc_name" type="text" class="form-control edit_required_fields_{{$tcd->id}}"  placeholder="Full Name" value="{{ucwords($tcd->tc_name)}}">
                              </div>
                            </div>
                          </div>
                          @php
                            $add_count = 1;
                            $address_count = count($tcd->address);
                          @endphp
                          @foreach ($tcd->address as $tca)
                            <div data-target="edit_address_container_{{$tcd->id}}" id="edit_address_{{$add_count}}_{{$tcd->id}}" class="col-lg-12 p-1">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend w-130">
                                      <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$tcd->id}} edit_address_{{$add_count}}_type">
                                        <option value="Office" {{($tca->tc_address_type == "Office") ? "selected" : ""}}>Office</option>
                                        <option value="Warehouse" {{($tca->tc_address_type == "Warehouse") ? "selected" : ""}}>Warehouse</option>
                                        <option value="Home" {{($tca->tc_address_type == "Home") ? "selected" : ""}}>Home</option>
                                      </select>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="hidden" class="edit_address_{{$add_count}}_id" value="{{$tca->id}}">
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$tcd->id}} edit_address_{{$add_count}}_address" placeholder="Address" value="{{ucwords($tca->tc_address)}}">
                                    @if ($address_count > 1)
                                      <div class="input-group-append">
                                        <button data-target="{{$tca->id}}|{{ucwords($tca->tc_address)}}" type="button" class="btn btn-danger remove_current_address">
                                          <i class="fas fa-times"></i>
                                        </button>
                                      </div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">City</div>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$tcd->id}} edit_address_{{$add_count}}_city" placeholder="City" value="{{ucwords($tca->tc_city)}}">
                                  </div>
                                </div>
                              </div>
                            </div>
                            @php
                              $add_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>
                          @php
                            $cp_count = 1;
                            $person_count = count($tcd->contact);
                          @endphp
                          @foreach ($tcd->contact as $tccp)
                            @php
                              $cpd_count = 1;
                            @endphp
                            <div data-target="edit_person_container_{{$tcd->id}}" id="edit_person_{{$cp_count}}_{{$tcd->id}}" class="row p-1">

                              <div class="col-lg-11">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Contact Person</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_id" value="{{$tccp->id}}">
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$tcd->id}} edit_person_{{$cp_count}}_name tc_name" placeholder="Full Name" value="{{ucwords($tccp->tc_cp_name)}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Position</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$tcd->id}} edit_person_{{$cp_count}}_position tc_position" placeholder="Position" value="{{ucwords($tccp->tc_cp_position)}}">
                                        @if ($person_count > 1)
                                          <div class="input-group-append">
                                            <button data-target="{{$tccp->id}}|{{ucwords($tccp->tc_cp_name)}}" type="button" class="btn btn-danger remove_current_person">
                                              <i class="fas fa-times"></i>
                                            </button>
                                          </div>
                                        @endif
                                      </div>
                                    </div>
                                  </div>

                                  @foreach ($tccp->detail as $cpd)
                                    @php
                                      $contact_count = count($tccp->detail);
                                    @endphp
                                    <div data-target="edit_contact_container_{{$tcd->id}}" id="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_{{$tcd->id}}" class="col-lg-6">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select data-target="edit_person_field_{{$cp_count}}" class="form-control edit_required_fields_{{$tcd->id}} edit_person_{{$cp_count}}_contact_type_{{$cpd_count}}">
                                              <option value="/Mobile-" {{($cpd->tc_cp_contact_type == "Mobile") ? "selected" : ""}}>Mobile</option>
                                              <option value="/Landline-" {{($cpd->tc_cp_contact_type == "Landline") ? "selected" : ""}}>Landline</option>
                                              <option value="/Fax-" {{($cpd->tc_cp_contact_type == "Fax") ? "selected" : ""}}>Fax</option>
                                              <option value="/Email-" {{($cpd->tc_cp_contact_type == "Email") ? "selected" : ""}}>Email</option>
                                              <option value="/WeChat-" {{($cpd->tc_cp_contact_type == "WeChat") ? "selected" : ""}}>WeChat</option>
                                            </select>
                                          </div>
                                          <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_id" value="{{$cpd->id}}">
                                          <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$tcd->id}} edit_person_{{$cp_count}}_contact_{{$cpd_count}}" placeholder="Contact Details" value="{{$cpd->tc_cp_contact}}">
                                          @if ($contact_count > 2)
                                            <div class="input-group-append">
                                              <button data-target="{{$cpd->id}}|{{$cpd->tc_cp_contact_type}}-{{ucwords($cpd->tc_cp_contact)}}" type="button" class="btn btn-danger remove_current_contact">
                                                <i class="fas fa-times"></i>
                                              </button>
                                            </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                    @php
                                      $cpd_count++;
                                    @endphp
                                  @endforeach
                                </div>
                              </div>

                              <div class="col-lg-1">
                                @if ($contact_count <= 3)
                                  <button type="button" class="btn btn-primary add_current_contact"
                                    data-toggle="modal" data-target="#additional_detail_{{$tccp->id}}">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                @endif
                              </div>

                              <div style="margin-top: 15%;" class="modal fade" id="additional_detail_{{$tccp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-xs" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header occolor_bg text-white">
                                      <h5 class="modal-title">Add Contact Detail - <b>{{ucwords($tccp->tc_cp_name)}}</b> </h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select class="form-control acd_fields_{{$tccp->id}} additional_contact_type_{{$tccp->id}}">
                                              <option value="">--Select--</option>
                                              <option value="Mobile">Mobile</option>
                                              <option value="Landline">Landline</option>
                                              <option value="Fax">Fax</option>
                                              <option value="Email">Email</option>
                                              <option value="WeChat">WeChat</option>
                                            </select>
                                          </div>
                                          <input type="text" class="form-control acd_fields_{{$tccp->id}} additional_contact_detail_{{$tccp->id}}" placeholder="Contact Details">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button id="{{$tccp->id}}" data-request="tc" type="button" class="btn btn-primary center add_contact_detail">Add</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            @php
                              $cp_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>

                          <div class="col-lg-12">
                            <div class="form-group half">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Terms</div>
                                </div>
                                <input data-target="tc_terms" type="number" class="form-control edit_required_fields_{{$tcd->id}} text-right" value="{{$tcd->tc_terms}}">
                                <div class="input-group-append">
                                  <div class="input-group-text"> <small>Days</small> </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Notes</div>
                                </div>
                                <textarea data-target="tc_note" class="form-control edit_required_fields_{{$tcd->id}}" rows="3">{{$tcd->tc_note}}</textarea>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 text-right">
                            <hr>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button data-target="{{$tcd->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                          </div>

                        </div>
                      </div>

                      <div class="tab-pane fade show" id="add_content_{{$tcd->id}}" role="tabpanel">

                        <input data-target="tc_id" type="hidden" class="additional_fields_{{$tcd->id}}" value="{{ucwords($tcd->id)}}">

                        <div data-target="add_address_container_{{$tcd->id}}" id="add_address_1_{{$tcd->id}}" class="row p-1 add_default_address_{{$tcd->id}}">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-130">
                                  <select data-target="add_address_field_1" class="form-control additional_fields_{{$tcd->id}} add_address_1_type">
                                    <option value="">--Select--</option>
                                    <option value="Office">Office</option>
                                    <option value="Warehouse">Warehouse</option>
                                    <option value="Home">Home</option>
                                  </select>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_address_1_address" placeholder="Address">
                                <div class="input-group-append">
                                  <button data-target="{{$tcd->id}}" type="button" class="btn btn-primary additional_address">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">City</div>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_address_1_city" placeholder="City">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 mb-4"></div>

                        <div data-target="add_person_container_{{$tcd->id}}" id="add_person_1_{{$tcd->id}}" class="row p-1 add_default_person_{{$tcd->id}}">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Contact Person</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_person_1_name" placeholder="Full Name">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Position</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_person_1_position" placeholder="Position">
                                <div class="input-group-append">
                                  <button data-target="{{$tcd->id}}" type="button" class="btn btn-primary additional_person">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$tcd->id}}" id="add_person_1_contact_1_{{$tcd->id}}" class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$tcd->id}} add_person_1_contact_type_1">
                                    <option value="/Mobile-" selected>Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-">Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_person_1_contact_1" placeholder="Contact Details">
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$tcd->id}}" id="add_person_1_contact_2_{{$tcd->id}}" class="col-lg-6 add_default_contact_{{$tcd->id}}">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$tcd->id}} add_person_1_contact_type_2">
                                    <option value="/Mobile-">Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-" selected>Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$tcd->id}} add_person_1_contact_2" placeholder="Contact Details">
                                <div class="input-group-append">
                                  <button name="add_person_1" data-target="{{$tcd->id}}" type="button" class="btn btn-primary additional_contact">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$tcd->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                        </div>

                      </div>

                      <div class="tab-pane fade show" id="plate_content_{{$tcd->id}}" role="tabpanel">

                        <div class="row">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <small>Registered Truck Plate Numbers</small><br>
                              <div class="input-group mb-2">
                                <input type="text" class="form-control plate_input add_plate_{{$tcd->id}}">
                                <div class="input-group-append">
                                  <button style="display: none;" data-target="{{$tcd->id}}" id="plate_details" type="button" class="btn btn-primary btn_add_plate add_plate">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 border_plate">
                            <div class="row">
                              <div class="col-lg-12 default_plate_container_{{$tcd->id}}"></div>
                              @foreach ($tcd->plate as $tc_plate)
                                <div class="col-lg-3 col-sm-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <input type="text" class="w-100" value="{{$tc_plate->tc_plate}}" readonly>
                                      <div class="input-group-append">
                                        <button data-target="{{$tc_plate->id}}|{{$tc_plate->tc_plate}}" type="button" class="btn btn-danger remove_current_plate">
                                          <i class="fas fa-times"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              @endforeach


                            </div>
                          </div>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script src="{{asset('js/crud/trucking-company-crud.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/controls/trucking-company-control.js')}}" charset="utf-8"></script>
@endsection
