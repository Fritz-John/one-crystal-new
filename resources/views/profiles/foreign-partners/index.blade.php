@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-header">
            <h1>Foreign Partners</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_fp_modal"> <i class="fas fa-plus"></i> Add Foreign Partner</button>

            <button type="button" id="fp_request" data-target="select_fp[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="fp_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>Country</th>
                  <th>Company Type</th>
                  <th>Contact Person</th>
                  <th>Position</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_foreign_partners as $foreign_partner)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($foreign_partner->fp_name)}}" type="checkbox" class="multi_container" name="select_fp[]" value="{{$foreign_partner->id}}">
                    </td>
                    <td>{{ucwords($foreign_partner->fp_name)}}</td>
                    <td>
                      @foreach ($foreign_partner->address as $fp_address)
                        {{ucwords($fp_address->fp_country)}}
                        @break
                      @endforeach
                    </td>
                    <td>{{ucwords($foreign_partner->fp_type)}}</td>
                    @foreach ($foreign_partner->contact as $fp_cp)
                      <td>{{ucwords($fp_cp->fp_cp_name)}}</td>
                      <td>{{ucwords($fp_cp->fp_cp_position)}}</td>
                      <td>
                        @foreach ($fp_cp->detail as $cp_detail)
                          @if ($cp_detail->fp_cp_contact_type == "Mobile")
                            {{$cp_detail->fp_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @foreach ($fp_cp->detail as $cp_detail)
                          @if ($cp_detail->fp_cp_contact_type == "Email")
                            {{$cp_detail->fp_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      @break
                    @endforeach
                    <td>
                      <button id="{{$foreign_partner->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_fp_modal_{{$foreign_partner->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$foreign_partner->id}}" type="button" data-type="fp-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($foreign_partner->fp_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

        <div class="modal fade" id="add_fp_modal" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
              <div class="modal-header occolor_bg text-white">
                <h5 class="modal-title">Add Foreign Partner</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="row">

                  <div class="col-lg-6">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Name</div>
                        </div>
                        <input type="text" class="form-control required_fields" name="fp_name" placeholder="Full Name">
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Type</div>
                        </div>
                        <select class="form-control required_fields" name="fp_type">
                          <option value="">--Select--</option>
                          <option value="Foreign Logistics">Foreign Logistics</option>
                          <option value="Factory">Factory</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div data-target="address_container" id="address_1" class="col-lg-12 p-1 default_address">
                    <div class="col-lg-12">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend w-130">
                            <select data-target="address_field_1" class="form-control required_fields address_1_type">
                              <option value="Office" selected>Office</option>
                              <option value="Warehouse">Warehouse</option>
                              <option value="Home">Home</option>
                            </select>
                          </div>
                          <input data-target="address_field_1" type="text" class="form-control required_fields address_1_address" placeholder="Address">
                          <div class="input-group-append">
                            <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                              <i class="fas fa-plus"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">Country</div>
                          </div>
                          <select data-target="address_field_1" class="form-control required_fields address_1_city">
                            <option value="">--Select--</option>
                            @foreach ($countries as $country)
                              <option value="{{$country}}">{{$country}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-12 mb-4"></div>

                  <div data-target="person_container" id="person_1" class="row p-1 default_person">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">Contact Person</div>
                          </div>
                          <input data-target="person_field_1" type="text" class="form-control required_fields person_1_name" placeholder="Full Name">
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text">Position</div>
                          </div>
                          <input data-target="person_field_1" type="text" class="form-control required_fields person_1_position" placeholder="Position">
                          <div class="input-group-append">
                            <button data-target="add_modal" type="button" class="btn btn-primary add_contact_person">
                              <i class="fas fa-plus"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div data-target="contact_container" id="person_1_contact_1" class="col-lg-6">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend w-120">
                            <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_1">
                              <option value="/Mobile-" selected>Mobile</option>
                              <option value="/Landline-">Landline</option>
                              <option value="/Fax-">Fax</option>
                              <option value="/Email-">Email</option>
                              <option value="/WeChat-">WeChat</option>
                            </select>
                          </div>
                          <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_1" placeholder="Contact Details">
                        </div>
                      </div>
                    </div>

                    <div data-target="contact_container" id="person_1_contact_2" class="col-lg-6 default_contact">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend w-120">
                            <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_2">
                              <option value="/Mobile-">Mobile</option>
                              <option value="/Landline-">Landline</option>
                              <option value="/Fax-">Fax</option>
                              <option value="/Email-" selected>Email</option>
                              <option value="/WeChat-">WeChat</option>
                            </select>
                          </div>
                          <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_2" placeholder="Contact Details">
                          <div class="input-group-append">
                            <button name="person_1" data-target="add_modal" type="button" class="btn btn-primary add_contact">
                              <i class="fas fa-plus"></i>
                            </button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-12 mb-4"></div>

                  <div class="col-lg-12">
                    <div class="form-group">
                      <div class="input-group mb-2">
                        <div class="input-group-prepend">
                          <div class="input-group-text">Notes</div>
                        </div>
                        <textarea class="form-control required_fields" rows="3" name="fp_note"></textarea>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
              </div>
            </div>
          </div>
        </div>

        @foreach ($get_fp_details as $fpd)
          <div class="modal fade" id="edit_fp_modal_{{$fpd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header occolor_bg text-white">
                  <h5 class="modal-title">Edit Foreign Partner</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <div class="row">

                    <div class="col-lg-12">
                      <div class="card card-primary card-outline card-outline-tabs">
                        <div class="card-header p-0 border-bottom-0">
                          <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" data-toggle="pill" href="#main_content_{{$fpd->id}}" role="tab" aria-selected="true">Main Details</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="pill" href="#add_content_{{$fpd->id}}" role="tab" aria-selected="false">Additional Details</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" data-toggle="pill" href="#shipper_content_{{$fpd->id}}" role="tab" aria-selected="false">Shipper Companies</a>
                            </li>
                          </ul>
                        </div>

                        <div class="card-body">

                          <div class="tab-content" id="custom-tabs-four-tabContent">

                            <div class="tab-pane fade show active" id="main_content_{{$fpd->id}}" role="tabpanel">
                              <div class="row">

                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Name</div>
                                      </div>
                                      <input data-target="fp_id" type="hidden" class="form-control edit_required_fields_{{$fpd->id}}" value="{{ucwords($fpd->id)}}">
                                      <input data-target="fp_name" type="text" class="form-control edit_required_fields_{{$fpd->id}}"  placeholder="Full Name" value="{{ucwords($fpd->fp_name)}}">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Type</div>
                                      </div>
                                      <select data-target="fp_type" class="form-control edit_required_fields_{{$fpd->id}}">
                                        <option value="">--Select--</option>
                                        <option value="Foreign Logistics" {{($fpd->fp_type == "Foreign Logistics") ? "selected" : ""}}>Foreign Logistics</option>
                                        <option value="Factory" {{($fpd->fp_type == "Factory") ? "selected" : ""}}>Factory</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>

                                @php
                                  $add_count = 1;
                                  $address_count = count($fpd->address);
                                @endphp
                                @foreach ($fpd->address as $fpa)
                                  <div data-target="edit_address_container_{{$fpd->id}}" id="edit_address_{{$add_count}}_{{$fpd->id}}" class="col-lg-12 p-1">
                                    <div class="col-lg-12">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-130">
                                            <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$fpd->id}} edit_address_{{$add_count}}_type">
                                              <option value="Office" {{($fpa->fp_address_type == "Office") ? "selected" : ""}}>Office</option>
                                              <option value="Warehouse" {{($fpa->fp_address_type == "Warehouse") ? "selected" : ""}}>Warehouse</option>
                                              <option value="Home" {{($fpa->fp_address_type == "Home") ? "selected" : ""}}>Home</option>
                                            </select>
                                          </div>
                                          <input data-target="edit_address_field_{{$add_count}}" type="hidden" class="edit_address_{{$add_count}}_id" value="{{$fpa->id}}">
                                          <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$fpd->id}} edit_address_{{$add_count}}_address" placeholder="Address" value="{{ucwords($fpa->fp_address)}}">
                                          @if ($address_count > 1)
                                            <div class="input-group-append">
                                              <button data-target="{{$fpa->id}}|{{ucwords($fpa->fp_address)}}" type="button" class="btn btn-danger remove_current_address">
                                                <i class="fas fa-times"></i>
                                              </button>
                                            </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>

                                    <div class="col-lg-6">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend">
                                            <div class="input-group-text">Country</div>
                                          </div>
                                          <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$fpd->id}} edit_address_{{$add_count}}_city">
                                            <option value="">--Select--</option>
                                            @foreach ($countries as $country)
                                              <option value="{{$country}}" {{($fpa->fp_country == $country) ? "selected" : ""}}>{{$country}}</option>
                                            @endforeach
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  @php
                                    $add_count++;
                                  @endphp
                                @endforeach

                                <div class="col-lg-12 mb-4"></div>
                                @php
                                  $cp_count = 1;
                                  $person_count = count($fpd->contact);
                                @endphp
                                @foreach ($fpd->contact as $fpcp)
                                  @php
                                    $cpd_count = 1;
                                  @endphp
                                  <div data-target="edit_person_container_{{$fpd->id}}" id="edit_person_{{$cp_count}}_{{$fpd->id}}" class="row p-1">
                                    <div class="col-lg-11">
                                      <div class="row">
                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">Contact Person</div>
                                              </div>
                                              <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_id" value="{{$fpcp->id}}">
                                              <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$fpd->id}} edit_person_{{$cp_count}}_name fp_name" placeholder="Full Name" value="{{ucwords($fpcp->fp_cp_name)}}">
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">Position</div>
                                              </div>
                                              <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$fpd->id}} edit_person_{{$cp_count}}_position fp_position" placeholder="Position" value="{{ucwords($fpcp->fp_cp_position)}}">
                                              @if ($person_count > 1)
                                                <div class="input-group-append">
                                                  <button data-target="{{$fpcp->id}}|{{ucwords($fpcp->fp_cp_name)}}" type="button" class="btn btn-danger remove_current_person">
                                                    <i class="fas fa-times"></i>
                                                  </button>
                                                </div>
                                              @endif
                                            </div>
                                          </div>
                                        </div>

                                        @foreach ($fpcp->detail as $cpd)
                                          @php
                                            $contact_count = count($fpcp->detail);
                                          @endphp
                                          <div data-target="edit_contact_container_{{$fpd->id}}" id="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_{{$fpd->id}}" class="col-lg-6">

                                            {{-- <div class="row"> --}}

                                                <div class="form-group">
                                                  <div class="input-group mb-2">
                                                    <div class="input-group-prepend w-120">
                                                      <select data-target="edit_person_field_{{$cp_count}}" class="form-control edit_required_fields_{{$fpd->id}} edit_person_{{$cp_count}}_contact_type_{{$cpd_count}}">
                                                        <option value="/Mobile-" {{($cpd->fp_cp_contact_type == "Mobile") ? "selected" : ""}}>Mobile</option>
                                                        <option value="/Landline-" {{($cpd->fp_cp_contact_type == "Landline") ? "selected" : ""}}>Landline</option>
                                                        <option value="/Fax-" {{($cpd->fp_cp_contact_type == "Fax") ? "selected" : ""}}>Fax</option>
                                                        <option value="/Email-" {{($cpd->fp_cp_contact_type == "Email") ? "selected" : ""}}>Email</option>
                                                        <option value="/WeChat-" {{($cpd->fp_cp_contact_type == "WeChat") ? "selected" : ""}}>WeChat</option>
                                                      </select>
                                                    </div>
                                                    <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_id" value="{{$cpd->id}}">
                                                    <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$fpd->id}} edit_person_{{$cp_count}}_contact_{{$cpd_count}}" placeholder="Contact Details" value="{{$cpd->fp_cp_contact}}">
                                                    @if ($contact_count > 2)
                                                      <div class="input-group-append">
                                                        <button data-target="{{$cpd->id}}|{{$cpd->fp_cp_contact_type}}-{{ucwords($cpd->fp_cp_contact)}}" type="button" class="btn btn-danger remove_current_contact">
                                                          <i class="fas fa-times"></i>
                                                        </button>
                                                      </div>
                                                    @endif
                                                  </div>
                                                </div>

                                            {{-- </div> --}}
                                          </div>
                                          @php
                                            $cpd_count++;
                                          @endphp
                                        @endforeach
                                      </div>
                                    </div>

                                    <div class="col-lg-1">
                                      @if ($contact_count <= 3)
                                        <button type="button" class="btn btn-primary add_current_contact"
                                          data-toggle="modal" data-target="#additional_detail_{{$fpcp->id}}">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                      @endif
                                    </div>

                                    <div style="margin-top: 15%;" class="modal fade" id="additional_detail_{{$fpcp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                      <div class="modal-dialog modal-xs" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header occolor_bg text-white">
                                            <h5 class="modal-title">Add Contact Detail - <b>{{ucwords($fpcp->fp_cp_name)}}</b> </h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <div class="form-group">
                                              <div class="input-group mb-2">
                                                <div class="input-group-prepend w-120">
                                                  <select class="form-control acd_fields_{{$fpcp->id}} additional_contact_type_{{$fpcp->id}}">
                                                    <option value="">--Select--</option>
                                                    <option value="Mobile">Mobile</option>
                                                    <option value="Landline">Landline</option>
                                                    <option value="Fax">Fax</option>
                                                    <option value="Email">Email</option>
                                                    <option value="WeChat">WeChat</option>
                                                  </select>
                                                </div>
                                                <input type="text" class="form-control acd_fields_{{$fpcp->id}} additional_contact_detail_{{$fpcp->id}}" placeholder="Contact Details">
                                              </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button id="{{$fpcp->id}}" data-request="fp" type="button" class="btn btn-primary center add_contact_detail">Add</button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>

                                  </div>
                                  @php
                                    $cp_count++;
                                  @endphp
                                @endforeach

                                <div class="col-lg-12 mb-4"></div>

                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Notes</div>
                                      </div>
                                      <textarea data-target="fp_note" class="form-control edit_required_fields_{{$fpd->id}}" rows="3">{{$fpd->fp_note}}</textarea>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12 text-right">
                                  <hr>
                                  <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                  <button data-target="{{$fpd->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                                </div>

                              </div>
                            </div>

                            <div class="tab-pane fade show" id="add_content_{{$fpd->id}}" role="tabpanel">

                              <input data-target="fp_id" type="hidden" class="additional_fields_{{$fpd->id}}" value="{{ucwords($fpd->id)}}">

                              <div data-target="add_address_container_{{$fpd->id}}" id="add_address_1_{{$fpd->id}}" class="row p-1 add_default_address_{{$fpd->id}}">
                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend w-130">
                                        <select data-target="add_address_field_1" class="form-control additional_fields_{{$fpd->id}} add_address_1_type">
                                          <option value="">--Select--</option>
                                          <option value="Office">Office</option>
                                          <option value="Warehouse">Warehouse</option>
                                          <option value="Home">Home</option>
                                        </select>
                                      </div>
                                      <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$fpd->id}} add_address_1_address" placeholder="Address">
                                      <div class="input-group-append">
                                        <button data-target="{{$fpd->id}}" type="button" class="btn btn-primary additional_address">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Country</div>
                                      </div>
                                      <select data-target="add_address_field_1" class="form-control additional_fields_{{$fpd->id}} add_address_1_city">
                                        <option value="">--Select--</option>
                                        @foreach ($countries as $country)
                                          <option value="{{$country}}">{{$country}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-12 mb-4"></div>

                              <div data-target="add_person_container_{{$fpd->id}}" id="add_person_1_{{$fpd->id}}" class="row p-1 add_default_person_{{$fpd->id}}">
                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Contact Person</div>
                                      </div>
                                      <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$fpd->id}} add_person_1_name" placeholder="Full Name">
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Position</div>
                                      </div>
                                      <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$fpd->id}} add_person_1_position" placeholder="Position">
                                      <div class="input-group-append">
                                        <button data-target="{{$fpd->id}}" type="button" class="btn btn-primary additional_person">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div data-target="add_contact_container_{{$fpd->id}}" id="add_person_1_contact_1_{{$fpd->id}}" class="col-lg-6">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend w-120">
                                        <select data-target="add_person_field_1" class="form-control additional_fields_{{$fpd->id}} add_person_1_contact_type_1">
                                          <option value="/Mobile-" selected>Mobile</option>
                                          <option value="/Landline-">Landline</option>
                                          <option value="/Fax-">Fax</option>
                                          <option value="/Email-">Email</option>
                                          <option value="/WeChat-">WeChat</option>
                                        </select>
                                      </div>
                                      <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$fpd->id}} add_person_1_contact_1" placeholder="Contact Details">
                                    </div>
                                  </div>
                                </div>

                                <div data-target="add_contact_container_{{$fpd->id}}" id="add_person_1_contact_2_{{$fpd->id}}" class="col-lg-6 add_default_contact_{{$fpd->id}}">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend w-120">
                                        <select data-target="add_person_field_1" class="form-control additional_fields_{{$fpd->id}} add_person_1_contact_type_2">
                                          <option value="/Mobile-">Mobile</option>
                                          <option value="/Landline-">Landline</option>
                                          <option value="/Fax-">Fax</option>
                                          <option value="/Email-" selected>Email</option>
                                          <option value="/WeChat-">WeChat</option>
                                        </select>
                                      </div>
                                      <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$fpd->id}} add_person_1_contact_2" placeholder="Contact Details">
                                      <div class="input-group-append">
                                        <button name="add_person_1" data-target="{{$fpd->id}}" type="button" class="btn btn-primary additional_contact">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-12 text-right">
                                <hr>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button data-target="{{$fpd->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                              </div>

                            </div>

                            <div class="tab-pane fade show" id="shipper_content_{{$fpd->id}}" role="tabpanel">

                              <div data-target="shipper_container_{{$fpd->id}}" id="shipper_1_{{$fpd->id}}" class="row default_shipper_{{$fpd->id}}">
                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Shipper Company</div>
                                      </div>
                                      <input class="form-control shipper_1_name shipper_fields_{{$fpd->id}}" type="text">
                                      <div class="input-group-append">
                                        <button data-target="{{$fpd->id}}" type="button" class="{{$fpd->shipper->count()}} btn btn-primary add_shipper">
                                          <i class="fas fa-plus"></i>
                                        </button>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="col-lg-12">
                                  <div class="form-group">
                                    <div class="input-group mb-2">
                                      <div class="input-group-prepend">
                                        <div class="input-group-text">Address</div>
                                      </div>
                                      <input class="form-control shipper_1_address shipper_fields_{{$fpd->id}}" type="text">
                                    </div>
                                  </div>
                                </div>
                              </div>

                              @php
                                $ship_count = 1;
                              @endphp
                              @foreach ($fpd->shipper as $fp_shipper)
                                @php
                                  $ship_count++;
                                @endphp
                                <div data-target="shipper_container_{{$fpd->id}}" id="shipper_{{$ship_count}}_{{$fpd->id}}" class="row">
                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Shipper Company</div>
                                        </div>
                                        <input class="form-control shipper_{{$ship_count}}_name shipper_fields_{{$fpd->id}}" type="text" value="{{ucwords($fp_shipper->fp_sc_name)}}">
                                        <div class="input-group-append">
                                          <button data-target="{{$fp_shipper->id}}|{{ucwords($fp_shipper->fp_sc_name)}}" type="button" class="btn btn-danger remove_current_shipper">
                                            <i class="fas fa-times"></i>
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-12">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Address</div>
                                        </div>
                                        <input class="form-control shipper_{{$ship_count}}_address shipper_fields_{{$fpd->id}}" type="text" value="{{$fp_shipper->fp_sc_address}}">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              @endforeach

                              <div class="col-lg-12 text-right">
                                <hr>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button data-target="{{$fpd->id}}" id="shipper_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                              </div>
                            </div>

                          </div>

                        </div>

                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>
        @endforeach

      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script>
    var countries = @json($countries);
    console.log(@json($get_fp_details));
  </script>
  <script src="{{asset('js/controls/foreign-partners-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/foreign-partners-crud.js')}}" charset="utf-8"></script>
@endsection
























{{--  --}}
