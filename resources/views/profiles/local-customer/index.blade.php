@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-header">
            <h1>Local Customers</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_lc_modal"> <i class="fas fa-plus"></i> Add Local Customer</button>
            <button type="button" id="lc_request" data-target="select_lc[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="lc_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>City</th>
                  <th>Contact Person</th>
                  <th>Position</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Terms</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_local_customers as $local_customer)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($local_customer->lc_name)}}" type="checkbox" class="multi_container" name="select_lc[]" value="{{$local_customer->id}}">
                    </td>
                    <td>{{ucwords($local_customer->lc_name)}}</td>
                    <td>
                      @foreach ($local_customer->address as $lc_address)
                        {{ucwords($lc_address->lc_city)}}
                        @break
                      @endforeach
                    </td>
                    @foreach ($local_customer->contact as $lc_cp)
                      <td>{{ucwords($lc_cp->lc_cp_name)}}</td>
                      <td>{{ucwords($lc_cp->lc_cp_position)}}</td>
                      <td>
                        @foreach ($lc_cp->detail as $cp_detail)
                          @if ($cp_detail->lc_cp_contact_type == "Mobile")
                            {{$cp_detail->lc_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @foreach ($lc_cp->detail as $cp_detail)
                          @if ($cp_detail->lc_cp_contact_type == "Email")
                            {{$cp_detail->lc_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      @break
                    @endforeach
                    <td>{{$local_customer->lc_terms}}</td>
                    <td>
                      <button id="{{$local_customer->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_lc_modal_{{$local_customer->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$local_customer->id}}" type="button" data-type="lc-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($local_customer->lc_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="modal fade" id="add_lc_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Local Customer</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="lc_name" placeholder="Full Name">
                </div>
              </div>
            </div>

            <div data-target="address_container" id="address_1" class="col-lg-12 p-1 default_address">
              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-130">
                      <select data-target="address_field_1" class="form-control required_fields address_1_type">
                        <option value="Office" selected>Office</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Home">Home</option>
                      </select>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_address" placeholder="Address">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">City</div>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_city" placeholder="City">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div data-target="person_container" id="person_1" class="row p-1 default_person">
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Contact Person</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_name" placeholder="Full Name">
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Position</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_position" placeholder="Position">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_contact_person">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_1" class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_1">
                        <option value="/Mobile-" selected>Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-">Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_1" placeholder="Contact Details">
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_2" class="col-lg-6 default_contact">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_2">
                        <option value="/Mobile-">Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-" selected>Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_2" placeholder="Contact Details">
                    <div class="input-group-append">
                      <button name="person_1" data-target="add_modal" type="button" class="btn btn-primary add_contact">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div class="col-lg-12">
              <div class="form-group half">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Terms</div>
                  </div>
                  <input type="number" class="form-control required_fields text-right" name="lc_terms">
                  <div class="input-group-append">
                    <div class="input-group-text"> <small>Days</small> </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group half">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Salesman</div>
                  </div>
                  <select class="form-control required_fields" name="lc_salesman">
                    <option value="">--Select--</option>
                    @foreach ($get_agents as $agent)
                      <option value="{{$agent->id}}">{{ucwords($agent->sa_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Notes</div>
                  </div>
                  <textarea class="form-control required_fields" rows="3" name="lc_note" ></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_lc_details as $lcd)
    <div class="modal fade" id="edit_lc_modal_{{$lcd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg text-white">
            <h5 class="modal-title">Edit Local Customer</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#main_content_{{$lcd->id}}" role="tab" aria-selected="true">Main Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#add_content_{{$lcd->id}}" role="tab" aria-selected="false">Additional Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#item_content_{{$lcd->id}}" role="tab" aria-selected="false">Items</a>
                      </li>
                    </ul>
                  </div>

                  <div class="card-body">

                    <div class="tab-content" id="custom-tabs-four-tabContent">

                      <div class="tab-pane fade show active" id="main_content_{{$lcd->id}}" role="tabpanel">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Name</div>
                                </div>
                                <input data-target="lc_id" type="hidden" class="form-control edit_required_fields_{{$lcd->id}}" value="{{ucwords($lcd->id)}}">
                                <input data-target="lc_name" type="text" class="form-control edit_required_fields_{{$lcd->id}}"  placeholder="Full Name" value="{{ucwords($lcd->lc_name)}}">
                              </div>
                            </div>
                          </div>
                          @php
                            $add_count = 1;
                            $address_count = count($lcd->address);
                          @endphp
                          @foreach ($lcd->address as $lca)
                            <div data-target="edit_address_container_{{$lcd->id}}" id="edit_address_{{$add_count}}_{{$lcd->id}}" class="col-lg-12 p-1">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend w-130">
                                      <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$lcd->id}} edit_address_{{$add_count}}_type">
                                        <option value="Office" {{($lca->lc_address_type == "Office") ? "selected" : ""}}>Office</option>
                                        <option value="Warehouse" {{($lca->lc_address_type == "Warehouse") ? "selected" : ""}}>Warehouse</option>
                                        <option value="Home" {{($lca->lc_address_type == "Home") ? "selected" : ""}}>Home</option>
                                      </select>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="hidden" class="edit_address_{{$add_count}}_id" value="{{$lca->id}}">
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$lcd->id}} edit_address_{{$add_count}}_address" placeholder="Address" value="{{ucwords($lca->lc_address)}}">
                                    @if ($address_count > 1)
                                      <div class="input-group-append">
                                        <button data-target="{{$lca->id}}|{{ucwords($lca->lc_address)}}" type="button" class="btn btn-danger remove_current_address">
                                          <i class="fas fa-times"></i>
                                        </button>
                                      </div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">City</div>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$lcd->id}} edit_address_{{$add_count}}_city" placeholder="City" value="{{ucwords($lca->lc_city)}}">
                                  </div>
                                </div>
                              </div>
                            </div>
                            @php
                              $add_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>
                          @php
                            $cp_count = 1;
                            $person_count = count($lcd->contact);
                          @endphp
                          @foreach ($lcd->contact as $lccp)
                            @php
                              $cpd_count = 1;
                            @endphp
                            <div data-target="edit_person_container_{{$lcd->id}}" id="edit_person_{{$cp_count}}_{{$lcd->id}}" class="row p-1">

                              <div class="col-lg-11">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Contact Person</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_id" value="{{$lccp->id}}">
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$lcd->id}} edit_person_{{$cp_count}}_name lc_name" placeholder="Full Name" value="{{ucwords($lccp->lc_cp_name)}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Position</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$lcd->id}} edit_person_{{$cp_count}}_position lc_position" placeholder="Position" value="{{ucwords($lccp->lc_cp_position)}}">
                                        @if ($person_count > 1)
                                          <div class="input-group-append">
                                            <button data-target="{{$lccp->id}}|{{ucwords($lccp->lc_cp_name)}}" type="button" class="btn btn-danger remove_current_person">
                                              <i class="fas fa-times"></i>
                                            </button>
                                          </div>
                                        @endif
                                      </div>
                                    </div>
                                  </div>

                                  @foreach ($lccp->detail as $cpd)
                                    @php
                                      $contact_count = count($lccp->detail);
                                    @endphp

                                    <div data-group="edit_contact_container_{{$lccp->id}}" data-target="edit_contact_container_{{$lcd->id}}" id="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_{{$lcd->id}}" class="col-lg-6">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select data-target="edit_person_field_{{$cp_count}}" class="form-control edit_required_fields_{{$lcd->id}} edit_person_{{$cp_count}}_contact_type_{{$cpd_count}}">
                                              <option value="/Mobile-" {{($cpd->lc_cp_contact_type == "Mobile") ? "selected" : ""}}>Mobile</option>
                                              <option value="/Landline-" {{($cpd->lc_cp_contact_type == "Landline") ? "selected" : ""}}>Landline</option>
                                              <option value="/Fax-" {{($cpd->lc_cp_contact_type == "Fax") ? "selected" : ""}}>Fax</option>
                                              <option value="/Email-" {{($cpd->lc_cp_contact_type == "Email") ? "selected" : ""}}>Email</option>
                                              <option value="/WeChat-" {{($cpd->lc_cp_contact_type == "WeChat") ? "selected" : ""}}>WeChat</option>
                                            </select>
                                          </div>
                                          <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_id" value="{{$cpd->id}}">
                                          <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$lcd->id}} edit_person_{{$cp_count}}_contact_{{$cpd_count}}" placeholder="Contact Details" value="{{$cpd->lc_cp_contact}}">
                                          @if ($contact_count > 2)
                                            <div class="input-group-append">
                                              <button data-target="{{$cpd->id}}|{{$cpd->lc_cp_contact_type}}-{{ucwords($cpd->lc_cp_contact)}}" type="button" class="btn btn-danger remove_current_contact">
                                                <i class="fas fa-times"></i>
                                              </button>
                                            </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                    @php
                                      $cpd_count++;
                                    @endphp
                                  @endforeach
                                </div>
                              </div>

                              <div class="col-lg-1 text-center">
                                @if ($contact_count <= 3)
                                  <button type="button" class="btn btn-primary add_current_contact"
                                    data-toggle="modal" data-target="#additional_detail_{{$lccp->id}}">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                @endif
                              </div>

                              <div style="margin-top: 15%;" class="modal fade" id="additional_detail_{{$lccp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-xs" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header occolor_bg text-white">
                                      <h5 class="modal-title">Add Contact Detail - <b>{{ucwords($lccp->lc_cp_name)}}</b> </h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select class="form-control acd_fields_{{$lccp->id}} additional_contact_type_{{$lccp->id}}">
                                              <option value="">--Select--</option>
                                              <option value="Mobile">Mobile</option>
                                              <option value="Landline">Landline</option>
                                              <option value="Fax">Fax</option>
                                              <option value="Email">Email</option>
                                              <option value="WeChat">WeChat</option>
                                            </select>
                                          </div>
                                          <input type="text" class="form-control acd_fields_{{$lccp->id}} additional_contact_detail_{{$lccp->id}}" placeholder="Contact Details">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button id="{{$lccp->id}}" data-request="lc" type="button" class="btn btn-primary center add_contact_detail">Add</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            @php
                              $cp_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>

                          <div class="col-lg-12">
                            <div class="form-group half">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Terms</div>
                                </div>
                                <input data-target="lc_terms" type="number" class="form-control edit_required_fields_{{$lcd->id}} text-right" value="{{$lcd->lc_terms}}">
                                <div class="input-group-append">
                                  <div class="input-group-text"> <small>Days</small> </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group half">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Salesman</div>
                                </div>
                                <select data-target="lc_salesman" class="form-control edit_required_fields_{{$lcd->id}}">
                                  <option value="">--Select--</option>
                                  @foreach ($get_agents as $agent)
                                    <option value="{{$agent->id}}" {{($agent->id == $lcd->lc_salesman) ? "selected" : ""}}>{{ucwords($agent->sa_name)}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Notes</div>
                                </div>
                                <textarea data-target="lc_note" class="form-control edit_required_fields_{{$lcd->id}}" rows="3">{{$lcd->lc_note}}</textarea>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 text-right">
                            <hr>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button data-target="{{$lcd->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                          </div>

                        </div>
                      </div>

                      <div class="tab-pane fade show" id="add_content_{{$lcd->id}}" role="tabpanel">

                        <input data-target="lc_id" type="hidden" class="additional_fields_{{$lcd->id}}" value="{{ucwords($lcd->id)}}">

                        <div data-target="add_address_container_{{$lcd->id}}" id="add_address_1_{{$lcd->id}}" class="row p-1 add_default_address_{{$lcd->id}}">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-130">
                                  <select data-target="add_address_field_1" class="form-control additional_fields_{{$lcd->id}} add_address_1_type">
                                    <option value="">--Select--</option>
                                    <option value="Office">Office</option>
                                    <option value="Warehouse">Warehouse</option>
                                    <option value="Home">Home</option>
                                  </select>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_address_1_address" placeholder="Address">
                                <div class="input-group-append">
                                  <button data-target="{{$lcd->id}}" type="button" class="btn btn-primary additional_address">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">City</div>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_address_1_city" placeholder="City">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 mb-4"></div>

                        <div data-target="add_person_container_{{$lcd->id}}" id="add_person_1_{{$lcd->id}}" class="row p-1 add_default_person_{{$lcd->id}}">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Contact Person</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_person_1_name" placeholder="Full Name">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Position</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_person_1_position" placeholder="Position">
                                <div class="input-group-append">
                                  <button data-target="{{$lcd->id}}" type="button" class="btn btn-primary additional_person">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$lcd->id}}" id="add_person_1_contact_1_{{$lcd->id}}" class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$lcd->id}} add_person_1_contact_type_1">
                                    <option value="/Mobile-" selected>Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-">Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_person_1_contact_1" placeholder="Contact Details">
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$lcd->id}}" id="add_person_1_contact_2_{{$lcd->id}}" class="col-lg-6 add_default_contact_{{$lcd->id}}">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$lcd->id}} add_person_1_contact_type_2">
                                    <option value="/Mobile-">Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-" selected>Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$lcd->id}} add_person_1_contact_2" placeholder="Contact Details">
                                <div class="input-group-append">
                                  <button name="add_person_1" data-target="{{$lcd->id}}" type="button" class="btn btn-primary additional_contact">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$lcd->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                        </div>

                      </div>

                      <div class="tab-pane fade show" id="item_content_{{$lcd->id}}" role="tabpanel">

                        <div class="col-lg-12">
                          <table class="table table-bordered table-striped table-hover">
                            <thead>
                              <tr>
                                <th>Container No.</th>
                                <th>Item</th>
                                <th>Package Price</th>
                                <th>Last Delivery Date</th>
                              </tr>
                            </thead>
                            <tbody>
                              @foreach ($get_lc_items as $items)
                                <tr>
                                  <td>{{$items->container->ic_container_no}}</td>
                                  <td>{{ucwords($items->item->item_name)}}</td>
                                  <td>{{($items->package_price != null) ? $items->package_price : "--"}}</td>
                                  <td>{{($items->last_delivery_date != null) ? $items->last_delivery_date : "--"}}</td>
                                </tr>
                              @endforeach
                            </tbody>
                          </table>
                        </div>

                        {{-- <div data-target="item_container_{{$lcd->id}}" id="item_1_{{$lcd->id}}" class="row default_item_{{$lcd->id}}">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Item</div>
                                </div>
                                <select data-target="item_list" class="form-control item_1_name item_fields_{{$lcd->id}}">
                                  <option value="">--Select--</option>
                                  @foreach ($get_items as $item)
                                    <option value="{{$item->id}}">{{ucwords($item->item_name)}}</option>
                                  @endforeach
                                </select>
                                <div class="input-group-append">
                                  <button data-target="{{$lcd->id}}" type="button" class="10 btn btn-primary add_item">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-3 text-center">
                            <small>Package Price</small><br>
                            <span>0.00</span>
                          </div>

                          <div class="col-lg-3 text-center">
                            <small>Last Delivery Date</small><br>
                            <span>--</span>
                          </div>
                        </div>

                        @php
                          $item_count = 1;
                        @endphp
                        @foreach ($lcd->item as $lc_items)
                            @php
                              $item_count++;
                            @endphp
                          <div data-target="item_container_{{$lcd->id}}" id="item_{{$item_count}}_{{$lcd->id}}" class="row">
                            <div class="col-lg-6">
                              <div class="form-group">
                                <div class="input-group mb-2">
                                  <div class="input-group-prepend">
                                    <div class="input-group-text">Item</div>
                                  </div>
                                  <select data-target="item_list" class="form-control item_{{$item_count}}_name item_fields_{{$lcd->id}}">
                                    <option value="">--Select--</option>
                                    @foreach ($get_items as $item)
                                      <option value="{{$item->id}}" {{($lc_items->item_id == $item->id) ? "selected" : ""}}>{{ucwords($item->item_name)}}</option>
                                    @endforeach
                                  </select>
                                  <div class="input-group-append">
                                    <button data-target="{{$lc_items->id}}|{{ucwords($lc_items->item->item_name)}}" type="button" class="btn btn-danger remove_current_item">
                                      <i class="fas fa-times"></i>
                                    </button>
                                  </div>
                                </div>
                              </div>
                            </div>

                            <div class="col-lg-3 text-center">
                              <script type="text/javascript">
                                console.log(@json($lc_items->item->container->ic_package_price));
                              </script>
                              <small>Package Price</small><br>
                            </div>

                            <div class="col-lg-3 text-center">
                              <small>Last Delivery Date</small><br>
                            </div>
                          </div>
                        @endforeach --}}

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          {{-- <button data-target="{{$lcd->id}}" id="item_details" type="button" class="btn occolor_bg text-white btn_save">Add</button> --}}
                        </div>
                      </div>

                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script>
    var items = @json($get_items);
    console.log(@json($get_lc_items));
  </script>
  <script src="{{asset('js/controls/local-customer-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/local-customer-crud.js')}}" charset="utf-8"></script>
@endsection



























{{--  --}}
