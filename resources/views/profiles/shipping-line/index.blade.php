@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Shipping Lines</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_sl_modal"> <i class="fas fa-plus"></i> Add Shipping Line</button>

            <button type="button" id="sl_request" data-target="select_sl[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="sl_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>City</th>
                  <th>Contact Person</th>
                  <th>Position</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Terms</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_shipping_lines as $shipping_line)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($shipping_line->sl_name)}}" type="checkbox" class="multi_container" name="select_sl[]" value="{{$shipping_line->id}}">
                    </td>
                    <td>{{ucwords($shipping_line->sl_name)}}</td>
                    <td>
                      @foreach ($shipping_line->address as $sl_address)
                        {{ucwords($sl_address->sl_city)}}
                        @break
                      @endforeach
                    </td>
                    @foreach ($shipping_line->contact as $sl_cp)
                      <td>{{ucwords($sl_cp->sl_cp_name)}}</td>
                      <td>{{ucwords($sl_cp->sl_cp_position)}}</td>
                      <td>
                        @foreach ($sl_cp->detail as $cp_detail)
                          @if ($cp_detail->sl_cp_contact_type == "Mobile")
                            {{$cp_detail->sl_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      <td>
                        @foreach ($sl_cp->detail as $cp_detail)
                          @if ($cp_detail->sl_cp_contact_type == "Email")
                            {{$cp_detail->sl_cp_contact}}
                            @break
                          @endif
                        @endforeach
                      </td>
                      @break
                    @endforeach
                    <td>{{$shipping_line->sl_terms}}</td>
                    <td>
                      <button id="{{$shipping_line->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_sl_modal_{{$shipping_line->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$shipping_line->id}}" type="button" data-type="sl-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($shipping_line->sl_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>

        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="add_sl_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Shipping Line</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="sl_name" placeholder="Full Name">
                </div>
              </div>
            </div>

            <div data-target="address_container" id="address_1" class="col-lg-12 p-1 default_address">
              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-130">
                      <select data-target="address_field_1" class="form-control required_fields address_1_type">
                        <option value="Office" selected>Office</option>
                        <option value="Warehouse">Warehouse</option>
                        <option value="Home">Home</option>
                      </select>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_address" placeholder="Address">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">City</div>
                    </div>
                    <input data-target="address_field_1" type="text" class="form-control required_fields address_1_city" placeholder="City">
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div data-target="person_container" id="person_1" class="row p-2 default_person">
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Contact Person</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_name" placeholder="Full Name">
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text">Position</div>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_position" placeholder="Position">
                    <div class="input-group-append">
                      <button data-target="add_modal" type="button" class="btn btn-primary add_contact_person">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_1" class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_1">
                        <option value="/Mobile-" selected>Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-">Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_1" placeholder="Contact Details">
                  </div>
                </div>
              </div>

              <div data-target="contact_container" id="person_1_contact_2" class="col-lg-6 default_contact">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend w-120">
                      <select data-target="person_field_1" class="form-control required_fields person_1_contact_type_2">
                        <option value="/Mobile-">Mobile</option>
                        <option value="/Landline-">Landline</option>
                        <option value="/Fax-">Fax</option>
                        <option value="/Email-" selected>Email</option>
                        <option value="/WeChat-">WeChat</option>
                      </select>
                    </div>
                    <input data-target="person_field_1" type="text" class="form-control required_fields person_1_contact_2" placeholder="Contact Details">
                    <div class="input-group-append">
                      <button name="person_1" data-target="add_modal" type="button" class="btn btn-primary add_contact">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div class="col-lg-12">
              <div class="form-group half">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Terms</div>
                  </div>
                  <input type="number" class="form-control required_fields text-right" name="sl_terms">
                  <div class="input-group-append">
                    <div class="input-group-text"> <small>Days</small> </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Notes</div>
                  </div>
                  <textarea class="form-control required_fields" rows="3" name="sl_note" ></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_sl_details as $sld)
    <div class="modal fade" id="edit_sl_modal_{{$sld->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg text-white">
            <h5 class="modal-title">Edit Shipping Line</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">

              <div class="col-lg-12">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#main_content_{{$sld->id}}" role="tab" aria-selected="true">Main Details</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#add_content_{{$sld->id}}" role="tab" aria-selected="false">Additional Details</a>
                      </li>
                    </ul>
                  </div>

                  <div class="card-body">

                    <div class="tab-content" id="custom-tabs-four-tabContent">

                      <div class="tab-pane fade show active" id="main_content_{{$sld->id}}" role="tabpanel">
                        <div class="row">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Name</div>
                                </div>
                                <input data-target="sl_id" type="hidden" class="form-control edit_required_fields_{{$sld->id}}" value="{{ucwords($sld->id)}}">
                                <input data-target="sl_name" type="text" class="form-control edit_required_fields_{{$sld->id}}"  placeholder="Full Name" value="{{ucwords($sld->sl_name)}}">
                              </div>
                            </div>
                          </div>
                          @php
                            $add_count = 1;
                            $address_count = count($sld->address);
                          @endphp
                          @foreach ($sld->address as $sla)
                            <div data-target="edit_address_container_{{$sld->id}}" id="edit_address_{{$add_count}}_{{$sld->id}}" class="col-lg-12 p-1">
                              <div class="col-lg-12">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend w-130">
                                      <select data-target="edit_address_field_{{$add_count}}" class="form-control edit_required_fields_{{$sld->id}} edit_address_{{$add_count}}_type">
                                        <option value="Office" {{($sla->sl_address_type == "Office") ? "selected" : ""}}>Office</option>
                                        <option value="Warehouse" {{($sla->sl_address_type == "Warehouse") ? "selected" : ""}}>Warehouse</option>
                                        <option value="Home" {{($sla->sl_address_type == "Home") ? "selected" : ""}}>Home</option>
                                      </select>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="hidden" class="edit_address_{{$add_count}}_id" value="{{$sla->id}}">
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$sld->id}} edit_address_{{$add_count}}_address" placeholder="Address" value="{{ucwords($sla->sl_address)}}">
                                    @if ($address_count > 1)
                                      <div class="input-group-append">
                                        <button data-target="{{$sla->id}}|{{ucwords($sla->sl_address)}}" type="button" class="btn btn-danger remove_current_address">
                                          <i class="fas fa-times"></i>
                                        </button>
                                      </div>
                                    @endif
                                  </div>
                                </div>
                              </div>

                              <div class="col-lg-6">
                                <div class="form-group">
                                  <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                      <div class="input-group-text">City</div>
                                    </div>
                                    <input data-target="edit_address_field_{{$add_count}}" type="text" class="form-control edit_required_fields_{{$sld->id}} edit_address_{{$add_count}}_city" placeholder="City" value="{{ucwords($sla->sl_city)}}">
                                  </div>
                                </div>
                              </div>
                            </div>
                            @php
                              $add_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>
                          @php
                            $cp_count = 1;
                            $person_count = count($sld->contact);
                          @endphp
                          @foreach ($sld->contact as $slcp)
                            @php
                              $cpd_count = 1;
                            @endphp
                            <div data-target="edit_person_container_{{$sld->id}}" id="edit_person_{{$cp_count}}_{{$sld->id}}" class="row p-1">

                              <div class="col-lg-11">
                                <div class="row">
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Contact Person</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_id" value="{{$slcp->id}}">
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$sld->id}} edit_person_{{$cp_count}}_name sl_name" placeholder="Full Name" value="{{ucwords($slcp->sl_cp_name)}}">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="col-lg-6">
                                    <div class="form-group">
                                      <div class="input-group mb-2">
                                        <div class="input-group-prepend">
                                          <div class="input-group-text">Position</div>
                                        </div>
                                        <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$sld->id}} edit_person_{{$cp_count}}_position sl_position" placeholder="Position" value="{{ucwords($slcp->sl_cp_position)}}">
                                        @if ($person_count > 1)
                                          <div class="input-group-append">
                                            <button data-target="{{$slcp->id}}|{{ucwords($slcp->sl_cp_name)}}" type="button" class="btn btn-danger remove_current_person">
                                              <i class="fas fa-times"></i>
                                            </button>
                                          </div>
                                        @endif
                                      </div>
                                    </div>
                                  </div>

                                  @foreach ($slcp->detail as $cpd)
                                    @php
                                      $contact_count = count($slcp->detail);
                                    @endphp
                                    <div data-target="edit_contact_container_{{$sld->id}}" id="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_{{$sld->id}}" class="col-lg-6">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select data-target="edit_person_field_{{$cp_count}}" class="form-control edit_required_fields_{{$sld->id}} edit_person_{{$cp_count}}_contact_type_{{$cpd_count}}">
                                              <option value="/Mobile-" {{($cpd->sl_cp_contact_type == "Mobile") ? "selected" : ""}}>Mobile</option>
                                              <option value="/Landline-" {{($cpd->sl_cp_contact_type == "Landline") ? "selected" : ""}}>Landline</option>
                                              <option value="/Fax-" {{($cpd->sl_cp_contact_type == "Fax") ? "selected" : ""}}>Fax</option>
                                              <option value="/Email-" {{($cpd->sl_cp_contact_type == "Email") ? "selected" : ""}}>Email</option>
                                              <option value="/WeChat-" {{($cpd->sl_cp_contact_type == "WeChat") ? "selected" : ""}}>WeChat</option>
                                            </select>
                                          </div>
                                          <input data-target="edit_person_field_{{$cp_count}}" type="hidden" class="edit_person_{{$cp_count}}_contact_{{$cpd_count}}_id" value="{{$cpd->id}}">
                                          <input data-target="edit_person_field_{{$cp_count}}" type="text" class="form-control edit_required_fields_{{$sld->id}} edit_person_{{$cp_count}}_contact_{{$cpd_count}}" placeholder="Contact Details" value="{{$cpd->sl_cp_contact}}">
                                          @if ($contact_count > 2)
                                            <div class="input-group-append">
                                              <button data-target="{{$cpd->id}}|{{$cpd->sl_cp_contact_type}}-{{ucwords($cpd->sl_cp_contact)}}" type="button" class="btn btn-danger remove_current_contact">
                                                <i class="fas fa-times"></i>
                                              </button>
                                            </div>
                                          @endif
                                        </div>
                                      </div>
                                    </div>
                                    @php
                                      $cpd_count++;
                                    @endphp
                                  @endforeach
                                </div>
                              </div>

                              <div class="col-lg-1">
                                @if ($contact_count <= 3)
                                  <button type="button" class="btn btn-primary add_current_contact"
                                    data-toggle="modal" data-target="#additional_detail_{{$slcp->id}}">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                @endif
                              </div>

                              <div style="margin-top: 15%;" class="modal fade" id="additional_detail_{{$slcp->id}}" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-xs" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header occolor_bg text-white">
                                      <h5 class="modal-title">Add Contact Detail - <b>{{ucwords($slcp->sl_cp_name)}}</b> </h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-group">
                                        <div class="input-group mb-2">
                                          <div class="input-group-prepend w-120">
                                            <select class="form-control acd_fields_{{$slcp->id}} additional_contact_type_{{$slcp->id}}">
                                              <option value="">--Select--</option>
                                              <option value="Mobile">Mobile</option>
                                              <option value="Landline">Landline</option>
                                              <option value="Fax">Fax</option>
                                              <option value="Email">Email</option>
                                              <option value="WeChat">WeChat</option>
                                            </select>
                                          </div>
                                          <input type="text" class="form-control acd_fields_{{$slcp->id}} additional_contact_detail_{{$slcp->id}}" placeholder="Contact Details">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button id="{{$slcp->id}}" data-request="sl" type="button" class="btn btn-primary center add_contact_detail">Add</button>
                                    </div>
                                  </div>
                                </div>
                              </div>

                            </div>
                            @php
                              $cp_count++;
                            @endphp
                          @endforeach

                          <div class="col-lg-12 mb-4"></div>

                          <div class="col-lg-12">
                            <div class="form-group half">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Terms</div>
                                </div>
                                <input data-target="sl_terms" type="number" class="form-control edit_required_fields_{{$sld->id}} text-right" value="{{$sld->sl_terms}}">
                                <div class="input-group-append">
                                  <div class="input-group-text"> <small>Days</small> </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Notes</div>
                                </div>
                                <textarea data-target="sl_note" class="form-control edit_required_fields_{{$sld->id}}" rows="3">{{$sld->sl_note}}</textarea>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-12 text-right">
                            <hr>
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            <button data-target="{{$sld->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                          </div>

                        </div>
                      </div>

                      <div class="tab-pane fade show" id="add_content_{{$sld->id}}" role="tabpanel">

                        <input data-target="sl_id" type="hidden" class="additional_fields_{{$sld->id}}" value="{{ucwords($sld->id)}}">

                        <div data-target="add_address_container_{{$sld->id}}" id="add_address_1_{{$sld->id}}" class="row p-1 add_default_address_{{$sld->id}}">
                          <div class="col-lg-12">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-130">
                                  <select data-target="add_address_field_1" class="form-control additional_fields_{{$sld->id}} add_address_1_type">
                                    <option value="">--Select--</option>
                                    <option value="Office">Office</option>
                                    <option value="Warehouse">Warehouse</option>
                                    <option value="Home">Home</option>
                                  </select>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_address_1_address" placeholder="Address">
                                <div class="input-group-append">
                                  <button data-target="{{$sld->id}}" type="button" class="btn btn-primary additional_address">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">City</div>
                                </div>
                                <input data-target="add_address_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_address_1_city" placeholder="City">
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 mb-4"></div>

                        <div data-target="add_person_container_{{$sld->id}}" id="add_person_1_{{$sld->id}}" class="row p-1 add_default_person_{{$sld->id}}">
                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Contact Person</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_person_1_name" placeholder="Full Name">
                              </div>
                            </div>
                          </div>

                          <div class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                  <div class="input-group-text">Position</div>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_person_1_position" placeholder="Position">
                                <div class="input-group-append">
                                  <button data-target="{{$sld->id}}" type="button" class="btn btn-primary additional_person">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$sld->id}}" id="add_person_1_contact_1_{{$sld->id}}" class="col-lg-6">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$sld->id}} add_person_1_contact_type_1">
                                    <option value="/Mobile-" selected>Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-">Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_person_1_contact_1" placeholder="Contact Details">
                              </div>
                            </div>
                          </div>

                          <div data-target="add_contact_container_{{$sld->id}}" id="add_person_1_contact_2_{{$sld->id}}" class="col-lg-6 add_default_contact_{{$sld->id}}">
                            <div class="form-group">
                              <div class="input-group mb-2">
                                <div class="input-group-prepend w-120">
                                  <select data-target="add_person_field_1" class="form-control additional_fields_{{$sld->id}} add_person_1_contact_type_2">
                                    <option value="/Mobile-">Mobile</option>
                                    <option value="/Landline-">Landline</option>
                                    <option value="/Fax-">Fax</option>
                                    <option value="/Email-" selected>Email</option>
                                    <option value="/WeChat-">WeChat</option>
                                  </select>
                                </div>
                                <input data-target="add_person_field_1" type="text" class="form-control additional_fields_{{$sld->id}} add_person_1_contact_2" placeholder="Contact Details">
                                <div class="input-group-append">
                                  <button name="add_person_1" data-target="{{$sld->id}}" type="button" class="btn btn-primary additional_contact">
                                    <i class="fas fa-plus"></i>
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                        <div class="col-lg-12 text-right">
                          <hr>
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                          <button data-target="{{$sld->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                        </div>

                      </div>

                    </div>

                  </div>

                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script src="{{asset('js/controls/shipping-line-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/shipping-line-crud.js')}}" charset="utf-8"></script>
@endsection
