@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">

    <div class="row">
      <div class="col-lg-12">

        <div class="card">
          <div class="card-header">
            <h1>Sales Agents</h1><br>
            <button type="button" class="btn btn-primary float_left mr-4" data-toggle="modal" data-target="#add_sales_agent_modal"> <i class="fas fa-plus"></i> Add Sales Agent</button>

            <button type="button" id="sa_request" data-target="select_sa[]"
            class="btn hide float_left
            {{($status == "Active") ? "btn-danger" : "btn-warning"}}
            {{($status == "Active") ? "btn_multi_retire" : "btn_multi_restore"}}">
            <i class="fas fa-times"></i>
             {{($status == "Active") ? "Retire" : "Restore"}}
           </button>

            <select id="sa_filter" class="float_right status_filter">
              <option value="Active" {{($status == "Active") ? "selected" : ""}}>Active</option>
              <option value="Retired" {{($status == "Retired") ? "selected" : ""}}>Retired</option>
            </select>
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover table-striped profile_table">
              <thead>
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th>Name</th>
                  <th>City</th>
                  <th>Mobile Number</th>
                  <th>Email Address</th>
                  <th>Com/Container</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_sales_agent as $sales_agent)
                  <tr>
                    <td class="text-center">
                      <input data-target="{{ucwords($sales_agent->sa_name)}}" type="checkbox" class="multi_container" name="select_sa[]" value="{{$sales_agent->id}}">
                    </td>
                    <td>{{ucwords($sales_agent->sa_name)}}</td>
                    @foreach ($sales_agent->address as $agent_address)
                      <td>{{ucwords($agent_address->sa_city)}}</td>
                    @break
                    @endforeach
                    <td>
                      @foreach ($sales_agent->contact as $agent_mobile)
                        @if ($agent_mobile->sa_contact_type == "Mobile")
                          {{$agent_mobile->sa_contact}}
                          @break
                        @endif
                      @endforeach
                    </td>
                    <td>
                      @foreach ($sales_agent->contact as $agent_mobile)
                        @if ($agent_mobile->sa_contact_type == "Email")
                          {{$agent_mobile->sa_contact}}
                          @break
                        @endif
                      @endforeach
                    </td>
                    <td>{{number_format($sales_agent->sa_commission,0,"",",")}}</td>
                    <td>
                      <button id="{{$sales_agent->id}}" type="button" class="btn btn-info {{($status == "Active") ? "" : "hide"}}" data-toggle="modal" data-target="#edit_sales_agent_modal_{{$sales_agent->id}}"> <i class="fas fa-edit"></i> Edit</button>
                      <button id="{{$sales_agent->id}}" type="button" data-type="sa-{{($status == "Active") ? "Retired" : "Active"}}"
                        data-target="{{strtoupper($sales_agent->sa_name)}}"
                        class="btn
                        {{($status == "Active") ? "btn-danger" : "btn-warning"}}
                        {{($status == "Active") ? "btn_retire" : "btn_restore"}}">
                        <i class="fas fa-times"></i> {{($status == "Active") ? "Retire" : "Restore"}}
                      </button>
                    </td>
                  </tr>

                  <div class="modal fade" id="edit_sales_agent_modal_{{$sales_agent->id}}" tabindex="-1" aria-hidden="true">

                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                        <div class="modal-header occolor_bg text-white">
                          <h5 class="modal-title">Edit Sales Agent</h5>
                          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">x</button>
                        </div>
                        <div class="modal-body">
                          <div class="row">

                            <div class="col-lg-12">
                              <div class="card card-primary card-outline card-outline-tabs">
                                <div class="card-header p-0 border-bottom-0">
                                  <ul class="nav nav-tabs" id="custom-tabs-four-tab" role="tablist">
                                    <li class="nav-item">
                                      <a class="nav-link active" id="main_tab" data-toggle="pill" href="#main_content_{{$sales_agent->id}}" role="tab" aria-selected="true">Main Details</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="add_tab" data-toggle="pill" href="#add_content_{{$sales_agent->id}}" role="tab" aria-selected="false">Additional Details</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link" id="company_tab" data-toggle="pill" href="#company_content_{{$sales_agent->id}}" role="tab" aria-selected="false">Company</a>
                                    </li>
                                  </ul>
                                </div>
                                <div class="card-body">
                                  <div class="tab-content" id="custom-tabs-four-tabContent">
                                    <div class="tab-pane fade show active" id="main_content_{{$sales_agent->id}}" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">Name</div>
                                              </div>
                                              <input type="text" class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_name" placeholder="Full Name" value="{{ucwords($sales_agent->sa_name)}}">
                                            </div>
                                          </div>
                                        </div>

                                        @foreach ($sales_agent->address as $agent_address)

                                          <input type="hidden" class="edit_required_fields_{{$sales_agent->id}}" name="agent_address_id" value="{{$agent_address->id}}">

                                          <div class="col-lg-12 address_{{$agent_address->id}}">
                                            <div class="form-group">
                                              <div class="input-group mb-2">
                                                <div class="input-group-prepend w-130">
                                                  <select class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_address_type[]">
                                                    @php
                                                      $office = "";
                                                      $warehouse = "";
                                                      $home = "";
                                                      switch ($agent_address->sa_address_type) {
                                                        case 'Office':
                                                          $office = "selected";
                                                          break;

                                                        case 'Office':
                                                          $warehouse = "selected";
                                                          break;

                                                        default:
                                                          $home = "selected";
                                                          break;
                                                      }
                                                    @endphp
                                                    <option value="Office" {{$office}}>Office</option>
                                                    <option value="Warehouse" {{$warehouse}}>Warehouse</option>
                                                    <option value="Home" {{$home}}>Home</option>
                                                  </select>
                                                </div>
                                                <input type="text" class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_address[]" placeholder="Address" value="{{$agent_address->sa_address}}">
                                                <div class="input-group-append">
                                                  <button data-target="{{$agent_address->id}}" type="button" class="btn btn-danger remove_current_address">
                                                    <i class="fas fa-times"></i>
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="col-lg-6 address_{{$agent_address->id}}">
                                            <div class="form-group">
                                              <div class="input-group mb-2">
                                                <div class="input-group-prepend">
                                                  <div class="input-group-text">City</div>
                                                </div>
                                                <input type="text" class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_city[]" placeholder="City" value="{{$agent_address->sa_city}}">
                                              </div>
                                            </div>
                                          </div>
                                        @endforeach


                                        <div class="col-lg-12 mb-4"></div>

                                        @foreach ($sales_agent->contact as $agent_contact)
                                          @php
                                            $mobile = "";
                                            $landline = "";
                                            $fax = "";
                                            $email = "";
                                            $wechat = "";
                                            switch ($agent_contact->sa_contact_type) {
                                              case 'Mobile':
                                                $mobile = "selected";
                                                break;

                                              case 'Landline':
                                                $landline = "selected";
                                                break;

                                              case 'Fax':
                                                $fax = "selected";
                                                break;

                                              case 'Email':
                                                $email = "selected";
                                                break;

                                              default:
                                                $wechat = "selected";
                                                break;
                                            }
                                          @endphp
                                          <div class="col-lg-6 contact_{{$agent_contact->id}}">
                                            <div class="form-group">
                                              <div class="input-group mb-2">
                                                <div class="input-group-prepend w-120">
                                                  <select class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_contact_type[]">
                                                    <option value="Mobile" {{$mobile}}>Mobile</option>
                                                    <option value="Landline" {{$landline}}>Landline</option>
                                                    <option value="Fax" {{$fax}}>Fax</option>
                                                    <option value="Email" {{$email}}>Email</option>
                                                    <option value="WeChat" {{$wechat}}>WeChat</option>
                                                  </select>
                                                </div>
                                                <input type="text" class="form-control edit_required_fields_{{$sales_agent->id}}" name="edit_agent_contact[]" placeholder="Contact Details" value={{$agent_contact->sa_contact}}>
                                                <div class="input-group-append">
                                                  <button data-target="{{$agent_contact->id}}" type="button" class="btn btn-danger remove_current_contact">
                                                    <i class="fas fa-times"></i>
                                                  </button>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        @endforeach

                                        <div class="col-lg-12 mb-4"></div>

                                        <div class="col-lg-6">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">
                                                  <small>Commission/Container</small>
                                                </div>
                                              </div>
                                              <input type="text" class="form-control text-right edit_required_fields_{{$sales_agent->id}} number-separator"
                                              name="edit_agent_comm"
                                              value="{{number_format($sales_agent->sa_commission,0,"",",")}}">
                                              <div class="input-group-append">
                                                <div class="input-group-text">
                                                  <small>Per Container</small>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">Notes</div>
                                              </div>
                                              <textarea name="edit_agent_note" class="form-control edit_required_fields_{{$sales_agent->id}}" rows="3">{{$sales_agent->sa_note}}</textarea>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-12 text-right">
                                          <hr>
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                          <button data-target="{{$sales_agent->id}}" id="main_details" type="button" class="btn occolor_bg text-white btn_save">Save</button>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="tab-pane fade" id="add_content_{{$sales_agent->id}}" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                                      <div class="row">
                                        <div class="col-lg-12">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend w-130">
                                                <select class="form-control additional_fields_{{$sales_agent->id}}" name="edit_agent_address_type[]">
                                                  <option value="">--Select--</option>
                                                  <option value="Office">Office</option>
                                                  <option value="Warehouse">Warehouse</option>
                                                  <option value="Home">Home</option>
                                                </select>
                                              </div>
                                              <input type="text" class="form-control additional_fields_{{$sales_agent->id}}" name="edit_agent_address[]" placeholder="Address">
                                              <div class="input-group-append">
                                                <button data-target="edit_modal" type="button" class="btn btn-primary add_address">
                                                  <i class="fas fa-plus"></i>
                                                </button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-6 edit_default_address">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend">
                                                <div class="input-group-text">City</div>
                                              </div>
                                              <input type="text" class="form-control additional_fields_{{$sales_agent->id}}" name="edit_agent_city[]" placeholder="City">
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-12 mb-5"></div>

                                        <div class="col-lg-6 edit_default_contact">
                                          <div class="form-group">
                                            <div class="input-group mb-2">
                                              <div class="input-group-prepend w-120">
                                                <select class="form-control additional_fields_{{$sales_agent->id}}" name="edit_agent_contact_type[]">
                                                  <option value="">--Select--</option>
                                                  <option value="Mobile">Mobile</option>
                                                  <option value="Landline">Landline</option>
                                                  <option value="Fax">Fax</option>
                                                  <option value="Email">Email</option>
                                                  <option value="WeChat">WeChat</option>
                                                </select>
                                              </div>
                                              <input type="text" class="form-control additional_fields_{{$sales_agent->id}}" name="edit_agent_contact[]" placeholder="Contact Details">
                                              <div class="input-group-append">
                                                <button data-id="{{$sales_agent->id}}" data-target="edit_modal" type="button" class="btn btn-primary add_contact">
                                                  <i class="fas fa-plus"></i>
                                                </button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>

                                        <div class="col-lg-12 text-right">
                                          <hr>
                                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                          <button data-target="{{$sales_agent->id}}" id="add_details" type="button" class="btn occolor_bg text-white btn_save">Add</button>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="tab-pane fade" id="company_content_{{$sales_agent->id}}" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                                      <div class="row">
                                        <h5>Local Companies Handled</h5>
                                        @foreach ($sales_agent->customer as $customer)
                                          <div class="col-lg-4 border text-center">
                                            <b>{{ucwords($customer->lc_name)}}</b>
                                          </div>
                                        @endforeach
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <!-- /.card -->
                              </div>
                            </div>

                          </div>
                        </div>
                      </div>
                    </div>
                  </div>

                @endforeach
              </tbody>
            </table>
          </div>
        </div>

      </div>
    </div>

  </div>

  <div class="modal fade" id="add_sales_agent_modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Sales Agent</h5>
          <button type="button" class="btn-close" data-dismiss="modal" aria-label="Close">x</button>
        </div>
        <div class="modal-body">
          <div class="row">

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Name</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="agent_name" placeholder="Full Name">
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend w-130">
                    <select class="form-control required_fields" name="agent_address_type[]">
                      <option value="Office" selected>Office</option>
                      <option value="Warehouse">Warehouse</option>
                      <option value="Home">Home</option>
                    </select>
                  </div>
                  <input type="text" class="form-control required_fields" name="agent_address[]" placeholder="Address">
                  <div class="input-group-append">
                    <button data-target="add_modal" type="button" class="btn btn-primary add_address">
                      <i class="fas fa-plus"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-6 default_address">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">City</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="agent_city[]" placeholder="City">
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-4"></div>

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend w-120">
                    <select class="form-control required_fields" name="agent_contact_type[]">
                      <option value="Mobile" selected>Mobile</option>
                      <option value="Landline">Landline</option>
                      <option value="Fax">Fax</option>
                      <option value="Email">Email</option>
                      <option value="WeChat">WeChat</option>
                    </select>
                  </div>
                  <input type="text" class="form-control required_fields" name="agent_contact[]" placeholder="Contact Details">
                </div>
              </div>
            </div>

            <div class="col-lg-6 default_contact">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend w-120">
                    <select class="form-control required_fields" name="agent_contact_type[]">
                      <option value="Mobile">Mobile</option>
                      <option value="Landline">Landline</option>
                      <option value="Fax">Fax</option>
                      <option value="Email" selected>Email</option>
                      <option value="WeChat">WeChat</option>
                    </select>
                  </div>
                  <input type="text" class="form-control required_fields" name="agent_contact[]" placeholder="Contact Details">
                  <div class="input-group-append">
                    <button data-target="add_modal" type="button" class="btn btn-primary add_contact">
                      <i class="fas fa-plus"></i>
                    </button>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-5"></div>

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <small>Commission/Container</small>
                    </div>
                  </div>
                  <input type="text" class="form-control text-right required_fields number-separator" name="agent_comm">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <small>Per Container</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Notes</div>
                  </div>
                  <textarea name="agent_note" class="form-control required_fields" rows="3"></textarea>
                </div>
              </div>
            </div>

          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@section('scripts')
  <script src="{{asset('js/controls/sales-agent-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/sales-agent-crud.js')}}" charset="utf-8"></script>
@endsection






















{{--  --}}
