@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Delivered Containers</h1><br>
            <button type="button" id="to_delivered" data-target="select_dc[]" class="btn btn-success btn_multi_proceed hide float_left mr-1"> <i class="fas fa-sign-in-alt"></i> Mark as Delivered</button>
            <input class="float_right w-220 mr-1" id="dc_daterange" type="text" name="daterange"
            value="{{date("m/d/Y", strtotime($start))." - ".date("m/d/Y", strtotime($end))}}">
          </div>

          <div class="card-body">
            <table class="table table-bordered table-hover table-striped transactions_table">
              <thead class="occolor_header text-white">
                <tr>
                  <th></th>
                  <th><small>Delivery Date</small></th>
                  <th>
                    <small>Yard Date</small><br>
                    <small>Lead Time</small>
                  </th>
                  <th>
                    <small>B/L</small><br>
                    <small>P/L</small><br>
                    <small>ACFTA Rec'd</small>
                  </th>
                  <th>
                    <small>Consignee Name</small><br>
                    <small>Container No.</small><br>
                    <small>Container Size</small><br>
                  </th>
                  <th>
                    <small>No. of Packages</small><br>
                    <small>Item(s)</small><br>
                    <small>Container Wt.</small><br>
                  </th>
                  <th class="text-right">
                    <small>Shipping Lines</small><br>
                    <small>Container Deposit</small><br>
                    <small>Refund Status</small><br>
                  </th>
                  <th class="text-right">
                    <small>Customer Name</small><br>
                    <small>Package Price</small><br>
                    <small>Payment Status</small><br>
                  </th>
                  <th class="text-right">
                    <small>Brokerage Name</small><br>
                    <small>Package Cost</small><br>
                    <small>Payment Status</small><br>
                  </th>
                  <th class="text-right">
                    <small>Trucking Company</small><br>
                    <small>Trucking Charge</small><br>
                    <small>Payment Status</small><br>
                  </th>
                  <th class="text-right"><small>Gross Profit</small></th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_delivered_containers as $ic)
                  <tr>
                    <td class="text-center">
                      <button type="button" data-toggle="modal"
                      data-target="#edit_form_modal_{{$ic->id}}"
                      class="btn btn-info btn_edit_row" name="button">
                        <i class="fas fa-ellipsis-h"></i>
                      </button>
                    </td>
                    <td>
                      <span>
                        {{($ic->ic_delivery_date != null) ? date("m-d-Y", strtotime($ic->ic_delivery_date)) : "--"}}
                      </span>
                    </td>
                    <td>
                      <span>
                        {{($ic->ic_yard_date != null) ? date("m-d-Y", strtotime($ic->ic_yard_date)) : "--"}}
                      </span><br>
                      <span>
                        --
                      </span>
                    </td>
                    <td class="occolor_border">
                      <span data-toggle="modal" data-target="#bl_modal_{{$ic->id}}" class="clickable">
                        {{($ic->ic_bl_date != null) ? date("m-d-Y", strtotime($ic->ic_bl_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#pl_modal_{{$ic->id}}" class="occolor_td clickable">
                        {{($ic->ic_pl_date != null) ? date("m-d-Y", strtotime($ic->ic_pl_date)) : "--"}}
                      </span><br>
                      <span data-toggle="modal" data-target="#acfta_modal_{{$ic->id}}" class="clickable">
                        {{($ic->ic_acfta_date != null) ? date("m-d-Y", strtotime($ic->ic_acfta_date)) : "--"}}
                      </span><br>
                    </td>
                    <td class="occolor_border">
                      <span>
                        {{ucwords($ic->consignee->cc_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{$ic->ic_container_no}}
                      </span><br>
                      <span>
                        {{$ic->ic_container_size}}
                      </span><br>
                    </td>
                    <td>
                      <span>
                        {{number_format($ic->ic_packages,0,"",",")}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->item->item_name)}}
                      </span><br>
                      <span>
                        {{number_format($ic->ic_gross_weight,0,"",",")}}
                      </span><br>
                    </td>
                    <td class="text-right occolor_border">
                      <span>{{ucwords($ic->shipping->sl_name)}}</span><br>
                      <span class="occolor_td">--</span><br>
                      <span>--</span>
                    </td>
                    <td class="text-right occolor_border">
                      <span class="dark_td">{{ucwords($ic->customer->lc_name)}}</span><br>
                      <span data-toggle="modal" data-target="#price_modal_{{$ic->id}}" class="occolor_td clickable">
                        {{($ic->ic_package_price != null) ? number_format($ic->ic_package_price, 0, ".", ",") : "0.00"}}
                      </span><br>
                      <span class="text-red">--</span>
                    </td>
                    <td class="text-right">
                      @foreach ($get_brokerage as $cb)
                        @if ($cb->id == $ic->consignee->cb_id)
                          <span>{{ucwords($cb->cb_name)}}</span><br>
                        @endif
                      @endforeach
                      <span data-toggle="modal" data-target="#cost_modal_{{$ic->id}}" class="occolor_td clickable">
                        {{($ic->ic_package_cost != null) ? number_format($ic->ic_package_cost, 0, ".", ",") : "0.00"}}
                      </span><br>
                      <span>--</span>
                    </td>
                    <td class="text-right">
                      <span>{{($ic->ic_trucking_company != null) ? ucwords($ic->trucking->tc_name) : "--"}}</span><br>
                      <span data-toggle="modal" data-target="#charge_modal_{{$ic->id}}" class="occolor_td clickable">
                        {{($ic->ic_trucking_charge != null) ? number_format($ic->ic_trucking_charge, 0, ".", ",") : "0.00"}}
                      </span><br>
                      <span>--</span>
                    </td>
                    <td class="text-right occolor_border">
                      <span class="occolor_td_lg">0.00</span><br>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_delivered_containers as $icd)
    @php
      $file_modal_arr = array("bl", "pl", "acfta");
      $package_field_arr = array("price", "cost", "charge");
      $field_arr = array(
        "Debited Duties-dd", "Excise Tax-et", "Signing Fee-sf", "Consignee Fee-csf",
        "Shipping Line Expenses-sl", "Container Deposit-cd", "Cleaning Fee-clf", "Arraste & Wharfage-aw",
        "Arraste OT-aot", "OCOM OT-oot", "Section OT-sot", "Processing Fee-pf",
        "Marine Fee-mf", "PCCI OT-pct", "Xray OT-xot", "DEA OT-dot",
        "Storage Charge-sc", "Dumurrage Charge-dmc", "Detention Charge-dtc", "DTI Expenses-dti",
        "BAI Expenses-bai", "BFAR Expenses-bfar", "BPI Expenses-bpi",
      )
    @endphp

    @foreach ($file_modal_arr as $file)
      @php
        switch ($file) {
          case 'bl':
            $upload_date = $icd->ic_bl_date;
            $filepath = $icd->ic_bl_file;
            break;

          case 'pl':
            $upload_date = $icd->ic_pl_date;
            $filepath = $icd->ic_pl_file;
            break;

          default:
            $upload_date = $icd->ic_acfta_date;
            $filepath = $icd->ic_acfta_file;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$file}}_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title">{{ucwords($file)}} File</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <div class="input-group mb-2">
                      <div class="input-group-prepend">
                        <div class="input-group-text">Date Uploaded</div>
                      </div>
                      <input type="text" class="form-control" value="{{date("m-d-Y", strtotime($upload_date))}}" readonly>
                    </div>
                  </div><br>
                  @php
                    $explode_url = explode("/", $filepath);
                    $filename = $explode_url[2];
                  @endphp
                  <a href="{{asset($filepath)}}" download>{{$filename}}</a>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    @foreach ($package_field_arr as $pf)
      @php
        switch ($pf) {
          case 'price':
            $value = $ic->ic_package_price;
            $value2 = $ic->ic_items."-".$ic->ic_customer;
            break;

          case 'cost':
            $value = $ic->ic_package_cost;
            break;

          default:
            $value = $ic->ic_trucking_charge;
            break;
        }
      @endphp
      <div class="modal fade" id="{{$pf}}_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">{{($pf == "charge") ? "Trucking" : "Package"}} {{ucwords($pf)}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">{{ucwords($pf)}}</div>
                  </div>
                  <input type="text" data-target="{{$value2}}" class="form-control text-right number-separator container_fields {{$pf}}_{{$icd->id}}" value="{{$value}}" placeholder="0.00">
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-type="{{$pf}}" data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_expense">Done</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

    <div class="modal fade" id="edit_form_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg2">
            <h5 class="modal-title">Brokerage Cost Breakdown</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              @php
                $count = 0;
              @endphp
              <div class="col-lg-12">
                <div class="row">

                  @foreach ($field_arr as $field)
                    @php
                    $count++;
                    $explode_val = explode("-", $field);
                    $field_name = $explode_val[0];
                    $field_class = $explode_val[1];
                    $field_val = "";

                    switch ($field_class) {
                      case 'dd':
                      $field_val .= $icd->ic_debited_duties;
                      break;

                      case 'sl':
                      $field_val .= $icd->ic_sl_expenses;
                      break;

                      case 'cd':
                      $field_val .= $icd->ic_sl_container_deposit;
                      break;

                      default:
                      break;
                    }
                    @endphp
                    <div class="col-lg-4">
                      <div class="form-group">
                        <div class="input-group mb-2">
                          <div class="input-group-prepend">
                            <div class="input-group-text w-160"> <small>{{$field_name}}</small> </div>
                          </div>
                          <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                          <input type="text" class="form-control text-right number-separator required_fields {{$field_class}}_{{$icd->id}}" value="{{$field_val}}" placeholder="0.00">
                        </div>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>

              <div class="col-lg-12 text-center">
                <hr>
                <h3>Total Cost: <span class="ml-4 total_cost number-separator">0.00</span> </h3>
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button type="button" class="btn occolor_bg text-white btn_edit">Done</button>
          </div>
        </div>
      </div>
    </div>
  @endforeach

@endsection

@section('scripts')
  <script src="{{asset('js/controls/delivered-containers-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/delivered-containers-crud.js')}}" charset="utf-8"></script>
@endsection

























{{--  --}}
