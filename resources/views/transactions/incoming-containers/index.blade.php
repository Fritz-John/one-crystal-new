@extends('layouts.app')

@section('content')

  <div style="background: white;" class="content-wrapper">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h1>Incoming Containers</h1><br>
            <button type="button" class="float_left mr-4 btn btn-primary" data-toggle="modal" data-target="#add_ic_modal"> <i class="fas fa-plus"></i> Add Container</button>
            <button type="button" id="to_on_process" data-target="select_ic[]" class="float_left mr-1 btn btn-success btn_multi_proceed hide"> <i class="fas fa-sign-in-alt"></i> Proceed</button>
            <button type="button" class="float_left mr-1 btn btn-info btn_edit hide"> <i class="fas fa-edit"></i> Edit</button>
            <button type="button" id="ic_request" data-target="select_ic[]" class="float_left mr-1 btn btn-danger btn_multi_retire hide"> <i class="fas fa-times"></i> Delete</button>
            <input style="float: right; margin-right: 1em; width: 220px;" type="text" id="ic_daterange" name="daterange"
            value="{{date("m/d/Y", strtotime($start))." - ".date("m/d/Y", strtotime($end))}}">
          </div>
          <div class="card-body">
            <table class="table table-bordered table-hover table-striped transactions_table">
              <thead class="occolor_header text-white">
                <tr>
                  <th class="text-center">
                    <input type="checkbox" class="check_all">
                  </th>
                  <th><small>Transaction Date</small></th>
                  <th><small>ETA</small></th>
                  <th>
                    <small>B/L <i class="fas fa-asterisk"></i></small><br>
                    <small>P/L <i class="fas fa-asterisk"></i></small><br>
                    <small>ACFTA Rec'd <i class="fas fa-asterisk"></i></small>
                  </th>
                  <th>
                    <small>Shipping Lines</small><br>
                    <small>B/L Number</small><br>
                  </th>
                  <th>
                    <small>Port of Origin</small><br>
                    <small>Port of Destination</small><br>
                  </th>
                  <th>
                    <small>Consignee Name</small><br>
                    <small>Container No.</small><br>
                    <small>Container Size</small><br>
                  </th>
                  <th>
                    <small>No. of Packages</small><br>
                    <small>Item(s)</small><br>
                    <small>Container Wt.</small><br>
                  </th>
                  <th><small>Customer Name</small></th>
                  <th><small>Supplier Name</small></th>
                  <th><small>Trucking <br> Included?</small></th>
                  <th>
                    <small>Encoded By</small><br>
                    <small>ETA & ACFTA Rec by</small><br>
                  </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($get_incoming_containers as $ic)
                  <tr id="{{$ic->id}}">
                    <td class="text-center">
                      <input data-target="{{$ic->id}}" type="checkbox" class="multi_container" name="select_ic[]" value="{{$ic->id}}">
                    </td>
                    <td><span>{{date("m-d-Y", strtotime($ic->created_at))}}</span></td>
                    <td>
                      <span data-toggle="modal" data-target="#eta_date_modal_{{$ic->id}}" class="btn_eta_date clickable">
                        {{date("m-d-Y", strtotime($ic->ic_eta))}}
                      </span>
                    </td>
                    <td class="occolor_border">
                      <input type="hidden" class="bl_container_{{$ic->id}}" value="{{$ic->ic_bl_date}}">
                      <span data-toggle="modal" data-target="#bl_date_modal_{{$ic->id}}" class="btn_bl_date clickable">
                        {{($ic->ic_bl_date != null) ? date("m-d-Y", strtotime($ic->ic_bl_date)) : "--"}}
                      </span><br>

                      <input type="hidden" class="pl_container_{{$ic->id}}" value="{{$ic->ic_pl_date}}">
                      <span data-toggle="modal" data-target="#pl_date_modal_{{$ic->id}}" class="occolor_td btn_pl_date clickable">
                        {{($ic->ic_pl_date != null) ? date("m-d-Y", strtotime($ic->ic_pl_date)) : "--"}}
                      </span><br>

                      <input type="hidden" class="acfta_container_{{$ic->id}}" value="{{$ic->ic_acfta_date}}">
                      <span data-toggle="modal" data-target="#acfta_date_modal_{{$ic->id}}" class="btn_acfta_date clickable">
                        {{($ic->ic_acfta_date != null) ? date("m-d-Y", strtotime($ic->ic_acfta_date)) : "--"}}
                      </span><br>
                    </td>
                    <td class="occolor_border">
                      <span>{{ucwords($ic->shipping->sl_name)}}</span><br>
                      <span class="occolor_td">{{$ic->ic_blno}}</span><br>
                    </td>
                    <td>
                      <span>
                        {{ucwords($ic->loading->port_country)}} - {{ucwords($ic->loading->port_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->discharge->port_country)}} - {{ucwords($ic->discharge->port_name)}}
                      </span><br>
                    </td>
                    <td class="occolor_border">
                      <span>
                        {{ucwords($ic->consignee->cc_name)}}
                      </span><br>
                      <span class="occolor_td">
                        {{$ic->ic_container_no}}
                      </span><br>
                      <span>
                        {{$ic->ic_container_size}}
                      </span><br>
                    </td>
                    <td>
                      <span>
                        {{number_format($ic->ic_packages,0,"",",")}}
                      </span><br>
                      <span class="occolor_td">
                        {{ucwords($ic->item->item_name)}}
                      </span><br>
                      <span>
                        {{number_format($ic->ic_gross_weight,0,"",",")}}
                      </span><br>
                    </td>
                    <td class="dark_td occolor_border">{{ucwords($ic->customer->lc_name)}}</td>
                    <td>{{ucwords($ic->shipper->fp_sc_name)}}</td>
                    <td class="occolor_border">{{($ic->ic_external_trucking != null) ? $ic->ic_external_trucking : "No"}}</td>
                    <td class="occolor_border">
                      <span>{{ucwords($ic->encoder->name)}}</span><br>
                      <span class="eta_acfta_by_{{$ic->id}} occolor_td">{{($ic->ic_eta_acfta_by != null) ? ucwords($ic->recorder->name) : "--"}}</span><br>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="add_ic_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header occolor_bg text-white">
          <h5 class="modal-title">Add Container</h5>
          <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          <div class="row">

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-120">Shipper</div>
                  </div>
                  <select class="form-control required_fields" name="ic_shipper">
                    <option value="">--Select Shipper--</option>
                    @foreach ($get_shippers as $shipper)
                      <option value="{{$shipper->id}}">{{ucwords($shipper->fp_sc_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-120">Consignee</div>
                  </div>
                  <select class="form-control required_fields" name="ic_consignee">
                    <option value="">--Select Consignee--</option>
                    @foreach ($get_consignees as $consignee)
                      <option value="{{$consignee->id}}">{{ucwords($consignee->cc_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">Bill of Lading No.</div>
                  </div>
                  <input type="text" class="form-control required_fields" name="ic_blno" placeholder="B/L No.">
                </div>
              </div>

              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-150">Shipping Line</div>
                  </div>
                  <select class="form-control required_fields" name="ic_shipping_line">
                    <option value="">--Select Shipping Line--</option>
                    @foreach ($get_shipping_lines as $shipping_line)
                      <option value="{{$shipping_line->id}}">{{ucwords($shipping_line->sl_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-1"> <hr> </div>

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-120"> <small>Port of Loading</small> </div>
                  </div>
                  <select class="form-control required_fields" name="ic_loading_port">
                    <option value="">--Select Loading Port--</option>
                    @foreach ($get_ports as $port)
                      <option value="{{$port->id}}">{{ucwords($port->port_name)}} - {{ucwords($port->port_country)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-150">Port of Discharge</div>
                  </div>
                  <select class="form-control required_fields" name="ic_discharge_port">
                    <option value="">--Select Discharge Port--</option>
                    @foreach ($get_ports as $port)
                      <option value="{{$port->id}}">{{ucwords($port->port_name)}} - {{ucwords($port->port_country)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-1"> <hr> </div>

            <div data-target="container_container" id="container_1" class="row p-2 default_container">
              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text w-120"> <small>Container Size</small> </div>
                    </div>
                    <select class="form-control container_1_size">
                      <option value="">--Select Size--</option>
                      <option value="20">20</option>
                      <option value="40">40</option>
                      <option value="45">45</option>
                      <option value="LC">LC</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-lg-6">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text w-150"> <small>Description of Goods</small> </div>
                    </div>
                    <select class="form-control container_1_item">
                      <option value="">--Select Item--</option>
                      @foreach ($get_items as $item)
                        <option value="{{$item->id}}">{{ucwords($item->item_name)}}</option>
                      @endforeach
                    </select>
                    <div class="input-group-append">
                      <button name="container_1" type="button" class="btn btn-primary add_container">
                        <i class="fas fa-plus"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-lg-4">
                <input type="text" class="form-control container_1_no" placeholder="Container No.">
              </div>

              <div class="col-lg-4">
                <input type="text" class="form-control container_1_package number-separator" placeholder="No. of Packages">
              </div>

              <div class="col-lg-4">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <input type="text" class="form-control container_1_gross number-separator" placeholder="Gross Weight Cargo">
                    <div class="input-group-append">
                      <div class="input-group-text"> <small>Kg</small> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 mb-1"> <hr> </div>

            <div class="col-lg-8">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-200"> <small>This Shipment is for Customer</small> </div>
                  </div>
                  <select class="form-control required_fields" name="ic_local_customer">
                    <option value="">--Select Customer--</option>
                    @foreach ($get_local_customers as $local_customer)
                      <option value="{{$local_customer->id}}">{{ucwords($local_customer->lc_name)}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>

            <div class="col-lg-4">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-220">
                      <small>Does this Shipment need acfta?</small>
                    </div>
                  </div>
                  <input type="checkbox" class="form-control required_checks" name="ic_need_acfta" value="yes" checked>
                </div>
              </div>

              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text w-220">
                      <small>Handled by External Trucking?</small>
                    </div>
                  </div>
                  <input type="checkbox" class="form-control required_checks" name="ic_external_trucking" value="yes">
                </div>
              </div>
            </div>

            <div class="col-lg-12">
              <div class="form-group">
                <div class="input-group mb-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <small>Customer Request <br> for this <br> Shipment?</small>
                    </div>
                  </div>
                  <textarea class="form-control required_fields" rows="3" name="ic_customer_request"></textarea>
                </div>
              </div>
            </div>

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
          <button type="button" class="btn occolor_bg text-white btn_done">Done</button>
        </div>
      </div>
    </div>
  </div>

  @foreach ($get_incoming_containers as $icd)
    @php
      $modal_arr = array("bl", "pl", "acfta");
    @endphp
    <div class="modal fade" id="eta_date_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header occolor_bg2">
            <h5 class="modal-title">Update ETA Date</h5>
            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <div class="form-group">
                  <div class="input-group mb-2">
                    <div class="input-group-prepend">
                      <div class="input-group-text w-120">ETA</div>
                    </div>
                    <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                    <input type="date" class="form-control required_fields eta_date_{{$icd->id}}" name="eta_date" value="{{$icd->ic_eta}}">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_eta_done">Done</button>
          </div>
        </div>
      </div>
    </div>

    @foreach ($modal_arr as $modal)
      @php
        $modal_title = "";
        $file = "";
        switch ($modal) {
          case 'bl':
            $modal_title .= "B/L";
            $file .= $icd->ic_bl_file;
            break;

          case 'pl':
            $modal_title .= "P/L";
            $file .= $icd->ic_pl_file;
            break;

          default:
            $modal_title .= "ACFTA";
            $file .= $icd->ic_acfta_file;
            break;
        }
      @endphp

      <div class="modal fade" id="{{$modal}}_date_modal_{{$icd->id}}" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header occolor_bg2">
              <h5 class="modal-title">Upload {{$modal_title}} File</h5>
              <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="col-lg-12">
                  <input type="hidden" class="container_id_{{$icd->id}}" value="{{$icd->id}}">
                  <input type="file" class="{{$modal}}_file_{{$icd->id}}"><br><br>
                  @if ($file != null)
                    @php
                      $explode_url = explode("/", $file);
                      $filename = $explode_url[2];
                    @endphp
                    <a class="{{$modal}}_anchor_{{$icd->id}}" href="{{asset($file)}}" download>{{$filename}}</a>
                  @endif
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
              <button data-target="{{$icd->id}}" type="button" class="btn occolor_bg text-white btn_{{$modal}}_done">Done</button>
            </div>
          </div>
        </div>
      </div>
    @endforeach

  @endforeach

@endsection

@section('scripts')
  <script>
    var items = @json($get_items);
    var asset_path = @json(asset("/"));
  </script>
  <script src="{{asset('js/controls/incoming-containers-control.js')}}" charset="utf-8"></script>
  <script src="{{asset('js/crud/incoming-containers-crud.js')}}" charset="utf-8"></script>
@endsection






















{{--  --}}
