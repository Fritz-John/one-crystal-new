<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{asset('css/sweetalert2.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/buttons.dataTables.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/googleapis.nunito.css')}}">
        <link rel="stylesheet" href="{{asset('css/fontawesome-free/css/all.min.css')}}">
        <!-- Styles -->
        @yield("css")
        <link rel="stylesheet" href="{{asset('js/select2/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{asset('css/adminlte.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/custom.css')}}">
        <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}">
        @livewireStyles

        <!-- Scripts -->
        <script src="{{ mix('js/app.js') }}" defer></script>
    </head>
    <body class="font-sans antialiased hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed sidebar-collapse">
        <x-jet-banner />

        <div class="min-h-screen bg-gray-100">
            @livewire('navigation-menu')

            {{-- #2e4e63 --}}

            <aside class="main-sidebar sidebar-light-primary elevation-4">
              <!-- Brand Logo -->
              <a href="#" class="brand-link text-center">
                {{-- <img src="{{asset('images/OCE_Logo.png')}}" class="brand-image img-circle elevation-3"> --}}
                <span class="brand-text font-weight-light">One Crystal</span>
              </a>

              @php
                $item = "";
                $split_url = explode("/", Request::url());
                $group = $split_url[3];
                if ($group != "dashboard") {
                  $item = $split_url[4];
                }
              @endphp

              <!-- Sidebar -->
              <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                    <li class="nav-item">
                      <a href="/dashboard" class="nav-link {{($group == "dashboard") ? "active" : ""}}">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>
                          Dashboard
                          {{-- <span class="right badge badge-danger">New</span> --}}
                        </p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link {{($group == "profiles") ? "active" : ""}}">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                          Profiles
                          <i class="fas fa-angle-left right"></i>
                          {{-- <span class="badge badge-info right">6</span> --}}
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="/profiles/sales-agent" class="nav-link {{($item == "sales-agent") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Sales Agents</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/local-customer" class="nav-link {{($item == "local-customer") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Local Customers</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/foreign-partners" class="nav-link {{($item == "foreign-partners") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Foreign Partners</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/customs-brokerage" class="nav-link {{($item == "customs-brokerage") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Customs Brokerage</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/trucking-companies" class="nav-link {{($item == "trucking-companies") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Trucking Companies</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/shipping-lines" class="nav-link {{($item == "shipping-lines") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Shipping Lines</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/ports" class="nav-link {{($item == "ports") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Ports</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/profiles/items" class="nav-link {{($item == "items") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Items</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link {{($group == "transactions") ? "active" : ""}}">
                        <i class="nav-icon fas fa-cash-register"></i>
                        <p>
                          Transactions
                          <i class="right fas fa-angle-left"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="/transactions/incoming-containers" class="nav-link {{($item == "incoming-containers") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Incoming Containers</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/transactions/on-process" class="nav-link {{($item == "on-process") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>On Process</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/transactions/endorsed-to-trucking" class="nav-link {{($item == "endorsed-to-trucking") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Endorsed to Trucking</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="/transactions/delivered-containers" class="nav-link {{($item == "delivered-containers") ? "active" : ""}}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Delivered Containers</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-money-bill"></i>
                        <p>
                          Debts
                          <i class="fas fa-angle-left right"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="pages/UI/general.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Account Receivables</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/UI/icons.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p style="font-size: 13px;">Container Deposit Receivables</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/UI/buttons.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Account Payables <br> Customs Brokerage</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/UI/sliders.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Account Payables <br> Trucking</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/UI/modals.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Writed Off Bad Debts</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/UI/navbar.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Writed Off Payables</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-hand-holding-usd"></i>
                        <p>
                          Payments
                          <i class="fas fa-angle-left right"></i>
                        </p>
                      </a>
                      <ul class="nav nav-treeview">
                        <li class="nav-item">
                          <a href="pages/forms/general.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Payments Received</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/forms/advanced.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p style="font-size: 15px;">Container Deposit Received</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/forms/editors.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Payments to <br> Customs Brokerage</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="pages/forms/validation.html" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p>Payments to Trucking</p>
                          </a>
                        </li>
                      </ul>
                    </li>
                    <li class="nav-item">
                      <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                          Users
                          <i class="fas fa-angle-left right"></i>
                        </p>
                      </a>
                    </li>
                  </ul>
                </nav>
                <!-- /.sidebar-menu -->
              </div>
              <!-- /.sidebar -->
            </aside>

            <!-- Page Heading -->
            {{-- @if (isset($header))
                <header class="bg-white shadow">
                    <div class="max-w-12xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
                        {{ $header }}
                    </div>
                </header>
            @endif --}}
            <!-- Page Content -->
            <main>
                {{-- {{ $slot }} --}}
                @yield('content')
            </main>
        </div>

        @stack('modals')

        @livewireScripts
    </body>
</html>

<script src="{{asset('js/jquery/jquery.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/bootstrap/js/bootstrap.bundle.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/adminlte.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/jquery.dataTables.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/plugins/jszip.min.js')}}"></script>
<script src="{{asset('js/plugins/pdfmake.min.js')}}"></script>
<script src="{{asset('js/plugins/vfs_fonts.js')}}"></script>
<script src="{{asset('js/plugins/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/plugins/buttons.print.min.js')}}"></script>
<script src="{{asset('js/plugins/sweetalert2.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/moment.min.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/daterangepicker.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/easy-number-separator.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/ellipsis.js')}}" charset="utf-8"></script>
<script src="{{asset('js/plugins/Chart.min.js')}}" charset="utf-8"></script>
<script type="text/javascript">
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
@yield('scripts')
<script src="{{asset('js/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('js/custom.js')}}" charset="utf-8"></script>




















{{--  --}}
