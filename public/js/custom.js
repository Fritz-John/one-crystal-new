//STATUS FILTER - PROFILES
$(".status_filter").on("change", function(){

  var request_type = $(this).attr("id");
  var status = $(this).val();

  $.ajax({
    type: "post",
    url: "/set-status",
    data: {request_type: request_type, status: status},
    success: function(data){
      window.location.reload();
    },
  });

});

//DATERANGEPICKER
$('#ic_daterange').daterangepicker({
  opens: 'left'
}, function(start, end, label){
  $.ajax({
    type: "post",
    url: "/set-daterange",
    data: {request_type: "ic_daterange", start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
    dataType: "json",
    success: function(data){
      window.location.reload();
    },
  });
});

$('#dc_daterange').daterangepicker({
  opens: 'left'
}, function(start, end, label){
  $.ajax({
    type: "post",
    url: "/set-daterange",
    data: {request_type: "dc_daterange", start: start.format('YYYY-MM-DD'), end: end.format('YYYY-MM-DD')},
    dataType: "json",
    success: function(data){
      window.location.reload();
    },
  });
});

//SELECT ALL RECORDS (CHECKBOX CONTROLS)
$(".check_all").on("click", function(){

  if ($(this).is(":checked")) {
    $(".multi_container").each(function(){
      $(this).prop("checked", true);
    });
  }
  else {
    $(".multi_container").each(function(){
      $(this).prop("checked", false);
    });
  }

});

//REMOVE BORDER COLOR ONCHANGE ONKEYPRESS ETC
$(document).on("change keypress", ".required_fields", function(){
  $(this).css("border-color", "");
});

//SELECT RECORDS (RETIRE, RESTORE, PROCEED, REVERT, EDIT)
$(".multi_container, .check_all").on("click", function(){
  if ($('.multi_container:checked').length >= 2) {
    $(".btn_multi_proceed").fadeIn();
    $(".btn_multi_revert").fadeIn();
    $(".btn_multi_retire").fadeIn();
    $(".btn_multi_restore").fadeIn();
    $(".btn_edit").fadeOut();
  }
  else if ($('.multi_container:checked').length == 1){
    $(".btn_multi_proceed").fadeIn();
    $(".btn_multi_revert").fadeIn();
    $(".btn_multi_retire").fadeIn();
    $(".btn_multi_restore").fadeIn();
    $(".btn_edit").fadeIn();
  }
  else {
    $(".btn_multi_proceed").fadeOut();
    $(".btn_multi_revert").fadeOut();
    $(".btn_multi_retire").fadeOut();
    $(".btn_multi_restore").fadeOut();
    $(".btn_edit").fadeOut();
  }
});

//ADDITIONAL CONTACT DETAIL FOR ALL CURRENT CONTACT PERSON (PROFILES)
$(".add_contact_detail").on("click", function(){

  var cp_id = $(this).attr("id");
  var request_type = $(this).attr("data-request");
  var contact_type = $(".additional_contact_type_"+cp_id).val();
  var contact_detail = $(".additional_contact_detail_"+cp_id).val();

  if (contact_type != "" && contact_detail != "") {

    $.ajax({
      type: "post",
      url: "/additional-contact-detail",
      data: {id: cp_id, type: request_type, contact_type: contact_type,
             contact_detail: contact_detail},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successEdit();
          }
        }, 1500);
      },
    });

  }
  else {
    emptyFields("acd_fields_"+cp_id);
  }

});


//CONTAINER'S PACKAGE PRICE & COST, TRUCKING CHARGES
$(".btn_expense").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var type = $(this).attr("data-type");
  var value = $("."+type+"_"+ic_id).val();
  var value2 = (type == "price") ? $("."+type+"_"+ic_id).attr("data-target") : "";
  var trim = value.replace(/\,/g, '');
  var input_val = parseFloat(trim);

  $.ajax({
    type: "post",
    url: "/container-expenses",
    data: {id: ic_id, type: type, value: input_val, value2: value2},
    dataType: "json",
    beforeSend: function(){
      loader();
    },
    success: function(data){
      setTimeout(function(){
        if (data.status == "success") {
          swal.fire({
            type: "success",
            title: "Success!",
            html: "<b class='swal_success'>"+data.title+" Updated Successfully!</b>",
          }).then(function(){
            $("[data-target='#"+type+"_modal_"+ic_id+"']").html(value);
            $(".modal").modal("hide");
          });
        }
      }, 1500);
    },
  });

});

//RETIRE RECORD (PROFILES)
$(".btn_retire, .btn_restore").on("click", function(){

  var id = $(this).attr("id");
  var name = $(this).attr("data-target");
  var split_val = $(this).attr("data-type").split("-");
  var type = split_val[0];
  var status = split_val[1];
  var title;
  var process;

  switch (type) {
    case "sa":
      title = "Sales Agent";
      break;

    case "lc":
      title = "Local Customer";
      break;

    case "fp":
      title = "Foreign Partner";
      break;

    case "cb":
      title = "Customs Brokerage";
      break;

    case "tc":
      title = "Trucking Company";
      break;

    case "sl":
      title = "Shipping Line";
      break;

    case "port":
      title = "Port";
      break;

    case "item":
      title = "Item";
      break;

    default:

  }

  switch (status) {
    case "Retired":
      process = "Retire";
      text = "retire";
      break;

    default:
      process = "Restore";
      text = "restore";
      break;

  }

  swal.fire({
    type: "warning",
    title: process+" "+title,
    html: "<b>Are you sure you want to "+text+" "+name+"?</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/single-set-status",
        data: {type: type, id: id, status: status},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              if (status == "Retired") {
                successRetire();
              }
              else {
                successRestore();
              }
            }
          }, 1500);
        },
      });
    }
  });

});

//MULTIPLE RETIRE
$(".btn_multi_retire").on("click", function(){

  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");
  var record_list = "";
  var form = new FormData();

  form.append("request_type", request_type);
  form.append("status", "Retired");

  $('input[name="'+get_type+'"]:checked').each(function(){
    form.append($(this).attr("name"), $(this).val());
    record_list += $(this).attr("data-target")+"<br>";
  });

  swal.fire({
    type: "info",
    title: "Retire Records",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
    html: "<b class='swal_info'>Are you sure you want to retire these records?<br>"+record_list+"</b>",
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/multiple-set-status",
        processData: false,
        contentType: false,
        cache: false,
        data: form,
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRetire();
            }
          }, 1500);
        },
      });
    }
  });

});

//MULTIPLE RESTORE
$(".btn_multi_restore").on("click", function(){

  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");
  var record_list = "";
  var form = new FormData();

  form.append("request_type", request_type);
  form.append("status", "Active");

  $('input[name="'+get_type+'"]:checked').each(function(){
    form.append($(this).attr("name"), $(this).val());
    record_list += $(this).attr("data-target")+"<br>";
  });

  swal.fire({
    type: "info",
    title: "Restore Records",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
    html: "<b class='swal_info'>Are you sure you want to restore these records?<br>"+record_list+"</b>",
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/multiple-set-status",
        processData: false,
        contentType: false,
        cache: false,
        data: form,
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRestore();
            }
          }, 1500);
        },
      });
    }
  });

});

//MULTIPLE PROCEED
function multipleProceed(get_type, request_type) {

  var record_list = "";
  var form = new FormData();

  form.append("request_type", request_type);

  $('input[name="'+get_type+'"]:checked').each(function(){
    form.append($(this).attr("name"), $(this).val());
    record_list += $(this).attr("data-target")+"<br>";
  });

  swal.fire({
    type: "info",
    title: "Update Records",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
    html: "<b class='swal_info'>Are you sure you want to update these records?<br>"+record_list+"</b>",
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/multiple-proceed",
        processData: false,
        contentType: false,
        cache: false,
        data: form,
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successUpdate();
            }
          }, 1500);
        },
      });
    }
  });

}

//MULTIPLE REVERT
function multipleRevert(get_type, request_type) {

  var record_list = "";
  var form = new FormData();

  form.append("request_type", request_type);

  $('input[name="'+get_type+'"]:checked').each(function(){
    form.append($(this).attr("name"), $(this).val());
    record_list += $(this).attr("data-target")+"<br>";
  });

  swal.fire({
    type: "warning",
    title: "Revert Records",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
    html: "<b class='swal_info'>Are you sure you want to revert these records?<br>"+record_list+"</b>",
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/multiple-revert",
        processData: false,
        contentType: false,
        cache: false,
        data: form,
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successUpdate();
            }
          }, 1500);
        },
      });
    }
  });

}

function loader(){
  swal.fire({
    title: "Processing please wait....",
    allowEscapeKey: false,
    allowOutsideClick: false,
    onOpen: () => {
      swal.showLoading();
    }
  });
}

function successSave() {
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Saved Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function successEdit(){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Edited Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function successETA(date, user_name, ic_id){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>ETA updated Successfully.</b>",
  }).then(function(){
    $('.modal').modal('hide');
    $("input[type='date']").val("");
    $("[data-target='#eta_date_modal_"+ic_id+"']").html(date);
    $(".eta_acfta_by_"+ic_id).html(user_name);
  });
}

function successForm(title, raw_date, date, values, user_name, ic_id, type, modal){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>"+title+" updated Successfully.</b>",
  }).then(function(){
    $("."+type+"_date_"+ic_id+"").val(raw_date);
    $("."+type+"_container_"+ic_id+"").val(raw_date);
    $("."+type+"_duties_"+ic_id+"").val(values);
    $("."+type+"_time_"+ic_id+"").val(raw_date);
    $("."+type+"_no_"+ic_id+"").val(values);
    $("."+type+"_trucking_"+ic_id+"").val(values);

    $("[data-target='#"+type+"_"+modal+"_modal_"+ic_id+"']").html((modal == "no" || modal == "rm") ? values : date);

    //extra conditionals for unique request types (eg. ETA & Shipping line payment POST REQUESTS)
    if (type == "eta") {
      $(".eta_acfta_by_"+ic_id).html(user_name);
    }
    if (type == "sl") {
      var split_val = values.split("-");
      $("."+type+"_expenses_"+ic_id+"").val(split_val[0]);
      $("."+type+"_deposit_"+ic_id+"").val(split_val[1]);
    }
    if (type == "gp") {
      var split_url = values.split("/");
      var filename = split_url[2];
      var title;
      $("input[type='file']").val("");
      $("."+type+"_anchor_"+ic_id).remove();
      $("."+type+"_file_"+ic_id).after('<br><br><a class="'+type+'_anchor_'+ic_id+'" href="'+asset_path+''+values+'" download>'+filename+'</a>');
    }
    if (type == "eir") {
      var split_url = values.split("/");
      var filename = split_url[2];
      var title;
      $("input[type='file']").val("");
      $("."+type+"_anchor_"+ic_id).remove();
      $("."+type+"_file_"+ic_id).after('<br><br><a class="'+type+'_anchor_'+ic_id+'" href="'+asset_path+''+values+'" download>'+filename+'</a>');
    }
    if (type == "trucking") {
        $(".trucking_name_"+ic_id+" option[value='"+values+"']").prop('selected',true);
    }

    $('.modal').modal('hide');
  });
}

function successUpload(type, date, file, user_name, ic_id){

  var split_url = file.split("/");
  var filename = split_url[2];
  var title;

  switch (type) {
    case "bl":
      title = "B/L";
      break;
    case "pl":
      title = "P/L";
      break;
    case "acfta":
      title = "ACFTA";
      break;
    case "gp":
      title = "Gate Pass";
      break;
    default:

  }

  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>"+title+" File Uploaded Successfully.</b>",
  }).then(function(){
    $("[data-target='#"+type+"_date_modal_"+ic_id+"']").html(date);
    $("."+type+"_container_"+ic_id+"").val(date);
    $("input[type='file']").val("");
    $("."+type+"_anchor_"+ic_id).remove();
    $("."+type+"_file_"+ic_id).after('<br><br><a class=""+type+"_anchor_'+ic_id+'" href="'+asset_path+''+file+'" download>'+filename+'</a>');
    if (type == "acfta") {
      $(".eta_acfta_by_"+ic_id).html(user_name);
    }
    $('.modal').modal('hide');
  });
}

function successUpdate(){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Updated Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function successRetire(){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Retired Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function successRestore(){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Restored Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function successRemove(){
  swal.fire({
    type: "success",
    title: "Success!",
    html: "<b class='swal_success'>Record Removed Successfully.</b>",
  }).then(function(){
    window.location.reload();
  });
}

function emptyFields(fields){
  swal.fire({
    type: "error",
    title: "Empty Fields!",
    html: "<b class='swal_error'>Please fill up all the required fields before proceeding.</b>",
  }).then(function(){
    $("."+fields).each(function(){
      if ($(this).val() == "") {
        $(this).css("border-color", "red");
      }
    });
  });
}

function contactPersonLimit(){
  swal.fire({
    type: "error",
    title: "Limit Reached!",
    html: "<b class='swal_error'>Only 5 Contact Persons allowed per record.</b>",
  });
}

function contactDetailLimit(){
  swal.fire({
    type: "error",
    title: "Limit Reached!",
    html: "<b class='swal_error'>Only 4 Contact Details allowed per Contact Person.</b>",
  });
}


//DATATABLES INITIALIZATION
$(document).ready( function () {
  $('.profile_table').DataTable({
    pageLength: 10,
    dom: 'Bfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  });

  $('.transactions_table').DataTable({
    pageLength: 10,
    order: false,
     // columnDefs: [{
     //   targets: [8, 5],
     //   render: $.fn.dataTable.render.ellipsis(20, true)
     // }],
     bSort : false,
    dom: 'Bfrtip',
    buttons: [
      'copy', 'csv', 'excel', 'pdf', 'print'
    ]
  });
});

















//
