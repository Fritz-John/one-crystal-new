//UPDATE ETA
$(".btn_eta_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_eta_date = $(".eta_date_"+ic_id+"").val();
  var request_type = "eta_date";

  if (get_eta_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_eta_date: get_eta_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("ETA", data.eta_date_raw,
            data.eta_date, "", "", ic_id, "eta", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPDATE YARD DATE
$(".btn_yard_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_yard_date = $(".yard_date_"+ic_id+"").val();
  var request_type = "yard_date";

  if (get_yard_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_yard_date: get_yard_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Yard Date", data.yard_date_raw,
            data.yard_date, "", "", ic_id, "yard", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//INSERT FINAL DUTIES
$(".btn_fd_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_fd_date = $(".fd_date_"+ic_id+"").val();
  var get_fd_duties = $(".fd_duties_"+ic_id+"").val();
  var request_type = "final_duties";

  if (get_fd_date != "" && get_fd_duties != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_fd_date: get_fd_date,
             get_fd_duties: get_fd_duties, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Final Duties", data.fd_date_raw, data.fd_date,
            data.fd_duties, data.user_name, ic_id, "fd", "form");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//INSERT DEBITED DUTIES
$(".btn_dd_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_dd_date = $(".dd_date_"+ic_id+"").val();
  var get_dd_duties = $(".dd_duties_"+ic_id+"").val();
  var request_type = "debited_duties";

  if (get_dd_date != "" && get_dd_duties != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_dd_date: get_dd_date,
             get_dd_duties: get_dd_duties, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Debited Duties", data.dd_date_raw, data.dd_date,
            data.dd_duties, data.user_name, ic_id, "dd", "form");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPLOAD GATE PASS FILE
$(".btn_gp_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_gp_file = $(".gp_file_"+ic_id+"").get(0).files;
  var get_gp_date = $(".gp_date_"+ic_id+"").val();
  var request_type = "upload_gp";
  var form = new FormData;

  $.each($(".gp_file_"+ic_id+"").get(0).files, function(){
    form.append("get_gp_file", this);
  });
  form.append("get_gp_date", get_gp_date);
  form.append("request_type", request_type);
  form.append("ic_id", ic_id);

  if (get_gp_file.length > 0 && get_gp_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Gate Pass", data.gp_date_raw,
            data.gp_date, data.gp_file, "", ic_id, "gp", "form");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("gp_file_"+ic_id+"");
  }

});

//UPDATE LODGE DATE
$(".btn_lodge_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_lodge_date = $(".lodge_date_"+ic_id+"").val();
  var request_type = "lodge_date";

  if (get_lodge_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_lodge_date: get_lodge_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Lodge Date", data.lodge_date_raw,
            data.lodge_date, "", "", ic_id, "lodge", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//INSERT SHIPPING LINE PAYMENT
$(".btn_sl_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_sl_date = $(".sl_date_"+ic_id+"").val();
  var get_sl_expenses = $(".sl_expenses_"+ic_id+"").val();
  var get_sl_deposit = $(".sl_deposit_"+ic_id+"").val();
  var request_type = "sl_payment";

  if (get_sl_date != "" && get_sl_expenses != "" && get_sl_deposit != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_sl_date: get_sl_date,
             get_sl_expenses: get_sl_expenses, get_sl_deposit: get_sl_deposit,
             ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            var slval = data.sl_expenses+"-"+data.sl_deposit;
            successForm("Shipping Line Payment", data.sl_date_raw, data.sl_date,
            slval, data.user_name, ic_id, "sl", "form");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPDATE STORAGE VALIDITY
$(".btn_storage_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_storage_date = $(".storage_date_"+ic_id+"").val();
  var request_type = "storage_date";

  if (get_storage_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_storage_date: get_storage_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Storage Validity", data.storage_date_raw,
            data.storage_date, "", "", ic_id, "storage", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPDATE CRO VALIDITY
$(".btn_cro_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_cro_date = $(".cro_date_"+ic_id+"").val();
  var request_type = "cro_date";

  if (get_cro_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_cro_date: get_cro_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("CRO Validity", data.cro_date_raw,
            data.cro_date, "", "", ic_id, "cro", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPDATE OFT & LFT
$(".btn_time_done").on("click", function(){

  var id = $(this).attr("id");
  var ic_id = $(this).attr("data-target");
  var get_time = $("."+id+"_time_"+ic_id+"").val();
  var request_type = ""+id+"_time";
  var title = (id == "oft") ? "Origin F. Time" : "Local F. Time";

  if (get_time != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_time: get_time, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm(title, data.time_raw,
            data.time, data.time, "", ic_id, id, "time");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//INSERT REG, ENTRY & PRO NO.
$(".btn_no_done").on("click", function(){

  var id = $(this).attr("id");
  var ic_id = $(this).attr("data-target");
  var get_no = $("."+id+"_no_"+ic_id+"").val();
  var request_type = ""+id+"_no";
  var title;

  switch (id) {
    case "reg":
      title = "Reg No.";
      break;

    case "entry":
      title = "Entry No.";
      break;

    case "pro":
      title = "PRO No.";
      break;
    default:

  }

  if (get_no != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-op",
      data: {request_type: request_type, get_no: get_no, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm(title, "", "", data.no,
            "", ic_id, id, "no");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});






















//
