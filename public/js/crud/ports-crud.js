//ADD PORT
$(".btn_done").on("click", function(){

  var empty_count = 0;
  var form = new FormData();

  $(".required_field").each(function(){
    if ($(this).val() == "") {
      empty_count++;
    }
    else {
      form.append($(this).attr("name"), $(this).val());
    }
  });

  if (empty_count == 0) {

    $.ajax({
      type: "post",
      url: "/store-port",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successSave();
          }
        }, 1500);
      },
    });

  }
  else {
    emptyFields("required_fields");
  }

});

//EDIT PORT
$(".btn_save").on("click", function(){

  var empty_count = 0;
  var form = new FormData();
  var get_port_id = $(this).attr("data-target");

  $(".edit_required_fields_"+get_port_id).each(function(){
    if ($(this).val() == "") {
      empty_count++;
    }
    else {
      form.append($(this).attr("name"), $(this).val());
    }
  });

  form.append("get_port_id", get_port_id);

  if (empty_count == 0) {

    $.ajax({
      type: "post",
      url: "/edit-port",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successEdit();
          }
        }, 1500);
      },
    });

  }
  else {
    emptyFields("edit_required_fields_"+get_port_id);
  }

});
