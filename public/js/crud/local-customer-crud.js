//ADD LOCAL CUSTOMER
$(".btn_done").on("click", function(){

  var form = new FormData;
  var empty_count = 0;

  $(".required_fields").each(function(){
    if ($(this).val() == "") {
      empty_count++;
    }
    else {
      form.append($(this).attr("name"), $(this).val());
    }
  });

  form.append("lc_address[]", groupFields("", "")[0]);
  form.append("lc_contact_person[]", groupFields("", "")[1]);

  if (empty_count == 0) {
    $.ajax({
      type: "post",
      url: "/store-local-customer",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successSave();
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//EDIT LOCAL CUSTOMER
$(".btn_save").on("click", function(){

  var get_detail_type = $(this).attr("id");
  var field_type;
  var lc_id = $(this).attr("data-target");
  var form = new FormData;
  var empty_count = 0;

  if (get_detail_type == "main_details") {

    field_type = "edit_required_fields_"+lc_id;

    $(".edit_required_fields_"+lc_id+"").each(function(){
      if ($(this).val() == "") {
        empty_count++;
      }
      else {
        form.append($(this).attr("data-target"), $(this).val());
      }
    });

    form.append("lc_address[]", groupFields("_"+lc_id, "edit_")[0]);
    form.append("lc_contact_person[]", groupFields("_"+lc_id, "edit_")[1]);
  }
  else if (get_detail_type == "add_details") {

    field_type = "additional_fields_"+lc_id;

    form.append("lc_address[]", groupFields("_"+lc_id, "add_")[0]);
    form.append("lc_contact_person[]", groupFields("_"+lc_id, "add_")[1]);

    $(".additional_fields_"+lc_id+"").each(function(){
      form.append($(this).attr("data-target"), $(this).val());
    });

    if (groupFields("_"+lc_id, "add_")[0].length == 0 && groupFields("_"+lc_id, "add_")[1].length == 0) {
      empty_count++;
    }
  }
  else {

    field_type = "item_fields_"+lc_id;
    form.append("lc_item[]", groupFields("_"+lc_id, "")[2]);
    form.append("lc_id", lc_id);

    if (groupFields("_"+lc_id, "")[2].length == 0) {
      empty_count++;
    }

  }

  for (var pair of form.entries()) {
    console.log(pair[0]+ ', ' + pair[1]);
}

  if (empty_count == 0) {

    form.append("type", get_detail_type);

    swal.fire({
      type: "info",
      title: "Save Changes",
      confirmButtonColor: 'lightgreen',
      cancelButtonColor: 'red',
      confirmButtonText: '<span style="color: white;">Proceed</span>',
      cancelButtonText: '<span style="color: white;">Cancel</span>',
      reverseButtons: true,
      showCancelButton: true,
      html: "<b class='swal_info'>Are you sure you want to save these changes?</b>",
    }).then((result) => {
      if (result.value) {
        $.ajax({
          type: "post",
          url: "/edit-local-customer",
          processData: false,
          contentType: false,
          cache: false,
          data: form,
          dataType: "json",
          beforeSend: function(){
            loader();
          },
          success: function(data){
            setTimeout(function(){
              if (data.status == "success") {
                successSave();
              }
            }, 1500);
          },
        });
      }
    });
  }
  else {
    emptyFields(field_type);
  }

});

//REMOVE CURRENT ADDRESS
$(".remove_current_address").on("click", function(){

  var split_val = $(this).attr("data-target").split("|");
  var get_address_id = split_val[0];
  var get_address = split_val[1];
  var type = "address";

  swal.fire({
    type: "warning",
    title: "Remove Address",
    html: "<b>Are you sure you want to remove this address? <br>"+get_address+"</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-local-customer",
        data: {get_address_id: get_address_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
            }
          }, 1500);
        },
      });
    }
  });

});

//REMOVE CURRENT CONTACT PERSON
$(".remove_current_person").on("click", function(){

  var split_val = $(this).attr("data-target").split("|");
  var get_person_id = split_val[0];
  var get_person = split_val[1];
  var type = "person";

  swal.fire({
    type: "warning",
    title: "Remove Contact Person",
    html: "<b>Are you sure you want to remove this contact person? <br>"+get_person+"</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-local-customer",
        data: {get_person_id: get_person_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
            }
          }, 1500);
        },
      });
    }
  });

});

//REMOVE CURRENT CONTACT DETAIL
$(".remove_current_contact").on("click", function(){

  var split_val = $(this).attr("data-target").split("|");
  var get_contact_id = split_val[0];
  var get_contact = split_val[1];
  var type = "contact";

  swal.fire({
    type: "warning",
    title: "Remove Contact Detail",
    html: "<b>Are you sure you want to remove this contact detail? <br>"+get_contact+"</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-local-customer",
        data: {get_contact_id: get_contact_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
            }
          }, 1500);
        },
      });
    }
  });

});

//REMOVE CURRENT ITEM
$(".remove_current_item").on("click", function(){

  var split_val = $(this).attr("data-target").split("|");
  var get_item_id = split_val[0];
  var get_item = split_val[1];
  var type = "item";

  swal.fire({
    type: "warning",
    title: "Remove Item",
    html: "<b>Are you sure you want to remove this Item? <br>"+get_item+"</b>",
    confirmButtonColor: 'lightgreen',
    cancelButtonColor: 'red',
    confirmButtonText: '<span style="color: white;">Proceed</span>',
    cancelButtonText: '<span style="color: white;">Cancel</span>',
    reverseButtons: true,
    showCancelButton: true,
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "post",
        url: "/remove-detail-local-customer",
        data: {get_item_id: get_item_id, type: type},
        dataType: "json",
        beforeSend: function(){
          loader();
        },
        success: function(data){
          setTimeout(function(){
            if (data.status == "success") {
              successRemove();
            }
          }, 1500);
        },
      });
    }
  });

});

function groupFields(lc_id, get_type){

  var address_arr = [];
  var contact_person_arr = [];
  var item_arr = [];
  var address_field = -Infinity;
  var person_field = -Infinity;
  var contact_field = -Infinity;
  var item_field = -Infinity;

  //GET CURRENT COUNT OF ALL CONTACT PERSON FIELDS
  $("[data-target='"+get_type+"address_container"+lc_id+"']").each(function() {
    var id_split = $(this).attr("id").split("_");
    var id_count = (get_type == "") ? id_split[1] : id_split[2];
    address_field = Math.max(address_field, parseFloat(id_count));
  });

  //GET CURRENT COUNT OF ALL CONTACT PERSON FIELDS
  $("[data-target='"+get_type+"person_container"+lc_id+"']").each(function() {
    var id_split = $(this).attr("id").split("_");
    var id_count = (get_type == "") ? id_split[1] : id_split[2];
    person_field = Math.max(person_field, parseFloat(id_count));
  });

  //GET CURRENT COUNT OF ALL CONTACT FIELDS
  $("[data-target='"+get_type+"contact_container"+lc_id+"']").each(function() {
    var id_split = $(this).attr("id").split("_");
    var id_count = (get_type == "") ? id_split[3] : id_split[4];
    contact_field = Math.max(contact_field, parseFloat(id_count));
  });

  //GET CURRENT COUNT OF ALL ITEM FIELDS
  $("[data-target='"+get_type+"item_container"+lc_id+"']").each(function() {
    var id_split = $(this).attr("id").split("_");
    var id_count = (get_type == "") ? id_split[1] : id_split[2];
    item_field = Math.max(item_field, parseFloat(id_count));
  });


  for (var i = 1; i <= address_field; i++) {

    var address_id = $("#"+get_type+"address_"+i+""+lc_id+"").parent().find("."+get_type+"address_"+i+"_id");
    var address_type = $("#"+get_type+"address_"+i+""+lc_id+"").parent().find("."+get_type+"address_"+i+"_type");
    var address_val = $("#"+get_type+"address_"+i+""+lc_id+"").parent().find("."+get_type+"address_"+i+"_address");
    var address_city = $("#"+get_type+"address_"+i+""+lc_id+"").parent().find("."+get_type+"address_"+i+"_city");

    if (address_type.val() != undefined && address_val.val() != undefined && address_city.val() != undefined) {
      if (address_type.val() != "" && address_val.val() != "" && address_city.val() != "") {
        if (get_type == "edit_") {
          address_arr.push(address_id.val()+"/"+address_type.val()+"/"+address_val.val()+"/"+address_city.val());
        }
        else {
          address_arr.push(address_type.val()+"/"+address_val.val()+"/"+address_city.val());
        }
      }
    }
  }

  //GROUP/MERGE ALL CONTACT PERSON'S FIELDS
  for (var index = 1; index <= person_field; index++) {
    var cp_id = $("#"+get_type+"person_"+index+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_id");
    var cp_name = $("#"+get_type+"person_"+index+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_name");
    var cp_position = $("#"+get_type+"person_"+index+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_position");
    var cp_contact = "";

    for (var index2 = 1; index2 <= contact_field; index2++) {
      var cp_contact_id = $("#"+get_type+"person_"+index+"_contact_"+index2+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_contact_"+index2+"_id").val();
      var cp_contact_type = $("#"+get_type+"person_"+index+"_contact_"+index2+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_contact_type_"+index2+"").val();
      var cp_contact_val = $("#"+get_type+"person_"+index+"_contact_"+index2+""+lc_id+"").parent().find("."+get_type+"person_"+index+"_contact_"+index2+"").val();

      if (cp_contact_type != undefined && cp_contact_val != undefined) {
        if (cp_contact_type != "" && cp_contact_val != "") {
          if (get_type == "edit_") {
            cp_contact += cp_contact_type+""+cp_contact_val+"-"+cp_contact_id;
          }
          else {
            cp_contact += cp_contact_type+""+cp_contact_val;
          }
        }
      }
    }

    if (cp_name.val() != undefined && cp_position.val() != undefined) {
      if (cp_name.val() != "" && cp_position.val() != "") {
        if (get_type == "edit_") {
          contact_person_arr.push(cp_id.val()+"/"+cp_name.val()+"/"+cp_position.val()+""+cp_contact);
        }
        else {
          contact_person_arr.push(cp_name.val()+"/"+cp_position.val()+""+cp_contact);
        }
      }
    }
  }

  //GROUP/MERGE ALL item FIELDS
  for (var i = 1; i <= item_field; i++) {

    var item_id = $("#"+get_type+"item_"+i+""+lc_id+"").parent().find("."+get_type+"item_"+i+"_id");
    var item_name = $("#"+get_type+"item_"+i+""+lc_id+"").parent().find("."+get_type+"item_"+i+"_name");
    var item_price = $("#"+get_type+"item_"+i+""+lc_id+"").parent().find("."+get_type+"item_"+i+"_price");

    if (item_name.val() != undefined) {
      if (item_name.val() != "") {
        if (get_type == "edit_") {
          item_arr.push(item_id.val()+"/"+item_name.val()+"/"+item_price.val());
        }
        else {
          item_arr.push(item_name.val()+"/"+item_price.val());
        }
      }
    }
  }

  return [address_arr, contact_person_arr, item_arr];
}




















//
