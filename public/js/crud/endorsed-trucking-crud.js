//UPDATE ENDORSED, LOAD, DELIVERED, EMPTY RETURN DATES
$(".btn_date_done").on("click", function(){

  var id = $(this).attr("id");
  var ic_id = $(this).attr("data-target");
  var get_date = $("."+id+"_date_"+ic_id+"").val();
  var request_type = ""+id+"_date";
  var title;

  switch (id) {
    case "endorsed":
      title = "Endorsed Date";
      break;

    case "load":
      title = "Load Date";
      break;

    case "delivered":
      title = "Delivered Date";
      break;
    default:
      title = "Empty Return";
      break;
  }

  if (get_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-et",
      data: {request_type: request_type, get_date: get_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm(title, data.date_raw, data.date, "",
            "", ic_id, id, "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});


//INSERT TRUCKING COMPANY
$(".btn_trucking_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_trucking = $(".trucking_name_"+ic_id+"").val();
  var get_trucking_name = $(".trucking_name_"+ic_id+" option:selected").text();
  var request_type = "trucking";

  if (get_trucking != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-et",
      data: {request_type: request_type, get_trucking: get_trucking, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Trucking Company", "", get_trucking_name, data.trucking,
            "", ic_id, "trucking", "comp");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});


//UPLOAD EIR FILE
$(".btn_eir_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_eir_file = $(".eir_file_"+ic_id+"").get(0).files;
  var get_eir_date = $(".eir_date_"+ic_id+"").val();
  var request_type = "upload_eir";
  var form = new FormData;

  $.each($(".eir_file_"+ic_id+"").get(0).files, function(){
    form.append("get_eir_file", this);
  });
  form.append("get_eir_date", get_eir_date);
  form.append("request_type", request_type);
  form.append("ic_id", ic_id);

  if (get_eir_file.length > 0 && get_eir_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-et",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Gate Pass", data.eir_date_raw,
            data.eir_date, data.eir_file, "", ic_id, "eir", "form");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("eir_file_"+ic_id+"");
  }

});


//INSERT REMARKS
$(".btn_remarks_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_remarks = $(".remarks_"+ic_id+"").val();
  var request_type = "remarks";

  if (get_remarks != "") {
    $.ajax({
      type: "post",
      url: "/additional-details-et",
      data: {request_type: request_type, get_remarks: get_remarks, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("Remarks", "", "", data.remarks,
            "", ic_id, "remarks", "rm");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});








































//
