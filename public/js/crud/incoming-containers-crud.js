//ADD INCOMING CONTAINER
$(".btn_done").on("click", function(){

  var form = new FormData;
  var empty_count = 0;

  $(".required_fields").each(function(){
    if ($(this).val() == "") {
      empty_count++;
    }
    else {
      form.append($(this).attr("name"), $(this).val());
    }
  });

  $(".required_checks").each(function(){
    if ($(this).is(":checked")) {
      form.append($(this).attr("name"), $(this).val());
    }
    else {
      form.append($(this).attr("name"), "");
    }
  });

  form.append("ic_container[]", groupFields("", ""));

  for (var pair of form.entries()) {
      console.log(pair[0]+ ', ' + pair[1]);
  }

  if (empty_count == 0) {
    $.ajax({
      type: "post",
      url: "/store-incoming-containers",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successSave();
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPDATE ETA
$(".btn_eta_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_eta_date = $(".eta_date_"+ic_id+"").val();
  var request_type = "eta_date";

  if (get_eta_date != "") {
    $.ajax({
      type: "post",
      url: "/additional-details",
      data: {request_type: request_type, get_eta_date: get_eta_date, ic_id: ic_id},
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successForm("ETA", data.eta_date_raw,
            data.eta_date, "", data.user_name, ic_id, "eta", "date");
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("required_fields");
  }

});

//UPLOAD B/L FILE
$(".btn_bl_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_bl_file = $(".bl_file_"+ic_id+"").get(0).files;
  var request_type = "upload_bl";
  var form = new FormData;

  $.each($(".bl_file_"+ic_id+"").get(0).files, function(){
    form.append("get_bl_file", this);
  });
  form.append("request_type", request_type);
  form.append("ic_id", ic_id);

  if (get_bl_file.length > 0) {
    $.ajax({
      type: "post",
      url: "/additional-details",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successUpload("bl", data.bl_date, data.bl_file, "", ic_id);
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("bl_file_"+ic_id+"");
  }

});

//UPLOAD B/L FILE
$(".btn_pl_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_pl_file = $(".pl_file_"+ic_id+"").get(0).files;
  var request_type = "upload_pl";
  var form = new FormData;

  $.each($(".pl_file_"+ic_id+"").get(0).files, function(){
    form.append("get_pl_file", this);
  });
  form.append("request_type", request_type);
  form.append("ic_id", ic_id);

  if (get_pl_file.length > 0) {
    $.ajax({
      type: "post",
      url: "/additional-details",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successUpload("pl", data.pl_date, data.pl_file, "", ic_id);
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("pl_file_"+ic_id+"");
  }

});

//UPLOAD B/L FILE
$(".btn_acfta_done").on("click", function(){

  var ic_id = $(this).attr("data-target");
  var get_acfta_file = $(".acfta_file_"+ic_id+"").get(0).files;
  var request_type = "upload_acfta";
  var form = new FormData;

  $.each($(".acfta_file_"+ic_id+"").get(0).files, function(){
    form.append("get_acfta_file", this);
  });
  form.append("request_type", request_type);
  form.append("ic_id", ic_id);

  if (get_acfta_file.length > 0) {
    $.ajax({
      type: "post",
      url: "/additional-details",
      processData: false,
      contentType: false,
      cache: false,
      data: form,
      dataType: "json",
      beforeSend: function(){
        loader();
      },
      success: function(data){
        setTimeout(function(){
          if (data.status == "success") {
            successUpload("acfta", data.acfta_date, data.acfta_file, data.user_name, ic_id);
          }
        }, 1500);
      },
    });
  }
  else {
    emptyFields("acfta_file_"+ic_id+"");
  }

});


function groupFields(ic_id, get_type){

  var container_arr = [];
  var container_field = -Infinity;

  //GET CURRENT COUNT OF ALL CONTACT PERSON FIELDS
  $("[data-target='"+get_type+"container_container"+ic_id+"']").each(function() {
    var id_split = $(this).attr("id").split("_");
    var id_count = (get_type == "") ? id_split[1] : id_split[2];
    container_field = Math.max(container_field, parseFloat(id_count));
  });

  for (var i = 1; i <= container_field; i++) {

    var container_size = $("#"+get_type+"container_"+i+""+ic_id+"").parent().find("."+get_type+"container_"+i+"_size");
    var container_item = $("#"+get_type+"container_"+i+""+ic_id+"").parent().find("."+get_type+"container_"+i+"_item");
    var container_no = $("#"+get_type+"container_"+i+""+ic_id+"").parent().find("."+get_type+"container_"+i+"_no");
    var container_package = $("#"+get_type+"container_"+i+""+ic_id+"").parent().find("."+get_type+"container_"+i+"_package");
    var container_gross = $("#"+get_type+"container_"+i+""+ic_id+"").parent().find("."+get_type+"container_"+i+"_gross");

    if (container_size.val() != undefined &&
        container_item.val() != undefined &&
        container_no.val() != undefined &&
        container_package.val() != undefined &&
        container_gross.val() != undefined) {

      if (container_size.val() != "" &&
          container_item.val() != "" &&
          container_no.val() != "" &&
          container_package.val() != "" &&
          container_gross.val() != "") {

            var trim1 = container_package.val().replace(/\,/g, '');
            var trim2 = container_gross.val().replace(/\,/g, '');
            var package = parseFloat(trim1);
            var gross = parseFloat(trim2);

          container_arr.push(container_size.val()+"/"+container_item.val()+"/"+container_no.val()+"/"+package+"/"+gross);

      }
    }
  }

  return [container_arr];

}



















//
