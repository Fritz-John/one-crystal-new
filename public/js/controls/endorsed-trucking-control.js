//CHECK EMPTY RETURN & EIR
$(".btn_multi_proceed").on("click", function(){

  var empty_count = 0;
  var get_type = $(this).attr("data-target");
  var request_type = $(this).attr("id");

  $("input[name='select_et[]']:checked").each(function(){
    var ic_id = $(this).val();
    if ($(".empty_container_"+ic_id).val() == "" || $(".eir_container_"+ic_id).val() == "") {
      empty_count++;
    }
  });

  if (empty_count == 0) {
    multipleProceed(get_type, request_type);
  }
  else {
    emptyFields();
  }

});
