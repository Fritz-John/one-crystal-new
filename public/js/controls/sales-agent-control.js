//ADD ADDRESS BUTTON
var count = 1;
var count_2 = 1;

$(".add_address").on("click", function(){

  count++;
  var type = $(this).attr("data-target");
  var name = (type === 'edit_modal') ? 'edit_' : '';
  var data_target = (type === 'edit_modal') ? 'edit_modal' : 'add_modal';

  $("."+name+"default_address").after('<div class="col-lg-12 new_address_'+count+'">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend w-130">'+
  '<select class="form-control required_fields required_fields" name="'+name+'agent_address_type[]">'+
  '<option value="">--Select--</option>'+
  '<option value="Office">Office</option>'+
  '<option value="Warehouse">Warehouse</option>'+
  '<option value="Home">Home</option>'+
  '</select>'+
  '</div>'+
  '<input type="text" class="form-control required_fields required_fields" name="'+name+'agent_address[]" placeholder="Address">'+
  '<div class="input-group-append">'+
  '<button data-target="'+data_target+'" id="new_address_'+count+'" type="button" class="btn btn-danger remove_address">'+
  '<i class="fas fa-times"></i>'+
  '</button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6 new_address_'+count+'">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">City</div>'+
  '</div>'+
  '<input type="text" class="form-control required_fields required_fields" name="'+name+'agent_city[]" placeholder="City">'+
  '</div>'+
  '</div>'+
  '</div>');

});

//ADD CONTACT DETAILS
$(".add_contact").on("click", function(){

  count_2++;

  var type = $(this).attr("data-target");
  var name = (type === 'edit_modal') ? 'edit_' : '';
  var data_target = (type === 'edit_modal') ? 'edit_modal' : 'add_modal';
  var id = $(this).attr("data-id");

  $("."+name+"default_contact").after('<div class="col-lg-6 new_contact_'+count_2+'">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend w-120">'+
  '<select class="form-control required_fields additional_fields_'+id+'" name="'+name+'agent_contact_type[]">'+
  '<option value="">--Select--</option>'+
  '<option value="Mobile">Mobile</option>'+
  '<option value="Landline">Landline</option>'+
  '<option value="Fax">Fax</option>'+
  '<option value="Email">Email</option>'+
  '<option value="WeChat">WeChat</option>'+
  '</select>'+
  '</div>'+
  '<input type="text" class="form-control required_fields additional_fields_'+id+'" name="'+name+'agent_contact[]" placeholder="Contact Details">'+
  '<div class="input-group-append">'+
  '<button data-target="'+data_target+'" id="new_contact_'+count_2+'" type="button" class="btn btn-danger remove_contact"><i class="fas fa-times"></i></button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>');

});

//REMOVE ADDRESS & CONTACT BUTTON
$(document).on("click", ".remove_address, .remove_contact", function(){

  var get_id = $(this).attr("id");

  $("."+get_id).remove();

});

//REMOVE BORDER COLOR ONCHANGE ONKEYPRESS ETC
$(document).on("change keypress", ".required_fields", function(){
  $(this).css("border-color", "");
});


//MULTIPLE RETIRE
$(".multi_retire").on("click", function(){
  if ($('.multi_retire:checked').length > 0) {
    $(".btn_multi_retire").fadeIn();
  }
  else {
    $(".btn_multi_retire").fadeOut();
  }
});




























//
