
calculate();

$(".container_fields").each(function(){
  var convert = $(this).val().toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
  $(this).val(convert);
})

$(".required_fields").on("change keyup", function(){
  calculate();
});

function calculate(){

  var total_cost = 0;

  $(".required_fields").each(function(){

    if ($(this).val() != "") {
      var trim = $(this).val().replace(/\,/g, '');
      var input_val = parseInt(trim);

      console.log(trim);
      total_cost += input_val;
    }

  });

  $(".total_cost").html(total_cost.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
}
