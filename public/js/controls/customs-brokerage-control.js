//ADD ADDRESS BUTTON
var count = 1;
var count_2 = 2;

$(".add_address").on("click", function(){

  count++;
  var type = $(this).attr("data-target");
  var name = (type === 'edit_modal') ? 'edit_' : '';
  var data_target = (type === 'edit_modal') ? 'edit_modal' : 'add_modal';

  $("."+name+"default_address").after('<div data-target="address_container" id="address_'+count+'" class="col-lg-12 p-1">'+
  '<div class="col-lg-12">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend w-130">'+
  '<select data-target="address_field_'+count+'" class="form-control required_fields address_'+count+'_type">'+
  '<option value="">--Select--</option>'+
  '<option value="Office">Office</option>'+
  '<option value="Warehouse">Warehouse</option>'+
  '<option value="Home">Home</option>'+
  '</select>'+
  '</div>'+
  '<input data-target="address_field_'+count+'" type="text" class="form-control required_fields address_'+count+'_address" placeholder="Address">'+
  '<div class="input-group-append">'+
  '<button data-target="address_'+count+'" type="button" class="btn btn-danger remove_address">'+
  '<i class="fas fa-times"></i>'+
  '</button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">City</div>'+
  '</div>'+
  '<input data-target="address_field_'+count+'" type="text" class="form-control required_fields address_'+count+'_city" placeholder="City">'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>');
});

//ADD CONTACT PERSON
$(".add_contact_person").on("click", function(){

  var contact_person_count = $("[data-target='person_container']").length + 1;

  if (contact_person_count <= 5) {
    count++;
    var type = $(this).attr("data-target");
    var name = (type === 'edit_modal') ? 'edit_' : '';
    var data_target = (type === 'edit_modal') ? 'edit_modal' : 'add_modal';

    $(".default_person").after('<div data-target="person_container" id="person_'+count+'" class="row p-1">'+
      '<div class="col-lg-6">'+
        '<div class="form-group">'+
          '<div class="input-group mb-2">'+
            '<div class="input-group-prepend">'+
              '<div class="input-group-text">Contact Person</div>'+
            '</div>'+
            '<input type="text" class="form-control required_fields person_'+count+'_name" placeholder="Full Name">'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<div class="col-lg-6">'+
        '<div class="form-group">'+
          '<div class="input-group mb-2">'+
            '<div class="input-group-prepend">'+
              '<div class="input-group-text">Position</div>'+
            '</div>'+
            '<input type="text" class="form-control required_fields person_'+count+'_position" placeholder="Position">'+
            '<div class="input-group-append">'+
              '<button data-target="person_'+count+'" type="button" class="btn btn-danger remove_person">'+
                '<i class="fas fa-times"></i>'+
              '</button>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<div data-target="contact_container" id="person_'+count+'_contact_1" class="col-lg-6">'+
        '<div class="form-group">'+
          '<div class="input-group mb-2">'+
            '<div class="input-group-prepend w-120">'+
              '<select class="form-control required_fields person_'+count+'_contact_type_1">'+
              '<option value="">--Select--</option>'+
                '<option value="/Mobile-" selected>Mobile</option>'+
                '<option value="/Landline-">Landline</option>'+
                '<option value="/Fax-">Fax</option>'+
                '<option value="/Email-">Email</option>'+
                '<option value="/WeChat-">WeChat</option>'+
              '</select>'+
            '</div>'+
            '<input type="text" class="form-control required_fields person_'+count+'_contact_1" placeholder="Contact Details">'+
          '</div>'+
        '</div>'+
      '</div>'+
      '<div data-target="contact_container" id="person_'+count+'_contact_2" class="col-lg-6 default_contact">'+
        '<div class="form-group">'+
          '<div class="input-group mb-2">'+
            '<div class="input-group-prepend w-120">'+
              '<select class="form-control required_fields person_'+count+'_contact_type_2">'+
              '<option value="">--Select--</option>'+
                '<option value="/Mobile-">Mobile</option>'+
                '<option value="/Landline-">Landline</option>'+
                '<option value="/Fax-">Fax</option>'+
                '<option value="/Email-" selected>Email</option>'+
                '<option value="/WeChat-">WeChat</option>'+
              '</select>'+
            '</div>'+
            '<input type="text" class="form-control required_fields person_'+count+'_contact_2" placeholder="Contact Details">'+
            '<div class="input-group-append">'+
              '<button name="person_'+count+'" data-target="add_modal" type="button" class="btn btn-primary add_contact">'+
                '<i class="fas fa-plus"></i>'+
              '</button>'+
            '</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>');
  }
  else {
    contactPersonLimit();
  }
});

//ADD CONTACT DETAIL (CONTACT PERSON)
$(document).on("click", ".add_contact", function(){

  count_2++;
  var get_person_id = $(this).attr("name");
  var contact_detail_count = $("#"+get_person_id+" [data-target='contact_container']").length + 1;

  if (contact_detail_count <= 4) {
    $("#"+get_person_id+" .default_contact").after('<div data-target="contact_container" id="'+get_person_id+'_contact_'+count_2+'" class="col-lg-6">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend w-120">'+
    '<select class="form-control required_fields '+get_person_id+'_contact_type_'+count_2+'">'+
    '<option value="">--Select--</option>'+
    '<option value="/Mobile-">Mobile</option>'+
    '<option value="/Landline-">Landline</option>'+
    '<option value="/Fax-">Fax</option>'+
    '<option value="/Email-">Email</option>'+
    '<option value="/WeChat-">WeChat</option>'+
    '</select>'+
    '</div>'+
    '<input type="text" class="form-control required_fields '+get_person_id+'_contact_'+count_2+'" placeholder="Contact Details">'+
    '<div class="input-group-append">'+
    '<button data-target="'+get_person_id+'_contact_'+count_2+'" type="button" class="btn btn-danger remove_contact">'+
    '<i class="fas fa-times"></i>'+
    '</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>');
  }
  else {
    contactDetailLimit();
  }

});

//ADDITIONAL ADDRESS
$(".additional_address").on("click", function(){

  count++;
  var cb_id = $(this).attr("data-target");

  $(".add_default_address_"+cb_id+"").after('<div data-target="add_address_container_'+cb_id+'" id="add_address_'+count+'_'+cb_id+'" class="row p-1">'+
  '<div class="col-lg-12">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend w-130">'+
  '<select data-target="add_address_field_'+count+'" class="form-control additional_fields_'+cb_id+' add_address_'+count+'_type">'+
  '<option value="">--Select--</option>'+
  '<option value="Office">Office</option>'+
  '<option value="Warehouse">Warehouse</option>'+
  '<option value="Home">Home</option>'+
  '</select>'+
  '</div>'+
  '<input data-target="add_address_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_address_'+count+'_address" placeholder="Address">'+
  '<div class="input-group-append">'+
  '<button data-target="add_address_'+count+'_'+cb_id+'" type="button" class="btn btn-danger remove_additional_address">'+
  '<i class="fas fa-times"></i>'+
  '</button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">City</div>'+
  '</div>'+
  '<input data-target="add_address_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_address_'+count+'_city" placeholder="City">'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>');

});

//ADDITIONAL CONTACT PERSON
$(".additional_person").on("click", function(){

  count++;
  var cb_id = $(this).attr("data-target");
  var contact_person_count = $("[data-target='add_person_container_"+cb_id+"']").length + 1;

  if (contact_person_count <= 5) {
    $(".add_default_person_"+cb_id+"").after('<div data-target="add_person_container_'+cb_id+'" id="add_person_'+count+'_'+cb_id+'" class="row p-1">'+
    '<div class="col-lg-6">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend">'+
    '<div class="input-group-text">Contact Person</div>'+
    '</div>'+
    '<input data-target="add_person_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_name" placeholder="Full Name">'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div class="col-lg-6">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend">'+
    '<div class="input-group-text">Position</div>'+
    '</div>'+
    '<input data-target="add_person_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_position" placeholder="Position">'+
    '<div class="input-group-append">'+
    '<button data-target="add_person_'+count+'_'+cb_id+'" type="button" class="btn btn-danger remove_additional_person">'+
    '<i class="fas fa-times"></i>'+
    '</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div data-target="add_contact_container_'+cb_id+'" id="add_person_'+count+'_contact_1_'+cb_id+'" class="col-lg-6">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend w-120">'+
    '<select data-target="add_person_field_'+count+'" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_contact_type_1">'+
    '<option value="/Mobile-" selected>Mobile</option>'+
    '<option value="/Landline-">Landline</option>'+
    '<option value="/Fax-">Fax</option>'+
    '<option value="/Email-">Email</option>'+
    '<option value="/WeChat-">WeChat</option>'+
    '</select>'+
    '</div>'+
    '<input data-target="add_person_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_contact_1" placeholder="Contact Details">'+
    '</div>'+
    '</div>'+
    '</div>'+
    '<div data-target="add_contact_container_'+cb_id+'" id="add_person_'+count+'_contact_2_'+cb_id+'" class="col-lg-6 add_default_contact_'+cb_id+'">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend w-120">'+
    '<select data-target="add_person_field_'+count+'" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_contact_type_2">'+
    '<option value="/Mobile-">Mobile</option>'+
    '<option value="/Landline-">Landline</option>'+
    '<option value="/Fax-">Fax</option>'+
    '<option value="/Email-" selected>Email</option>'+
    '<option value="/WeChat-">WeChat</option>'+
    '</select>'+
    '</div>'+
    '<input data-target="add_person_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' add_person_'+count+'_contact_2" placeholder="Contact Details">'+
    '<div class="input-group-append">'+
    '<button name="add_person_'+count+'" data-target="'+cb_id+'" type="button" class="btn btn-primary additional_contact">'+
    '<i class="fas fa-plus"></i>'+
    '</button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>');
  }
  else {
    contactPersonLimit();
  }

});

//ADDITIONAL CONTACT DETAIL
$(document).on("click", ".additional_contact", function(){

  count_2++;
  var get_cp = $(this).attr("name");
  var cb_id = $(this).attr("data-target");
  var contact_detail_count = $("#"+get_cp+"_"+cb_id+" [data-target='add_contact_container_"+cb_id+"']").length + 1;

  if (contact_detail_count <= 4) {
    $("#"+get_cp+"_"+cb_id+" .add_default_contact_"+cb_id+"").after('<div data-target="add_contact_container_'+cb_id+'" id="'+get_cp+'_contact_'+count_2+'_'+cb_id+'" class="col-lg-6">'+
    '<div class="form-group">'+
    '<div class="input-group mb-2">'+
    '<div class="input-group-prepend w-120">'+
    '<select data-target="add_person_field_'+count+'" class="form-control additional_fields_'+cb_id+' '+get_cp+'_contact_type_'+count_2+'">'+
    '<option value="">--Select--</option>'+
    '<option value="/Mobile-">Mobile</option>'+
    '<option value="/Landline-">Landline</option>'+
    '<option value="/Fax-">Fax</option>'+
    '<option value="/Email-">Email</option>'+
    '<option value="/WeChat-">WeChat</option>'+
    '</select>'+
    '</div>'+
    '<input data-target="add_person_field_'+count+'" type="text" class="form-control additional_fields_'+cb_id+' '+get_cp+'_contact_'+count_2+'" placeholder="Contact Details">'+
    '<div class="input-group-append">'+
    '<button data-target="'+get_cp+'_contact_'+count_2+'_'+cb_id+'" type="button" class="btn btn-danger remove_additional_contact">'+
    '<i class="fas fa-times"></i>'+
    '</button>'+
    '</div>'+
    '</div>'+
    '</div>');
  }
  else {
    contactDetailLimit();
  }

});

$(".add_consignee").on("click", function(){

  count++;
  var cb_id = $(this).attr("data-target");
  var consignee_count = parseInt($(this).attr("class")[0]) + count;

  $(".default_consignee_"+cb_id+"").after('<div data-target="consignee_container_'+cb_id+'" id="consignee_'+consignee_count+'_'+cb_id+'" class="row">'+
  '<div class="col-lg-12">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">Consignee Company</div>'+
  '</div>'+
  '<input class="form-control consignee_'+consignee_count+'_name consignee_fields_'+cb_id+'" type="text">'+
  '<div class="input-group-append">'+
  '<button data-target="consignee_'+consignee_count+'_'+cb_id+'" type="button" class="btn btn-danger remove_consignee">'+
  '<i class="fas fa-times"></i>'+
  '</button>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-12">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">Address</div>'+
  '</div>'+
  '<input class="form-control consignee_'+consignee_count+'_address consignee_fields_'+cb_id+'" type="text">'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">SEC/DTI Reg.</div>'+
  '</div>'+
  '<input class="form-control consignee_'+consignee_count+'_registration consignee_fields_'+cb_id+'" type="text">'+
  '</div>'+
  '</div>'+
  '</div>'+
  '<div class="col-lg-6">'+
  '<div class="form-group">'+
  '<div class="input-group mb-2">'+
  '<div class="input-group-prepend">'+
  '<div class="input-group-text">TIN</div>'+
  '</div>'+
  '<input class="form-control consignee_'+consignee_count+'_tin consignee_fields_'+cb_id+'" type="text">'+
  '</div>'+
  '</div>'+
  '</div>'+
  '</div>');

});

//REMOVE CONTACT PERSON, ADDRESS & CONTACT BUTTON
$(document).on("click", ".remove_address, .remove_additional_address, .remove_additional_person, .remove_additional_contact, .remove_person, .remove_contact, .remove_consignee", function(){
  var get_id = $(this).attr("data-target");
  $("#"+get_id).remove();
});







































//
