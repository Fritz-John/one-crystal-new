<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalCustomerItem extends Model
{
    use HasFactory;

    public function customer()
    {
      return $this->belongsTo('App\Models\LocalCustomer', 'lc_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Items', 'item_id');
    }

    public function container(){
      return $this->belongsTo('App\Models\Containers', 'ic_id');
    }
}
