<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomsBrokerage extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\CustomsBrokerageAddress', 'cb_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\CustomsBrokerageContactPerson', 'cb_id', 'id');
    }

    public function consignee()
    {
        return $this->hasMany('App\Models\CustomsBrokerageConsignee', 'cb_id', 'id');
    }
}
