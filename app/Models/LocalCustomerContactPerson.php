<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalCustomerContactPerson extends Model
{
    use HasFactory;

    public function customer()
    {
      return $this->belongsTo('App\Models\LocalCustomer', 'lc_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\LocalCustomerContact', 'lc_cp_id', 'id');
    }
}
