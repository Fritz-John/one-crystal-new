<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignPartnersContact extends Model
{
    use HasFactory;

    public function person()
    {
      return $this->belongsTo('App\Models\ForeignPartnersContactPerson', 'fp_cp_id');
    }
}
