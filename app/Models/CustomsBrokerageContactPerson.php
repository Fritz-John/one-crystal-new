<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomsBrokerageContactPerson extends Model
{
    use HasFactory;

    public function customs()
    {
      return $this->belongsTo('App\Models\CustomsBrokerage', 'cb_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\CustomsBrokerageContact', 'cb_cp_id', 'id');
    }
}
