<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingLineAddress extends Model
{
    use HasFactory;

    public function shipping(){
      return $this->belongsTo('App\Models\ShippingLine', 'sl_id');
    }
}
