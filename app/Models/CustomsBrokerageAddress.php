<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomsBrokerageAddress extends Model
{
    use HasFactory;

    public function customs(){
      return $this->belongsTo('App\Models\CustomsBrokerage', 'cb_id');
    }
}
