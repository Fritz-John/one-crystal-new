<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckingCompanyPlate extends Model
{
    use HasFactory;

    public function customs()
    {
      return $this->belongsTo('App\Models\TruckingCompany', 'tc_id');
    }
}
