<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignPartnersContactPerson extends Model
{
    use HasFactory;

    public function partner()
    {
      return $this->belongsTo('App\Models\ForeignPartners', 'fp_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\ForeignPartnersContact', 'fp_cp_id', 'id');
    }
}
