<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LocalCustomerAddress extends Model
{
    use HasFactory;

    public function customer(){
      return $this->belongsTo('App\Models\LocalCustomer', 'lc_id');
    }
}
