<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TruckingCompany extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\TruckingCompanyAddress', 'tc_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\TruckingCompanyContactPerson', 'tc_id', 'id');
    }

    public function plate()
    {
        return $this->hasMany('App\Models\TruckingCompanyPlate', 'tc_id', 'id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}
