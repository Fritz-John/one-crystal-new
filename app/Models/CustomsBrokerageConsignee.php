<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomsBrokerageConsignee extends Model
{
    use HasFactory;

    public function customs()
    {
      return $this->belongsTo('App\Models\CustomsBrokerage', 'cb_id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}
