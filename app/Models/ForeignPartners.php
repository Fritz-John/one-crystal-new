<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignPartners extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\ForeignPartnersAddress', 'fp_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\ForeignPartnersContactPerson', 'fp_id', 'id');
    }

    public function shipper()
    {
        return $this->hasMany('App\Models\ForeignPartnersShipper', 'fp_id', 'id');
    }
}
