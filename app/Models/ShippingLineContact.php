<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingLineContact extends Model
{
    use HasFactory;

    public function person()
    {
      return $this->belongsTo('App\Models\ShippingLineContactPerson', 'sl_cp_id');
    }
}
