<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingLineContactPerson extends Model
{
    use HasFactory;

    public function shipping()
    {
      return $this->belongsTo('App\Models\ShippingLine', 'sl_id');
    }

    public function detail()
    {
        return $this->hasMany('App\Models\ShippingLineContact', 'sl_cp_id', 'id');
    }
}
