<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ForeignPartnersShipper extends Model
{
    use HasFactory;

    public function partner()
    {
      return $this->belongsTo('App\Models\ForeignPartners', 'fp_id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }

}
