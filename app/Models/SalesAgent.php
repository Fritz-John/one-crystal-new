<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SalesAgent extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\SalesAgentAddress', 'sa_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\SalesAgentContact', 'sa_id', 'id');
    }

    public function customer()
    {
        return $this->hasMany('App\Models\LocalCustomer', 'lc_salesman', 'id');
    }
}
