<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShippingLine extends Model
{
    use HasFactory;

    public function address()
    {
        return $this->hasMany('App\Models\ShippingLineAddress', 'sl_id', 'id');
    }

    public function contact()
    {
        return $this->hasMany('App\Models\ShippingLineContactPerson', 'sl_id', 'id');
    }

    public function container()
    {
      return $this->belongsTo('App\Models\Containers', 'id');
    }
}
