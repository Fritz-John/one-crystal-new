<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\SalesAgent;
use App\Models\Items;
use App\Models\LocalCustomer;
use App\Models\LocalCustomerAddress;
use App\Models\LocalCustomerContactPerson;
use App\Models\LocalCustomerContact;
use App\Models\LocalCustomerItem;

class LocalCustomerController extends Controller
{

  public function index(){

    $get_lc_details = $get_agents = $get_items = $get_lc_items = [];

    $selected_status = Session::get("lc_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_local_customers = LocalCustomer::with(["address", "contact.detail" => function($sub_query){
      $sub_query->where("lc_cp_contact_type", "Mobile")->orWhere("lc_cp_contact_type", "Email");
    }])->where("status", $status)->get();

    if ($status == "Active") {
      $get_lc_details = LocalCustomer::with("address", "contact.detail", "item")->where("status", $status)->get();
      $get_agents = SalesAgent::where("status", $status)->get();
      $get_items = Items::where("status", $status)->get();
      $get_lc_items = LocalCustomerItem::with("item", "container")->get();
    }

    return view('profiles.local-customer.index', compact('get_agents', 'get_local_customers', 'get_lc_details', 'get_items', 'get_lc_items', 'status'));
  }

  public function store(Request $request){

    $lc_name = $request->lc_name;
    $lc_address_arr = $request->lc_address;
    $lc_cp_arr = $request->lc_contact_person;
    $lc_terms = $request->lc_terms;
    $lc_salesman = $request->lc_salesman;
    $lc_note = $request->lc_note;

    $new_lc = new LocalCustomer();
    $new_lc->lc_name = $lc_name;
    $new_lc->lc_terms = $lc_terms;
    $new_lc->lc_salesman = $lc_salesman;
    $new_lc->lc_note = $lc_note;
    $new_lc->status = "Active";
    $new_lc->save();
    $new_lc_id = $new_lc->id;

    foreach ($lc_address_arr as $lc_address) {
      $explode_address = explode(",", $lc_address);

      foreach ($explode_address as $lc_add) {
        $explode = explode("/", $lc_add);
        $address_type = $explode[0];
        $address = $explode[1];
        $city = $explode[2];

        $insert_address[] = [
          "lc_address_type" => $address_type,
          "lc_address" => $address,
          "lc_city" => $city,
          "lc_id" => $new_lc_id,
        ];
      }
    }
    LocalCustomerAddress::insert($insert_address);

    foreach ($lc_cp_arr as $lc_cp) {
      $explode_cp = explode(",", $lc_cp);

      foreach ($explode_cp as $cp) {
        $explode_val = explode("/", $cp);
        $arr_count = count($explode_val);
        $cp_name = $explode_val[0];
        $cp_position = $explode_val[1];

        $new_cp = new LocalCustomerContactPerson();
        $new_cp->lc_cp_name = $cp_name;
        $new_cp->lc_cp_position = $cp_position;
        $new_cp->lc_id = $new_lc_id;
        $new_cp->save();
        $new_cp_id = $new_cp->id;

        for ($i=2; $i <= $arr_count-1; $i++) {
          $explode_cp_contact = explode("-", $explode_val[$i]);
          $contact_type = $explode_cp_contact[0];
          $contact = $explode_cp_contact[1];

          $insert_contact[] = [
            "lc_cp_contact_type" => $contact_type,
            "lc_cp_contact" => $contact,
            "lc_cp_id" => $new_cp_id,
          ];
        }
      }
      LocalCustomerContact::insert($insert_contact);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $lc_id = $request->lc_id;

    if ($type == "main_details") {

      $lc_name = $request->lc_name;
      $lc_address_arr = $request->lc_address;
      $lc_cp_arr = $request->lc_contact_person;
      $lc_terms = $request->lc_terms;
      $lc_salesman = $request->lc_salesman;
      $lc_note = $request->lc_note;

      LocalCustomer::where("id", $lc_id)->update(array(
        "lc_name" => $lc_name,
        "lc_terms" => $lc_terms,
        "lc_salesman" => $lc_salesman,
        "lc_note" => $lc_note,
      ));

      foreach ($lc_address_arr as $lc_address) {
        $explode_address = explode(",", $lc_address);

        foreach ($explode_address as $lc_add) {
          $explode = explode("/", $lc_add);
          $address_id = $explode[0];
          $address_type = $explode[1];
          $address = $explode[2];
          $city = $explode[3];

          LocalCustomerAddress::where("id", $address_id)->update(array(
            "lc_address_type" => $address_type,
            "lc_address" => $address,
            "lc_city" => $city,
          ));
        }
      }

      foreach ($lc_cp_arr as $lc_cp) {
        $explode_cp = explode(",", $lc_cp);

        foreach ($explode_cp as $cp) {
          $explode_val = explode("/", $cp);
          $arr_count = count($explode_val);
          $cp_id = $explode_val[0];
          $cp_name = $explode_val[1];
          $cp_position = $explode_val[2];

          LocalCustomerContactPerson::where("id", $cp_id)->update(array(
            "lc_cp_name" => $cp_name,
            "lc_cp_position" => $cp_position,
          ));

          for ($i=3; $i <= $arr_count-1; $i++) {
            $explode_cp_contact = explode("-", $explode_val[$i]);
            $contact_type = $explode_cp_contact[0];
            $contact = $explode_cp_contact[1];
            $contact_id = $explode_cp_contact[2];

            LocalCustomerContact::where("id", $contact_id)->update(array(
              "lc_cp_contact_type" => $contact_type,
              "lc_cp_contact" => $contact,
            ));
          }
        }
      }

    }
    elseif ($type == "add_details") {

      $lc_address_arr = $request->lc_address;
      $lc_cp_arr = $request->lc_contact_person;

      if (array_filter($lc_address_arr) != []) {
        foreach ($lc_address_arr as $lc_address) {
          $explode_address = explode(",", $lc_address);

          foreach ($explode_address as $lc_add) {
            $explode = explode("/", $lc_add);
            $address_type = $explode[0];
            $address = $explode[1];
            $city = $explode[2];

            $insert_address[] = [
              "lc_address_type" => $address_type,
              "lc_address" => $address,
              "lc_city" => $city,
              "lc_id" => $lc_id,
            ];
          }
        }
        LocalCustomerAddress::insert($insert_address);
      }

      if (array_filter($lc_cp_arr) != []) {
        foreach ($lc_cp_arr as $lc_cp) {
          $explode_cp = explode(",", $lc_cp);

          foreach ($explode_cp as $cp) {
            $explode_val = explode("/", $cp);
            $arr_count = count($explode_val);
            $cp_name = $explode_val[0];
            $cp_position = $explode_val[1];

            $new_cp = new LocalCustomerContactPerson();
            $new_cp->lc_cp_name = $cp_name;
            $new_cp->lc_cp_position = $cp_position;
            $new_cp->lc_id = $lc_id;
            $new_cp->save();
            $new_cp_id = $new_cp->id;

            for ($i=2; $i <= $arr_count-1; $i++) {
              $explode_cp_contact = explode("-", $explode_val[$i]);
              $contact_type = $explode_cp_contact[0];
              $contact = $explode_cp_contact[1];

              $insert_contact[] = [
                "lc_cp_contact_type" => $contact_type,
                "lc_cp_contact" => $contact,
                "lc_cp_id" => $new_cp_id,
              ];
            }
          }
          LocalCustomerContact::insert($insert_contact);
        }
      }

    }
    else {

      $lc_item_arr = $request->lc_item;

      LocalCustomerItem::where("lc_id", $lc_id)->delete();

      foreach ($lc_item_arr as $lc_item) {
        $explode_item = explode(",", $lc_item);
        foreach ($explode_item as $item) {
          $explode_val = explode("/", $item);
          $insert_item[] = [
            "item_id" => $explode_val[0],
            "package_price" => ($explode_val[1] != "") ? $explode_val[1] : "N/A",
            "lc_id" => $lc_id,
          ];
        }
      }
      LocalCustomerItem::insert($insert_item);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      LocalCustomerAddress::where("id", $request->get_address_id)->delete();
    }
    elseif ($get_type == "person") {
      LocalCustomerContact::where("lc_cp_id", $request->get_person_id)->delete();
      LocalCustomerContactPerson::where("id", $request->get_person_id)->delete();
    }
    elseif ($get_type == "contact") {
      LocalCustomerContact::where("id", $request->get_contact_id)->delete();
    }
    else {
      LocalCustomerItem::where("id", $request->get_item_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

}




























//
