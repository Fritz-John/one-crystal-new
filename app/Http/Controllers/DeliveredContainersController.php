<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\Containers;
use App\Models\CustomsBrokerage;
use App\Models\User;

class DeliveredContainersController extends Controller
{

  public function index(){

    $selected_daterange = Session::get("dc_daterange");
    $brokerage_arr = [];

    if ($selected_daterange != "") {
      $explode_val = explode("/", $selected_daterange);
      $start = $explode_val[0];
      $end = $explode_val[1];
    }
    else {
      $start = date('Y-m-d', strtotime('-3 month'));
      $end = date('Y-m-d');
    }

    $get_delivered_containers = Containers::with("shipper","consignee","shipping",
                                "loading","discharge","item", "trucking",
                                "customer","customer.item","encoder","recorder")
                              ->where("ic_status", "Delivered")
                              ->where("status", "Active")
                              ->whereDate("ic_delivery_date", ">=", $start)
                              ->whereDate("ic_delivery_date", "<=", $end)
                              ->get();


    foreach ($get_delivered_containers as $dc) {
      array_push($brokerage_arr, $dc->consignee->cb_id);
    }

    $get_brokerage = CustomsBrokerage::whereIn("id", $brokerage_arr)->where("status", "Active")->get();

    return view('transactions.delivered-containers.index', compact('get_delivered_containers', 'get_brokerage', 'start', 'end'));
    
  }

}
