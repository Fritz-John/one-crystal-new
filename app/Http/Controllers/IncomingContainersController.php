<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\ForeignPartnersShipper;
use App\Models\CustomsBrokerageConsignee;
use App\Models\ShippingLine;
use App\Models\Ports;
use App\Models\Items;
use App\Models\LocalCustomer;
use App\Models\LocalCustomerItem;
use App\Models\Containers;
use App\Models\User;

class IncomingContainersController extends Controller
{

  public function index(){

    $selected_daterange = Session::get("ic_daterange");

    if ($selected_daterange != "") {
      $explode_val = explode("/", $selected_daterange);
      $start = $explode_val[0];
      $end = $explode_val[1];
    }
    else {
      $start = date('Y-m-d', strtotime('-3 month'));
      $end = date('Y-m-d');
    }

    $get_shippers = ForeignPartnersShipper::all();
    $get_consignees = CustomsBrokerageConsignee::all();
    $get_shipping_lines = ShippingLine::where("status", "Active")->get();
    $get_ports = Ports::where("status", "Active")->get();
    $get_items = Items::with("customer_item")->where("status", "Active")->get();
    $get_local_customers = LocalCustomer::where("status", "Active")->get();

    $get_incoming_containers = Containers::with("shipper","consignee","shipping",
                                "loading","discharge","item",
                                "customer","encoder","recorder")
                              ->where("ic_status", "Incoming")
                              ->where("status", "Active")
                              ->whereDate("created_at", ">=", $start)
                              ->whereDate("created_at", "<=", $end)
                              ->get();

    return view('transactions.incoming-containers.index',
    compact(
      'get_shippers',
      'get_consignees',
      'get_shipping_lines',
      'get_ports',
      'get_items',
      'get_local_customers',
      'get_incoming_containers',
      'start',
      'end',
    ));
  }

  public function store(Request $request){

    $user = Auth::guard()->user();
    $user_id = $user->id;
    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $datetime = date('Y-m-d H:i:s');
    $ic_shipper = $request->ic_shipper;
    $ic_consignee = $request->ic_consignee;
    $ic_blno = $request->ic_blno;
    $ic_shipping_line = $request->ic_shipping_line;
    $ic_loading_port = $request->ic_loading_port;
    $ic_discharge_port = $request->ic_discharge_port;
    $ic_local_customer = $request->ic_local_customer;
    $ic_need_acfta = $request->ic_need_acfta;
    $ic_external_trucking = $request->ic_external_trucking;
    $ic_customer_request = $request->ic_customer_request;
    $ic_container_arr = $request->ic_container;

    if (array_filter($ic_container_arr) != []) {
      foreach ($ic_container_arr as $ic_container) {
        $explode_container = explode(",", $ic_container);

        foreach ($explode_container as $ic_con) {
          $explode = explode("/", $ic_con);
          $container_size = $explode[0];
          $item = $explode[1];
          $container_no = $explode[2];
          $packages = $explode[3];
          $gross_weight = $explode[4];

          $insert_container = new Containers();
          $insert_container->ic_shipper_id = $ic_shipper;
          $insert_container->ic_consignee_id = $ic_consignee;
          $insert_container->ic_blno = $ic_blno;
          $insert_container->ic_shipping_id = $ic_shipping_line;
          $insert_container->ic_loading_port = $ic_loading_port;
          $insert_container->ic_discharge_port = $ic_discharge_port;

          $insert_container->ic_container_size = $container_size;
          $insert_container->ic_items = $item;
          $insert_container->ic_container_no = $container_no;
          $insert_container->ic_packages = $packages;
          $insert_container->ic_gross_weight = $gross_weight;

          $insert_container->ic_customer = $ic_local_customer;
          $insert_container->ic_need_acfta = $ic_need_acfta;
          $insert_container->ic_external_trucking = $ic_external_trucking;
          $insert_container->ic_customer_request = $ic_customer_request;
          $insert_container->ic_encoded_by = $user_id;
          $insert_container->ic_status = "Incoming";
          $insert_container->ic_eta = $date;
          $insert_container->ic_in_transit = "No";
          $insert_container->ic_in_yard = "No";
          $insert_container->status = "Active";
          $insert_container->created_at = $datetime;
          $insert_container->save();
          $new_ic_id = $insert_container->id;

          $insert_item = new LocalCustomerItem();
          $insert_item->item_id = $item;
          $insert_item->lc_id = $ic_local_customer;
          $insert_item->ic_id = $new_ic_id;
          $insert_item->save();

        }
      }
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function additionalDetail(Request $request){

    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $user = Auth::guard()->user();
    $user_id = $user->id;
    $user_name = ucwords($user->name);
    $ic_id = $request->ic_id;
    $request_type = $request->request_type;

    function generateRandomString($length) {
      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    $random_str = generateRandomString(10);

    switch ($request_type) {
      case 'eta_date':
        $query = array(
          "ic_eta" => $request->get_eta_date,
          "ic_eta_acfta_by" => $user_id
        );
        break;

      case 'upload_bl':
        $file = $request->file('get_bl_file');
        $name = $file->getClientOriginalName();
        $file->move(public_path('storage/incoming-container-files/'), 'BL_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name);
        $url = 'storage/incoming-container-files/BL_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name;
        $query = array(
          "ic_bl_date" => $date,
          "ic_bl_file" => $url,
        );
        break;

      case 'upload_pl':
        $file = $request->file('get_pl_file');
        $name = $file->getClientOriginalName();
        $file->move(public_path('storage/incoming-container-files/'), 'PL_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name);
        $url = 'storage/incoming-container-files/PL_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name;
        $query = array(
          "ic_pl_date" => $date,
          "ic_pl_file" => $url,
        );
        break;

      case 'upload_acfta':
        $file = $request->file('get_acfta_file');
        $name = $file->getClientOriginalName();
        $file->move(public_path('storage/incoming-container-files/'), 'ACFTA_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name);
        $url = 'storage/incoming-container-files/ACFTA_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name;
        $query = array(
          "ic_acfta_date" => $date,
          "ic_acfta_file" => $url,
          "ic_eta_acfta_by" => $user_id
        );
        break;

      default:

        break;
    }

    Containers::where("id", $ic_id)->update($query);
    $get_container = Containers::find($ic_id);

    return response()->json(array(
      "status" => "success",
      "user_name" => $user_name,
      "eta_date_raw" => $get_container->ic_eta,
      "eta_date" => date("m-d-Y", strtotime($get_container->ic_eta)),
      "bl_date" => date("m-d-Y", strtotime($get_container->ic_bl_date)),
      "pl_date" => date("m-d-Y", strtotime($get_container->ic_pl_date)),
      "acfta_date" => date("m-d-Y", strtotime($get_container->ic_acfta_date)),
      "bl_file" => $get_container->ic_bl_file,
      "pl_file" => $get_container->ic_pl_file,
      "acfta_file" => $get_container->ic_acfta_file,
    ));
  }

}




































//
