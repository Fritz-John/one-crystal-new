<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\Ports;
use Monarobase\CountryList\CountryListFacade;

class PortsController extends Controller
{
    public function index(){

      $countries = [];

      $selected_status = Session::get("port_status");
      $status = ($selected_status != "") ? $selected_status : "Active";

      $get_ports = Ports::where("status", $status)->get();

      if ($status == "Active") {
        $countries = CountryListFacade::getList('en');
      }

      return view('profiles.ports.index', compact('countries', 'get_ports', 'status'));
    }

    public function store(Request $request){

      $port_country = $request->port_country;
      $port_name = $request->port_name;

      $new_port = new Ports();
      $new_port->port_country = $port_country;
      $new_port->port_name = $port_name;
      $new_port->status = "Active";
      $new_port->save();

      return response()->json(array(
        "status" => "success",
      ));
    }

    public function edit(Request $request){

      $port_id = $request->get_port_id;
      $port_country = $request->edit_port_country;
      $port_name = $request->edit_port_name;

      Ports::where("id", $port_id)->update(array(
        "port_country" => $port_country,
        "port_name" => $port_name,
      ));

      return response()->json(array(
        "status" => "success",
      ));
    }

}














//
