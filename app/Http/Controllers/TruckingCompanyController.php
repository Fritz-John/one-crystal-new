<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\TruckingCompany;
use App\Models\TruckingCompanyAddress;
use App\Models\TruckingCompanyContactPerson;
use App\Models\TruckingCompanyContact;
use App\Models\TruckingCompanyPlate;


class TruckingCompanyController extends Controller
{

  public function index(){

    $get_tc_details = [];

    $selected_status = Session::get("tc_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_trucking_companies = TruckingCompany::with(["address", "contact.detail" => function($sub_query){
      $sub_query->where("tc_cp_contact_type", "Mobile")->orWhere("tc_cp_contact_type", "Email");
    }])->where("status", $status)->get();

    if ($status == "Active") {
      $get_tc_details = TruckingCompany::with("address", "contact.detail", "plate")->where("status", $status)->get();
    }

    return view('profiles.trucking-company.index', compact('get_trucking_companies', 'get_tc_details', 'status'));
  }

  public function store(Request $request){

    $tc_name = $request->tc_name;
    $tc_address_arr = $request->tc_address;
    $tc_cp_arr = $request->tc_contact_person;
    $tc_terms = $request->tc_terms;
    $tc_note = $request->tc_note;

    $new_tc = new TruckingCompany();
    $new_tc->tc_name = $tc_name;
    $new_tc->tc_terms = $tc_terms;
    $new_tc->tc_note = $tc_note;
    $new_tc->status = "Active";
    $new_tc->save();
    $new_tc_id = $new_tc->id;

    foreach ($tc_address_arr as $tc_address) {
      $explode_address = explode(",", $tc_address);

      foreach ($explode_address as $tc_add) {
        $explode = explode("/", $tc_add);
        $address_type = $explode[0];
        $address = $explode[1];
        $city = $explode[2];

        $insert_address[] = [
          "tc_address_type" => $address_type,
          "tc_address" => $address,
          "tc_city" => $city,
          "tc_id" => $new_tc_id,
        ];
      }
    }
    TruckingCompanyAddress::insert($insert_address);

    foreach ($tc_cp_arr as $tc_cp) {
      $explode_cp = explode(",", $tc_cp);

      foreach ($explode_cp as $cp) {
        $explode_val = explode("/", $cp);
        $arr_count = count($explode_val);
        $cp_name = $explode_val[0];
        $cp_position = $explode_val[1];

        $new_cp = new TruckingCompanyContactPerson();
        $new_cp->tc_cp_name = $cp_name;
        $new_cp->tc_cp_position = $cp_position;
        $new_cp->tc_id = $new_tc_id;
        $new_cp->save();
        $new_cp_id = $new_cp->id;

        for ($i=2; $i <= $arr_count-1; $i++) {
          $explode_cp_contact = explode("-", $explode_val[$i]);
          $contact_type = $explode_cp_contact[0];
          $contact = $explode_cp_contact[1];

          $insert_contact[] = [
            "tc_cp_contact_type" => $contact_type,
            "tc_cp_contact" => $contact,
            "tc_cp_id" => $new_cp_id,
          ];
        }
      }
      TruckingCompanyContact::insert($insert_contact);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $tc_id = $request->tc_id;
    $new_plate_id = "";

    if ($type == "main_details") {

      $tc_name = $request->tc_name;
      $tc_address_arr = $request->tc_address;
      $tc_cp_arr = $request->tc_contact_person;
      $tc_terms = $request->tc_terms;
      $tc_note = $request->tc_note;

      TruckingCompany::where("id", $tc_id)->update(array(
        "tc_name" => $tc_name,
        "tc_terms" => $tc_terms,
        "tc_note" => $tc_note,
      ));

      foreach ($tc_address_arr as $tc_address) {
        $explode_address = explode(",", $tc_address);

        foreach ($explode_address as $tc_add) {
          $explode = explode("/", $tc_add);
          $address_id = $explode[0];
          $address_type = $explode[1];
          $address = $explode[2];
          $city = $explode[3];

          TruckingCompanyAddress::where("id", $address_id)->update(array(
            "tc_address_type" => $address_type,
            "tc_address" => $address,
            "tc_city" => $city,
          ));
        }
      }

      foreach ($tc_cp_arr as $tc_cp) {
        $explode_cp = explode(",", $tc_cp);

        foreach ($explode_cp as $cp) {
          $explode_val = explode("/", $cp);
          $arr_count = count($explode_val);
          $cp_id = $explode_val[0];
          $cp_name = $explode_val[1];
          $cp_position = $explode_val[2];

          TruckingCompanyContactPerson::where("id", $cp_id)->update(array(
            "tc_cp_name" => $cp_name,
            "tc_cp_position" => $cp_position,
          ));

          for ($i=3; $i <= $arr_count-1; $i++) {
            $explode_cp_contact = explode("-", $explode_val[$i]);
            $contact_type = $explode_cp_contact[0];
            $contact = $explode_cp_contact[1];
            $contact_id = $explode_cp_contact[2];

            TruckingCompanyContact::where("id", $contact_id)->update(array(
              "tc_cp_contact_type" => $contact_type,
              "tc_cp_contact" => $contact,
            ));
          }
        }
      }

    }
    elseif ($type == "add_details") {

      $tc_address_arr = $request->tc_address;
      $tc_cp_arr = $request->tc_contact_person;

      if (array_filter($tc_address_arr) != []) {
        foreach ($tc_address_arr as $tc_address) {
          $explode_address = explode(",", $tc_address);

          foreach ($explode_address as $tc_add) {
            $explode = explode("/", $tc_add);
            $address_type = $explode[0];
            $address = $explode[1];
            $city = $explode[2];

            $insert_address[] = [
              "tc_address_type" => $address_type,
              "tc_address" => $address,
              "tc_city" => $city,
              "tc_id" => $tc_id,
            ];
          }
        }
        TruckingCompanyAddress::insert($insert_address);
      }

      if (array_filter($tc_cp_arr) != []) {
        foreach ($tc_cp_arr as $tc_cp) {
          $explode_cp = explode(",", $tc_cp);

          foreach ($explode_cp as $cp) {
            $explode_val = explode("/", $cp);
            $arr_count = count($explode_val);
            $cp_name = $explode_val[0];
            $cp_position = $explode_val[1];

            $new_cp = new TruckingCompanyContactPerson();
            $new_cp->tc_cp_name = $cp_name;
            $new_cp->tc_cp_position = $cp_position;
            $new_cp->tc_id = $tc_id;
            $new_cp->save();
            $new_cp_id = $new_cp->id;

            for ($i=2; $i <= $arr_count-1; $i++) {
              $explode_cp_contact = explode("-", $explode_val[$i]);
              $contact_type = $explode_cp_contact[0];
              $contact = $explode_cp_contact[1];

              $insert_contact[] = [
                "tc_cp_contact_type" => $contact_type,
                "tc_cp_contact" => $contact,
                "tc_cp_id" => $new_cp_id,
              ];
            }
          }
          TruckingCompanyContact::insert($insert_contact);
        }
      }

    }
    else {

      $plate = $request->get_plate;

      $new_plate = new TruckingCompanyPlate();
      $new_plate->tc_plate = $plate;
      $new_plate->tc_id = $tc_id;
      $new_plate->save();
      $new_plate_id .= $new_plate->id;

    }

    return response()->json(array(
      "status" => "success",
      "plate_id" => $new_plate_id,
    ));
  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      TruckingCompanyAddress::where("id", $request->get_address_id)->delete();
    }
    elseif ($get_type == "person") {
      TruckingCompanyContact::where("tc_cp_id", $request->get_person_id)->delete();
      TruckingCompanyContactPerson::where("id", $request->get_person_id)->delete();
    }
    elseif ($get_type == "contact") {
      TruckingCompanyContact::where("id", $request->get_contact_id)->delete();
    }
    else {
      TruckingCompanyPlate::where("id", $request->get_plate_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

}
