<?php

namespace App\Http\Controllers;

use Session;
use App\Models\Items;
use Illuminate\Http\Request;

class ItemsController extends Controller
{

  public function index(){

    $selected_status = Session::get("item_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_items = Items::where("status", $status)->get();

    return view('profiles.items.index', compact('get_items', 'status'));
  }

  public function store(Request $request){

    $item_name = $request->item_name;

    $new_item = new Items();
    $new_item->item_name = $item_name;
    $new_item->status = "Active";
    $new_item->save();

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $item_id = $request->get_item_id;
    $item_name = $request->edit_item_name;

    Items::where("id", $item_id)->update(array(
      "item_name" => $item_name,
    ));

    return response()->json(array(
      "status" => "success",
    ));
  }

}
