<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Models\CustomsBrokerage;
use App\Models\CustomsBrokerageAddress;
use App\Models\CustomsBrokerageContactPerson;
use App\Models\CustomsBrokerageContact;
use App\Models\CustomsBrokerageConsignee;

class CustomsBrokerageController extends Controller
{
  public function index(){

    $get_cb_details = [];

    $selected_status = Session::get("cb_status");
    $status = ($selected_status != "") ? $selected_status : "Active";

    $get_customs_brokerage = CustomsBrokerage::with(["address", "contact.detail" => function($sub_query){
      $sub_query->where("cb_cp_contact_type", "Mobile")->orWhere("cb_cp_contact_type", "Email");
    }])->where("status", $status)->get();

    if ($status == "Active") {
      $get_cb_details = CustomsBrokerage::with("address", "contact.detail", "consignee")->where("status", $status)->get();
    }

    return view('profiles.customs-brokerage.index', compact('get_customs_brokerage', 'get_cb_details', 'status'));
  }

  public function store(Request $request){

    $cb_name = $request->cb_name;
    $cb_address_arr = $request->cb_address;
    $cb_cp_arr = $request->cb_contact_person;
    $cb_terms = $request->cb_terms;
    $cb_note = $request->cb_note;

    $new_cb = new CustomsBrokerage();
    $new_cb->cb_name = $cb_name;
    $new_cb->cb_terms = $cb_terms;
    $new_cb->cb_note = $cb_note;
    $new_cb->status = "Active";
    $new_cb->save();
    $new_cb_id = $new_cb->id;

    foreach ($cb_address_arr as $cb_address) {
      $explode_address = explode(",", $cb_address);

      foreach ($explode_address as $cb_add) {
        $explode = explode("/", $cb_add);
        $address_type = $explode[0];
        $address = $explode[1];
        $city = $explode[2];

        $insert_address[] = [
          "cb_address_type" => $address_type,
          "cb_address" => $address,
          "cb_city" => $city,
          "cb_id" => $new_cb_id,
        ];
      }
    }
    CustomsBrokerageAddress::insert($insert_address);

    foreach ($cb_cp_arr as $cb_cp) {
      $explode_cp = explode(",", $cb_cp);

      foreach ($explode_cp as $cp) {
        $explode_val = explode("/", $cp);
        $arr_count = count($explode_val);
        $cp_name = $explode_val[0];
        $cp_position = $explode_val[1];

        $new_cp = new CustomsBrokerageContactPerson();
        $new_cp->cb_cp_name = $cp_name;
        $new_cp->cb_cp_position = $cp_position;
        $new_cp->cb_id = $new_cb_id;
        $new_cp->save();
        $new_cp_id = $new_cp->id;

        for ($i=2; $i <= $arr_count-1; $i++) {
          $explode_cp_contact = explode("-", $explode_val[$i]);
          $contact_type = $explode_cp_contact[0];
          $contact = $explode_cp_contact[1];

          $insert_contact[] = [
            "cb_cp_contact_type" => $contact_type,
            "cb_cp_contact" => $contact,
            "cb_cp_id" => $new_cp_id,
          ];
        }
      }
      CustomsBrokerageContact::insert($insert_contact);
    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function edit(Request $request){

    $type = $request->type;
    $cb_id = $request->cb_id;

    if ($type == "main_details") {

      $cb_name = $request->cb_name;
      $cb_address_arr = $request->cb_address;
      $cb_cp_arr = $request->cb_contact_person;
      $cb_terms = $request->cb_terms;
      $cb_note = $request->cb_note;

      CustomsBrokerage::where("id", $cb_id)->update(array(
        "cb_name" => $cb_name,
        "cb_terms" => $cb_terms,
        "cb_note" => $cb_note,
      ));

      foreach ($cb_address_arr as $cb_address) {
        $explode_address = explode(",", $cb_address);

        foreach ($explode_address as $cb_add) {
          $explode = explode("/", $cb_add);
          $address_id = $explode[0];
          $address_type = $explode[1];
          $address = $explode[2];
          $city = $explode[3];

          CustomsBrokerageAddress::where("id", $address_id)->update(array(
            "cb_address_type" => $address_type,
            "cb_address" => $address,
            "cb_city" => $city,
          ));
        }
      }

      foreach ($cb_cp_arr as $cb_cp) {
        $explode_cp = explode(",", $cb_cp);

        foreach ($explode_cp as $cp) {
          $explode_val = explode("/", $cp);
          $arr_count = count($explode_val);
          $cp_id = $explode_val[0];
          $cp_name = $explode_val[1];
          $cp_position = $explode_val[2];

          CustomsBrokerageContactPerson::where("id", $cp_id)->update(array(
            "cb_cp_name" => $cp_name,
            "cb_cp_position" => $cp_position,
          ));

          for ($i=3; $i <= $arr_count-1; $i++) {
            $explode_cp_contact = explode("-", $explode_val[$i]);
            $contact_type = $explode_cp_contact[0];
            $contact = $explode_cp_contact[1];
            $contact_id = $explode_cp_contact[2];

            CustomsBrokerageContact::where("id", $contact_id)->update(array(
              "cb_cp_contact_type" => $contact_type,
              "cb_cp_contact" => $contact,
            ));
          }
        }
      }

    }
    elseif ($type == "add_details") {

      $cb_address_arr = $request->cb_address;
      $cb_cp_arr = $request->cb_contact_person;

      if (array_filter($cb_address_arr) != []) {
        foreach ($cb_address_arr as $cb_address) {
          $explode_address = explode(",", $cb_address);

          foreach ($explode_address as $cb_add) {
            $explode = explode("/", $cb_add);
            $address_type = $explode[0];
            $address = $explode[1];
            $city = $explode[2];

            $insert_address[] = [
              "cb_address_type" => $address_type,
              "cb_address" => $address,
              "cb_city" => $city,
              "cb_id" => $cb_id,
            ];
          }
        }
        CustomsBrokerageAddress::insert($insert_address);
      }

      if (array_filter($cb_cp_arr) != []) {
        foreach ($cb_cp_arr as $cb_cp) {
          $explode_cp = explode(",", $cb_cp);

          foreach ($explode_cp as $cp) {
            $explode_val = explode("/", $cp);
            $arr_count = count($explode_val);
            $cp_name = $explode_val[0];
            $cp_position = $explode_val[1];

            $new_cp = new CustomsBrokerageContactPerson();
            $new_cp->cb_cp_name = $cp_name;
            $new_cp->cb_cp_position = $cp_position;
            $new_cp->cb_id = $cb_id;
            $new_cp->save();
            $new_cp_id = $new_cp->id;

            for ($i=2; $i <= $arr_count-1; $i++) {
              $explode_cp_contact = explode("-", $explode_val[$i]);
              $contact_type = $explode_cp_contact[0];
              $contact = $explode_cp_contact[1];

              $insert_contact[] = [
                "cb_cp_contact_type" => $contact_type,
                "cb_cp_contact" => $contact,
                "cb_cp_id" => $new_cp_id,
              ];
            }
          }
          CustomsBrokerageContact::insert($insert_contact);
        }
      }

    }
    else {

      $cb_consignee_arr = $request->cb_consignee;

      if (array_filter($cb_consignee_arr) != []) {

        CustomsBrokerageConsignee::where("cb_id", $cb_id)->delete();

        foreach ($cb_consignee_arr as $cb_consignee) {
          $explode_consignee = explode(",", $cb_consignee);

          foreach ($explode_consignee as $cb_sc) {
            $explode = explode("/", $cb_sc);
            $name = $explode[0];
            $address = $explode[1];
            $registration = $explode[2];
            $tin = $explode[3];

            $insert_consignee[] = [
              "cc_name" => $name,
              "cc_address" => $address,
              "cc_registration" => $registration,
              "cc_tin" => $tin,
              "cb_id" => $cb_id,
            ];
          }
        }
        CustomsBrokerageConsignee::insert($insert_consignee);
      }

    }

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function retire(Request $request){

    $cb_id = $request->get_cb_id;

    CustomsBrokerage::where("id", $cb_id)->update(array("status" => "Retired"));

    return response()->json(array(
      "status" => "success",
    ));
  }

  public function removeDetail(Request $request){

    $get_type = $request->type;

    if ($get_type == "address") {
      CustomsBrokerageAddress::where("id", $request->get_address_id)->delete();
    }
    elseif ($get_type == "person") {
      CustomsBrokerageContact::where("cb_cp_id", $request->get_person_id)->delete();
      CustomsBrokerageContactPerson::where("id", $request->get_person_id)->delete();
    }
    elseif ($get_type == "contact") {
      CustomsBrokerageContact::where("id", $request->get_contact_id)->delete();
    }
    else {
      CustomsBrokerageConsignee::where("id", $request->get_consignee_id)->delete();
    }

    return response()->json(array(
      "status" => "success",
    ));
  }
}
























//
