<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use App\Models\Containers;
use App\Models\User;
use App\Models\TruckingCompany;

class EndorsedTruckingController extends Controller
{

  public function index(){

    $current_date = date('Y-m-d');
    $get_trucking_companies = TruckingCompany::where("status", "Active")->get();
    $get_endorsed_trucking = Containers::with("shipper", "consignee", "shipping",
                                "loading", "discharge", "item", "trucking",
                                "customer", "encoder", "recorder")
                              ->where("ic_status", "Endorsed-Trucking")
                              ->where("status", "Active")
                              ->get();

    return view('transactions.endorsed-trucking.index', compact('get_endorsed_trucking', 'current_date', 'get_trucking_companies'));
  }

  public function additionalDetail(Request $request){

    date_default_timezone_set('Asia/Manila');
    $date = date('Y-m-d');
    $user = Auth::guard()->user();
    $user_id = $user->id;
    $ic_id = $request->ic_id;
    $request_type = $request->request_type;
    $response = array(
      "status" => "success",
    );

    function generateRandomString($length) {
      return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    $random_str = generateRandomString(10);

    switch ($request_type) {
      case 'endorsed_date':
        $query = array(
          "ic_endorsed_date" => $request->get_date,
        );
        $response["date_raw"] = $request->get_date;
        $response["date"] = date("m-d-Y", strtotime($request->get_date));
        break;

      case 'load_date':
        $query = array(
          "ic_load_date" => $request->get_date,
        );
        $response["date_raw"] = $request->get_date;
        $response["date"] = date("m-d-Y", strtotime($request->get_date));
        break;

      case 'delivered_date':
        $query = array(
          "ic_delivery_date" => $request->get_date,
        );
        $response["date_raw"] = $request->get_date;
        $response["date"] = date("m-d-Y", strtotime($request->get_date));
        break;

      case 'empty_date':
        $query = array(
          "ic_empty_date" => $request->get_date,
        );
        $response["date_raw"] = $request->get_date;
        $response["date"] = date("m-d-Y", strtotime($request->get_date));
        break;

      case 'trucking':
        $query = array(
          "ic_trucking_company" => $request->get_trucking,
        );
        $response["trucking"] = $request->get_trucking;
        break;

      case 'upload_eir':
        $file = $request->file('get_eir_file');
        $name = $file->getClientOriginalName();
        $file->move(public_path('storage/incoming-container-files/'), 'eir_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name);
        $url = 'storage/incoming-container-files/eir_'.$ic_id.'_'.$date.'_'.$random_str.'_'.$name;
        $query = array(
          "ic_eir_date" => $request->get_eir_date,
          "ic_eir_file" => $url,
        );
        $response["eir_date_raw"] = $request->get_eir_date;
        $response["eir_date"] = date("m-d-Y", strtotime($request->get_eir_date));
        $response["eir_file"] = $url;
        break;

      case 'remarks':
        $query = array(
          "ic_final_remarks" => $request->get_remarks,
        );
        $response["remarks"] = $request->get_remarks;
        break;

      default:

        break;
    }

    Containers::where("id", $ic_id)->update($query);

    return response()->json($response);
  }

}






















//
